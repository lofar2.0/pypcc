from test_common import *

name="RCU_IO4_GPIO2"
RCU=2;

connect()
setRCUmask([RCU])
led=get_debug_value(name+"_R")
print("GPIO:",[hex(x) for x in led])

disconnect()
