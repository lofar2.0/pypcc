##NB translator should be off or monitor should be 0

#name='APSCTTR' #YAML config file with all register values etc
#varID='APSCT_PCB_ID'

name='APSPUTR' #YAML config file with all register values etc
varID='APSPU_PCB_ID'

logFile='SetVersion'

import logging
import argparse
from pypcc.opcuaserv import opcuaserv
from pypcc.opcuaserv import i2client
from pypcc.opcuaserv import yamlreader
#from opcuaserv import pypcc2
from pypcc.i2cserv import i2cthread
import threading
import time
import sys
import signal
from pypcc.yamlconfig import Find;
import pypcc.yamlconfig as yc
from datetime import datetime

testtime=datetime.now().strftime("%y-%m-%d %H:%M")
if len(sys.argv)<2:
  print("setVersion Var Value");
  print("e.g. SetVersion RCU_PCB_version RCU2L-XXYZ")
  exit();

varSet=sys.argv[1]
Ver=sys.argv[2] if (len(sys.argv)>2) else None

logging.basicConfig(level="WARNING",format='%(asctime)s [%(levelname)-8s,%(filename)-20s:%(lineno)-3d] %(message)s')
#logging.basicConfig(level="DEBUG",format='%(asctime)s [%(levelname)-8s,%(filename)-20s:%(lineno)-3d] %(message)s')

RunTimer=True;
conf=yc.yamlconfig(name)
conf.linkdevices()
conf.loaddrivers()
conf.linkdrivers()

def GetVal(name,N=1):
 varid=conf.getvarid(name);
 var1=conf.getvars()[varid]
 drv=var1.get('drivercls');
 mask=[True];
 data=drv.OPCUAReadVariable(varid,var1,mask)
 data=data[0].data
# N3=len(data)//32;
 return data,var1

#reading the ID also set the switch
data,var1=GetVal(varID);
ID=("%.2x%.2x%.2x%.2x" % (data[0],data[1],data[2],data[3]))

rootLogger = logging.getLogger()
rootLogger = logging.getLogger()
fileHandler = logging.FileHandler("{0}.log".format(logFile))
#fileHandler.setFormatter(logFormatter)
rootLogger.addHandler(fileHandler)

logging.warning("ID=%s" % ID)
if int(ID,16)==0: 
    logging.error("Error: No ID");
    exit()

data,var1=GetVal(varSet);
version=bytearray(data).decode("utf-8",errors='ignore')
logging.warning("Old "+varSet+":"+version)
if Ver is None: exit()
#print(var1)
ROMaddr=var1['devreg'][0]['addr']
ROMreg=var1['devreg'][0]['register_W']
varlen=var1['width']//8
#print(varlen)
#exit()

i2c=conf.conf['drivers'][0]['obj'] #assume I2C is first device

#exit()

#R1=0
#ROM=0x50

#Upload version
Ver2=[(c.encode('utf-8')[0]) for c in Ver]
Ver2=Ver2[:varlen]
#print(len(Ver),Ver,Ver2)
V2=[0]
for i,v in enumerate(Ver2):
 time.sleep(0.1)
 i2c.i2csetget(ROMaddr,[v],reg=ROMreg+i,read=0)
# time.sleep(0.1)
# i2c.i2csetget(ROMaddr,V2,reg=ROMreg+i,read=1)
# print(i,v,V2)
for i in range(len(Ver2),varlen):
 time.sleep(0.1)
 i2c.i2csetget(ROMaddr,[255],reg=ROMreg+i,read=0)
# time.sleep(0.1)
# i2c.i2csetget(ROMaddr,V2,reg=ROMreg+i,read=1)

time.sleep(0.1)
data,var1=GetVal(varSet);
version=bytearray(data).decode("utf-8",errors='ignore')
#print("Version",version)
logging.warning("New "+varSet+":"+version)
