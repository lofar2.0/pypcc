RCU=1
HBAT=0 #HBAT on RCU 0..2
HBA=5; #HBA Element in HBAT
BFX=11 #delay in 0.5ns
BFY=BFX+1
name="HBA_element_beamformer_delays"

from test_common import *
import numpy as np

AntMask=[(x==HBAT) for x in range(3)]
setAntmask([RCU],AntMask)

i=(RCU*3+HBAT)*32+HBA*2

val,tp=get_value_type(name+"_R")
print("old:",tp,val[i:i+2])

val[i]=BFX
val[i+1]=BFY

set_value(name+"_RW",val,tp)
time.sleep(1)
val=get_value(name+"_R")
print("new:",val[i:i+2])

disconnect()
