##NB translator should be off or monitor should be 0
name='RECVTR_LB' #YAML config file with all register values etc
varID='RCU_PCB_ID'
regname="RCU_ADC_test"

logFile='ADCtest'

import logging
import argparse
from pypcc.opcuaserv import opcuaserv,i2client,yamlreader
#from opcuaserv import pypcc2
from pypcc.i2cserv import i2cthread
import threading
import time
import sys
import signal
from pypcc.yamlconfig import Find;
import pypcc.yamlconfig as yc
from datetime import datetime
import numpy as np

testtime=datetime.now().strftime("%y-%m-%d %H:%M")
#if len(sys.argv)<3:
#  print("setVersion RCUnr RCUsi HBAnr");
#  print("e.g. SetVersion 8 1 1")
#  exit();

#RCUNR=int(sys.argv[1]);
#SINR=int(sys.argv[2]);
#HBANR=int(sys.argv[3]);
#print(RCUNR,SINR,HBANR)
logging.basicConfig(level="WARNING",format='%(asctime)s [%(levelname)-8s,%(filename)-20s:%(lineno)-3d] %(message)s')
#logging.basicConfig(level="DEBUG",format='%(asctime)s [%(levelname)-8s,%(filename)-20s:%(lineno)-3d] %(message)s')

RunTimer=True;
conf=yc.yamlconfig(name)
conf.linkdevices()
conf.loaddrivers()
conf.linkdrivers()

def GetVal(name):
 varid=conf.getvarid(name);
# print("varid",varid)
 var1=conf.getvars()[varid]
 dim=var1['dim']
 drv=var1.get('drivercls');
 mask=[True]*dim#+[True]*N+[False]*((dim-RCUNR-1)*N);
 data=drv.OPCUAReadVariable(varid,var1,mask)
 data=data[0].data
# N3=len(data[0])//dim
 return data,var1


def SetVal(name,data):
 varid=conf.getvarid(name);
# print("varid",varid)
 var1=conf.getvars()[varid]
 dim=var1['dim']
 drv=var1.get('drivercls');
 mask=[True]*dim#+[True]*N+[False]*((dim-RCUNR-1)*N);
 return drv.OPCUASetVariable(varid,var1,data,mask)

def SetReg(name,data,drvvar):
 varid=conf.getvarid(drvvar)
 var2=conf.getvars()[varid]
 drv=var2.get('drivercls');
 var1=conf.getdevreg(name);
# print("Var",var1)
# drv=var1.get('drivercls');
# print("DRV",drv)
 drv.Setdevreg(var1,data,[])



#reading the ID also set the switch
print("Get IDs")
data,var1=GetVal(varID);
print(data)
data=np.array(data)[::4]
mask=[not(d is None) for d in data]
print(mask)
#GetReg("GPIO3.
#SetReg("IO3.GPIO1",[21]*32)
#SetReg("IO3.GPIO2",[71]*32)

#exit()
data,var1=GetVal("RCU_IO1_GPIO1")
data,var1=GetVal("RCU_IO1_GPIO2")
data,var1=GetVal("RCU_IO2_GPIO1")
data,var1=GetVal("RCU_IO2_GPIO2")

if False:
  SetVal("RCU_IO3_GPIO1",[0]*32)
  SetVal("RCU_IO3_GPIO2",[0]*32)
  SetVal("RCU_PWR_DIGITAL_on",[0]*32)
  SetVal("RCU_PWR_DIGITAL_on",[1]*32)

SetVal("RCU_IO3_GPIO1",[21]*32)
SetVal("RCU_IO3_GPIO2",[71]*32)
SetReg("IO3.CONF1",[0]*32,"RCU_IO3_GPIO1")
SetReg("IO3.CONF2",[0]*32,"RCU_IO3_GPIO1")
SetReg("IO3.POL1",[0]*32,"RCU_IO3_GPIO1")
SetReg("IO3.POL2",[0]*32,"RCU_IO3_GPIO1")


data,var1=GetVal("RCU_PWR_DIGITAL_on")
print("RCU_PWR_DIGITAL_on",data)

data,var1=GetVal("RCU_IO3_GPIO1")
print("RCU_IO3_GPIO1",data)
data,var1=GetVal("RCU_IO3_GPIO2")
print("RCU_IO3_GPIO2",data)
#print("Set Reg")
#exit()
#print("Read Reg")

if False:
  SetVal("RCU_ADC_shutdown",[1]*96)
  SetVal("RCU_ADC_shutdown",[0]*96)
data,var1=GetVal("RCU_ADC_shutdown")
print("RCU_ADC_shutdown",data)

if True: #SPI bus reset
  SetVal("RCU_IO3_GPIO2",[0x40]*32) #bit0,1,2 = CS = Low
  SetVal("RCU_IO3_GPIO1",[0x17]*32) #CLK high 
  SetVal("RCU_IO3_GPIO1",[0x15]*32) #CLK low
  SetVal("RCU_IO3_GPIO1",[0x17]*32) #CLK high
  SetVal("RCU_IO3_GPIO1",[0x15]*32) #CLK low
  SetVal("RCU_IO3_GPIO2",[0x47]*32) #CS = High
#  for x in range(1):
#     SetVal("RCU_IO3_GPIO1",[0x02]*32) #CLK high
#     SetVal("RCU_IO3_GPIO1",[0x00]*32) #CLK low

#  SetVal("RCU_IO3_GPIO1",[0x15]*32) #CLK low
  time.sleep(1)

SetReg("ADC2.test",[2]*32,regname)
SetReg("ADC3.test",[3]*32,regname)
SetReg("ADC1.test",[1]*32,regname)

data,var1=GetVal(regname);
data=np.array(data).reshape([32,3])
print(data)
#exit()
for x in range(1,100):
  print("Set reg",x)
  SetReg("ADC3.test",[x+2]*32,regname)
  SetReg("ADC2.test",[x+1]*32,regname)
  SetReg("ADC1.test",[x]*32,regname)
#  data=np.array([x]*96)
#  SetVal(regname,data)

#print("Read reg")
  data,var1=GetVal(regname);
  data=np.array(data).reshape([32,3])
  print(data)


exit()
#ID=("%.2x%.2x%.2x%.2x" % (data[0],data[1],data[2],data[3]))

