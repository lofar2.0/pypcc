#!/usr/local/bin/python3.11
from pypcc.i2cdirect import i2cdirect
from time import sleep
import hbat1 as hb
d1=i2cdirect('RECVTR_HB_TEST.yaml')

DANTE_ADDR=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]
#DANTE_ADDR=[3,2,5,14,15,16,10,1,19,21,11,6,7,18,4,12]#,3,12,5]#,10,11,14,15,16,19,21,17]
#DANTE_ADDR=[3,12,2]
#DANTE_ADDR.sort()
uCaddr=0x40
HBAi2c=0x42 #middle input


#DANTE registers
HBA_PWR=2 #pwr register
HBA_ID=0x14

#reading the ID also set the switch
#varID='RCU_PCB_ID'

#data,var1=d1.GetVal(varID);
#print('ID=',hex(data[0]))
#ID=("%.2x%.2x%.2x%.2x" % (data[0],data[1],data[2],data[3]))
#print('ID=',ID)
for varname in ['RCU_PCB_ID','RCU_PCB_version','RCU_PCB_number',
                'RCU_firmware_version',]:
    data,var1=d1.GetVal(varname);
    print(varname+"=",data[0])


def SetRegisters(reg,data,addr):
    func=len(data)*2;
    TX1=hb.MakeRequest(addr,data,func,reg)[:-2];
    print(TX1)
    i2c.i2csetget(HBAi2c,TX1,reg=None,read=0)
def getreg(reg,length,addr,wait=0.5):
    func=length*2+1;
    TX1=hb.MakeRequest(addr,[],func,reg)[:-2];
    i2c.i2csetget(HBAi2c,TX1,reg=None,read=0)
    if length==0: return []
    sleep(wait)
    data=[0]*(length+6)
    i2c.i2csetget(HBAi2c,data,reg=None,read=1)
    if data[2]!=addr+0x80:
        print(addr,'no comm');
        return []
    return data[4:-2]

from dante import dante
hba=dante(SetRegisters,getreg)
#def RequestRegisters(reg,length):
#        func=length*2+1;
#        TX1=hb.MakeRequest(HBA_select,[],func,reg);
#        if Debug: print("Packet to TX",TX1)
#        TX2=hb.ManchesterEncode(TX1)
 #       self.ser.write(bytearray(TX2))
#        return GetPackets(self.ser);
#Configure RCU in debug mode
i2c=d1.conf.conf['drivers'][0]['obj'] #assume I2C is first device
#if True:
def setup_rcu():
#    print("Set Vref=0x0c");
    i2c.i2csetget(uCaddr,[0x08],reg=13,read=0)
#    print("Set mode=0");
    i2c.i2csetget(uCaddr,[0],reg=2,read=0)
#    print("Set Auto readback=0");
    i2c.i2csetget(uCaddr,[0],reg=5,read=0)
#    print("Set timeouts");
    i2c.i2csetget(uCaddr,[0xf0],reg=11,read=0) #was 0x50
    i2c.i2csetget(uCaddr,[0xa],reg=9,read=0) #0x4
    print("TX start high=20");
    i2c.i2csetget(uCaddr,[32],reg=14,read=0)
setup_rcu()
v=[0]
i2c.i2csetget(uCaddr,v,reg=2 ,read=1);print("RCU uC Mode",v[0]);
i2c.i2csetget(uCaddr,v,reg=13,read=1);print("RCU uC Vref",hex(v[0]));
i2c.i2csetget(uCaddr,v,reg=5 ,read=1);print("RCU uC Readback",v[0]);
i2c.i2csetget(uCaddr,v,reg=14,read=1);print("RCU uC start high",v[0]);
i2c.i2csetget(uCaddr,v,reg=11,read=1);print("RCU uC RX start timeout",hex(v[0]));
i2c.i2csetget(uCaddr,v,reg=9,read=1);print("RCU uC RX end timeout",hex(v[0]));

#print(DANTE_ADDR)
if False: #get IDs
# for x in range(3):
  for select in DANTE_ADDR[::]:
    setup_rcu()
    id=hba.get_id(select);
    if len(id)<3: 
        setup_rcu()
        id=hba.get_id(select);

    print(select,id)
#  sleep(2)
#exit()
select=7
SetRegisters(0,[0x80,0x80],addr=select);#power on
exit()
#sleep(0.05)
#SetRegisters(0,[0x80,0x80],addr=select);#power on
for select in DANTE_ADDR:
#for select in []:
# if select in [7]: continue;
 SetRegisters(0,[0x82,0x82],addr=select);#power off
 sleep(0.05)
#select=7
#SetRegisters(0,[0x80,0x80],addr=select);#power on
#sleep(0.05)
#SetRegisters(0,[0x80,0x80],addr=select);#power on
exit()
#while True:
if True:# power off
 for select in DANTE_ADDR:
    setup_rcu()
#    hba.power_on(select)
    SetRegisters(HBA_PWR,[0x00],addr=select);#only buffer amps
    sleep(0.05)
    #hba.set_term(True,select)
    #sleep(0.2)
#exit()
 sleep(2)
if True:# power on
 for select in DANTE_ADDR:
    setup_rcu()
#    hba.power_on(select)
    SetRegisters(HBA_PWR,[0x0C],addr=select);#only buffer amps
    sleep(0.05)
    #hba.set_term(True,select)
    #sleep(0.2)
#if True: #LEDs on
 sleep(2)
while True:
 for select in DANTE_ADDR:
    setup_rcu()
    hba.set_LED(True,select)
    sleep(0.03)
# sleep(2)
 for select in DANTE_ADDR:
    setup_rcu()
    hba.set_LED(False,select)
    sleep(0.03)
# sleep(2)