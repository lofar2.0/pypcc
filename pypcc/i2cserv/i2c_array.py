import numpy as np
import logging
from .spibitbang1 import spibitbang1
from pypcc.queuetypes import *
from .hwdev import hwdev
from .i2c_dev import *
from time import sleep

class i2c_array(i2c_dev):
    def __init__(self,config):
        i2c_dev.__init__(self,config);
#        self.Qout=Qout;
#        self.Qin=Qin;
#        self.I2Ccallback=I2Ccallback
#        self.SWcallback=Switchcallback
#        self.previousHBA=np.zeros([number,3,32],dtype='int')
        pars=config['parameters'];
        self.RCU_Switch1=pars; #range(pars[0],pars[1]+1);
        self.RCUorder=sorted(range(len(pars)),key=lambda k:pars[k])
        self.N=len(self.RCU_Switch1);
        self.I2Ccut=config.get('I2Ccut',5);#pars[2];
        self.enablemask=[p<self.N for p in pars]
        print("enable mask",self.enablemask) 
        self.I2Cmask=[0]*self.N
        self.disableI2ConError=self.I2Ccut>0;
        self.RCUi=0
#        self.devregs,RCU_storeReg=DevRegList(yaml)
#        print("Init",config['name'],'len=',len(self.RCU_Switch1),' stored reg=',RCU_storeReg)
#        self.previous   =np.zeros([self.N,RCU_storeReg],dtype='int')

    def OPCUAReadVariable(self,varid,var1,mask):
      if len(mask)==0: mask=self.enablemask.copy(); 
      return i2c_dev.OPCUAReadVariable(self,varid,var1,mask)

    def SetSwitch(self,RCUi):
        return self.conf['parentcls'].SetSW1(self.RCU_Switch1[RCUi]);

    def SetSwitchMask(self,mask):
        m=0;
        self.mask=[]
        self.conf['parentcls'].ClearNewChannel();
        for RCUi in self.RCUorder:
           if (mask[RCUi]) and (self.I2Cmask[RCUi]<=self.I2Ccut): 
               self.conf['parentcls'].AddNewChannel(self.RCU_Switch1[RCUi]);
               self.RCUi=RCUi
               self.mask+=[RCUi]
        self.conf['parentcls'].UpdateNewChannel();

    def SetGetVarValueMask(self,var1,data,mask,getalso=True):
        Step,Step2=GetSteps(var1);
        value1=[0]*Step*Step2;
        if len(data)==Step:
           data=data*self.N;
        if not(len(data)==Step*Step2):
            print("Check data length!");
            return;
        Step2//=self.N
        if (len(mask)==self.N):
          mask=[m for m in mask for x in range(Step)]
        if not(len(mask)==Step*self.N):
            print("Check mask length!",len(mask),Step,self.N);
            return
        else: 
          for x,enanbled in enumerate(self.enablemask):
               if enanbled: continue;
               for y in range(Step): mask[x*Step+y]=False;
        self.conf['parentcls'].clearSwitch();
#        if (len(value1)==V1.nVars) and (self.N>1):  value1=(value1*self.N);
#        logging.debug(str(("Step=",Step,"Mask=",mask)))
        if var1.get('disable_autoreadback',False)==True: 
                    getalso=False
        if var1.get('read_parallel')=='all':
             for Vari in range(Step):
               devreg=var1['devreg'][Vari];
               width=var1.get('width',8)
               bitoffset=GetField(var1,'bitoffset',Vari,0)
               mask2=[mask[RCUi*Step+Vari] & (self.I2Cmask[RCUi]<=self.I2Ccut) for RCUi in range(self.N)]
               #self.mask=mask2;
               isthesame=True;
               self.SetSwitchMask(mask2);
               for RCUi in self.mask: 
#                    if not(mask2[RCUi]): continue;
                    for i in range(Step2):
                        if data[(Vari+RCUi*Step)*Step2+i]!=data[(Vari+self.RCUi*Step)*Step2+i]:
                             isthesame=False;
               if isthesame:
                  data2=data[(Vari+self.RCUi*Step)*Step2:(Vari+self.RCUi*Step+1)*Step2]
                  res=self.SetVarValue(devreg,width,bitoffset,data2)
               else:
                 for RCUi in self.RCUorder:
                    if not(mask[RCUi*Step+Vari]): continue
                    self.mask=[RCUi];
                    if not(self.SetSwitch(RCUi)):
                           #self.I2Cmask[RCUi]+=1;
                           return value1,[False for x in mask]
#                           continue;
                    self.RCUi=RCUi;
                    i0=(RCUi*Step+    Vari)*Step2
                    i1=(RCUi*Step+(Vari+1))*Step2
                    res=self.SetVarValue(devreg,width,bitoffset,data[i0:i1])
               if getalso:
                 self.SetSwitchMask(mask2);
                 value2=[0]*self.N*Step2;               
                 res=self.GetVarValue(devreg,width,bitoffset,value2,0)
#                print(Vari,mask2,value2)
                 for r in range(self.N): value1[(Vari+r*Step)*Step2:(Vari+r*Step+1)*Step2]=value2[r*Step2:(r+1)*Step2]
#              print(len(value1),value1)
             return value1,mask
        for RCUi in self.RCUorder:
            for Vari in range(Step):
                if not(mask[RCUi*Step+Vari]): continue
                if not(self.I2Cmask[RCUi]<=self.I2Ccut):
                  mask[RCUi*Step+Vari]=False;
                  continue;
                i0=(RCUi*Step+    Vari)*Step2
                i1=(RCUi*Step+(Vari+1))*Step2
                devreg=var1['devreg'][Vari];
                width=var1.get('width',8)
                bitoffset=GetField(var1,'bitoffset',Vari,0)
                if not(self.SetSwitch(RCUi)):
                       #self.I2Cmask[RCUi]+=1;
                       #mask[RCUi*Step+Vari]=False;
                       return value1,[False for x in mask]
#                       continue;
                self.RCUi=RCUi;
                res=self.SetVarValue(devreg,width,bitoffset,data[i0:i1])
                if getalso and (var1.get('wait',0)>0): sleep(var1['wait']/1000)
                if not(res):
                  if self.disableI2ConError: self.I2Cmask[RCUi]+=1;
                  if self.I2Cmask[RCUi]>self.I2Ccut: mask[RCUi*Step+Vari]=False;
                  continue;
                self.I2Cmask[RCUi]=0;
                if getalso:
                  value2=value1[i0:i1]
                  res=self.GetVarValue(devreg,width,bitoffset,value2)
                  if not(res):
                    if self.disableI2ConError: self.I2Cmask[RCUi]+=1;
                    if self.I2Cmask[RCUi]>self.I2Ccut: mask[RCUi*Step+Vari]=False;
                    continue;
                  value1[i0:i1]=value2
                  self.I2Cmask[RCUi]=0;
        return value1,mask

#    def GetVarValueParallel(self,var1,mask):
#         mask2=[mask[RCUi*Step] & (self.I2Cmask[RCUi]<=self.I2Ccut) for RCUi in range(self.N)]
#         self.SetSwitchMask(mask2);
#         def 

    def GetVarValueMask(self,var1,mask):
        Step,Step2=GetSteps(var1);
        value1=[0]*Step*Step2;
        Step2//=self.N
        if (len(mask)==self.N):
          mask=[m for m in mask for x in range(Step)]
        if not(len(mask)==Step*self.N):
            print("Check mask length!",len(mask),Step,self.N);
            return;
        else:
           if not(var1.get('check_value')): 
             for x,enanbled in enumerate(self.enablemask):
               if enanbled: continue;
               for y in range(Step): mask[x*Step+y]=False;
#               mask[x*Step:(x+1)*Step]=False;
#        if (len(value1)==V1.nVars) and (self.N>1):  value1=(value1*self.N);
        i2c=self.conf['parentcls'];
        self.conf['parentcls'].clearSwitch();
#        logging.debug(str(("Step=",Step,"Mask=",mask)))
#        if var1.get('read_parallel')=='all':
#              return self.GetVarValueParallel(self,var1,mask)
#        print(Step,Step2,len(mask),len(value1))
        if var1.get('read_parallel')=='all':
             for Vari in range(Step):
               devreg=var1['devreg'][Vari];
               width=var1.get('width',8)
               bitoffset=GetField(var1,'bitoffset',Vari,0)
               mask2=[mask[RCUi*Step+Vari] & (self.I2Cmask[RCUi]<=self.I2Ccut) for RCUi in range(self.N)]
               self.SetSwitchMask(mask2)
#               self.mask=mask2;
               value2=[0]*self.N*Step2;
               res=self.GetVarValue(devreg,width,bitoffset,value2,0)
#               print(Vari,mask2,value2)
               for r in range(self.N): value1[(Vari+r*Step)*Step2:(Vari+r*Step+1)*Step2]=value2[r*Step2:(r+1)*Step2]
#             print(len(value1),value1)
             return value1,mask
           
        mode=(1 if var1.get('read_parallel') else 0)
        for Vari in range(Step):
          if mode==1:
#            logging.debug("parallel write register for read")
#            print(mask2);
               devreg=var1['devreg'][Vari];
               width=var1.get('width',8)
               bitoffset=GetField(var1,'bitoffset',Vari,0)
               mask2=[mask[RCUi*Step+Vari] & (self.I2Cmask[RCUi]<=self.I2Ccut) for RCUi in range(self.N)]
               self.SetSwitchMask(mask2);
               res=self.GetVarValue(devreg,width,bitoffset,[],mode=3) #wait if needed
               res=self.GetVarValue(devreg,width,bitoffset,[],mode=2) #only set register
               res=self.GetVarValue(devreg,width,bitoffset,[],mode=3) #wait if needed
#            for Vari in range(Step):
#        else:
#            for Vari in range(Step):
          for RCUi in self.RCUorder:
                if not(mask[RCUi*Step+Vari]): continue
                i0=(RCUi*Step+    Vari)*Step2
                i1=(RCUi*Step+(Vari+1))*Step2
                if not(self.I2Cmask[RCUi]<=self.I2Ccut):
#                  mask[RCUi*Step+Vari]=False;  #Use this if we do not what to update opc-ua var with NaN
                  value1[i0]=None
                  continue;
                devreg=var1['devreg'][Vari];
                width=var1.get('width',8)
                bitoffset=GetField(var1,'bitoffset',Vari,0)
                if not(self.SetSwitch(RCUi)): return value1,[False for x in mask]
                value2=value1[i0:i1]
                self.RCUi=RCUi;
                res=self.GetVarValue(devreg,width,bitoffset,value2,mode=mode)
                if not(res):
                  if self.disableI2ConError: self.I2Cmask[RCUi]+=1;
                  if self.I2Cmask[RCUi]>self.I2Ccut: mask[RCUi*Step+Vari]=False;
                  value1[i0]=None
                  continue;
                self.I2Cmask[RCUi]=0;
                value1[i0:i1]=value2
        if var1.get('check_value'):
            check_val=var1.get('check_value')
            Ncheck=len(check_val);
            if Ncheck>Step2: Ncheck=Step2;
            for Vari in range(Step):
               for RCUi in self.RCUorder:
                  if not(mask[RCUi*Step+Vari]): continue
                  i0=(RCUi*Step+    Vari)*Step2
                  i1=(RCUi*Step+(Vari+1))*Step2
                  if value1[i0] is None: 
                           #print(RCUi,'None')
                           self.enablemask[RCUi]=False;
                           mask[RCUi*Step+Vari]=False
                           continue;
                  for x in range(Ncheck):
                        #print(RCUi,ord(check_val[x]),value1[i0+x])
                        if ord(check_val[x])!=value1[i0+x]:
                           self.enablemask[RCUi]=False;
                           mask[RCUi*Step+Vari]=False
            logging.warn(str(("Identify RCUs:",check_val,self.enablemask)))
        return value1,mask


    def SetStoredValue(self,var1,data,mask):
        Step,Step2=GetSteps(var1);
        Step2//=self.N
        if len(mask)==0: 
             mask=self.enablemask.copy()
        if (len(mask)==self.N):
          mask=[m for m in mask for x in range(Step)]
        if not(len(mask)==Step*self.N):
            logging.warning(str(("Check mask length!",len(mask),Step,self.N)));
            return False
#        print(self.N,Step,Step2,len(mask),len(data),mask)
        for Vari in range(Step):
          devreg=var1['devreg'][Vari];
          width=var1.get('width',8)
          bitoffset=GetField(var1,'bitoffset',Vari,0)
          if not(devreg.get('store')): continue
          storearray=self.getstorearray(devreg);
          for RCUi in self.RCUorder:
                if not(mask[RCUi*Step+Vari]): continue
                i0=(RCUi*Step+    Vari)*Step2
#                i1=(RCUi*Step+(Vari+1))*Step2
#                value2=data[i0:i1]

                previous=storearray[RCUi];
#                print(RCUi,previous,data[i0:i0+Step2])
#                for x in range(len(value2)):
                value2=[ApplyMask(data[i0+x],width,bitoffset,(previous[x] if isinstance(previous,list) else previous)) for x in range(Step2)]
                storearray[RCUi]=(value2[0] if len(value2)==1 else value2[:])
                logging.info(str(("Stored values:",self.getstorearray(devreg),'*',storearray[RCUi])))
        return True



    def GetStoredValue(self,var1):
        Step,Step2=GetSteps(var1);
        value1=[0]*Step*Step2;
        Step2//=self.N
        for Vari in range(Step):
          devreg=var1['devreg'][Vari];
          width=var1.get('width',8)
          bitoffset=GetField(var1,'bitoffset',Vari,0)
          if not(devreg.get('store')): continue
          storearray=self.getstorearray(devreg);
          for RCUi in self.RCUorder:
                i0=(RCUi*Step+    Vari)*Step2
                i1=(RCUi*Step+(Vari+1))*Step2
                value2=value1[i0:i1]
#                res=self.GetVarValue(devreg,width,bitoffset,value2,mode=mode)
                try:
                  value2[:]=storearray[RCUi]
                except:
                  logging.debug("RCU%i Wrong size (no data?) in stored array"%(RCUi))
# of len(storearray[RCUi]==0)
#                storearray[self.RCUi]=(value[0] if len(value)==1 else value[:])
                l1=int(np.floor((width+bitoffset+7)/8))
                if (width!=l1*8) or (bitoffset>0):
                  if (width<8):
                    for i in range(len(value2)):
                       value2[i]=UnMask(value2[i],width,bitoffset)
                  else:
                       value2[0]=UnMask(value2[0],width-(l1-1)*8,bitoffset)
#        else: value[0]=value2[0]
                value1[i0:i1]=value2
        return value1


    def getstorearray(self,devreg,N=1):
          storearray=devreg.get('storearray')
          if not(storearray):
                devreg['storearray']=([0] if N==1 else [[0]*N])*self.N;
                storearray=devreg.get('storearray');
          return storearray;

    def Setdevreg(self,devreg,value,mask=[]):
#        if devreg.get('store'): logging.debug("Stored")
#        print(devreg['store'])
        if len(mask)==0: 
             mask=self.enablemask.copy()
#             mask=[True]*self.N;
             self.RCUi=0; 
#        self.mask=mask;
        self.conf['parentcls'].clearSwitch();
        self.SetSwitchMask(mask)
        if not(self.SetVarValue(devreg,8,0,value)): return False;
        if devreg.get('store'):
                storearray=self.getstorearray(devreg);
                for RCUi in range(self.N):
                  if (mask[RCUi]) and (self.I2Cmask[RCUi]<=self.I2Ccut) and (self.enablemask[RCUi]) and not(value[0] is None):   
                      storearray[RCUi]=value[0] if len(value)==1 else value[:]
                      self.RCUi=RCUi;
                logging.info(str(("Stored values:",self.getstorearray(devreg))))
        return True;
        

    def Getdevreg(self,devreg,mask=[]):
        value1=[0]*self.N
        mask=self.enablemask.copy()
        if devreg.get('store'):
                storearray=self.getstorearray(devreg);
        i2c=self.conf['parentcls'];
        self.conf['parentcls'].clearSwitch();
        for RCUi in self.RCUorder:
                if not(mask[RCUi]): continue
                if not(self.I2Cmask[RCUi]<=self.I2Ccut): continue
                if not(self.SetSwitch(RCUi)): return False;
                value2=[value1[RCUi]]
                self.RCUi=RCUi;
                res=self.GetVarValue(devreg,8,0,value2)
                if not(res):
                    if self.disableI2ConError: self.I2Cmask[RCUi]+=1;
                self.I2Cmask[RCUi]=0; 
                value1[RCUi]=value2[0]
                if devreg.get('store')==True:
                  if mask[RCUi] and not(value2[0] is None):   
                      storearray[RCUi]=value2[0]
        logging.debug(str(("Stored values:",self.getstorearray(devreg))))
        return True;



    def SetVarValue(self,devreg,width,bitoffset,value):
            if devreg['register_W']==-1: return True; #We can not set it, only read it e.g. temperature
            logging.debug(str(("RCU1 Set ",self.RCUi,devreg['addr'],value)))
            #self.parentcls.SetChannel(1<<RCU_Switch[RCUi]);
            if devreg['store']:
                storearray=self.getstorearray(devreg);
                previous=storearray[self.RCUi];
#                print(value,previous)
                for x in range(len(value)):
                  value[x]=ApplyMask(value[x],width,bitoffset,(previous[x] if isinstance(previous,list) else previous));
                storearray[self.RCUi]=(value[0] if len(value)==1 else value[:])
                logging.debug("Stored value:"+str(storearray[self.RCUi]))
            #  devreg['drivercls'].i2csetget
            return devreg['drivercls'].i2csetget(devreg['addr'],value,reg=devreg['register_W'])

    def GetVarValue(self,devreg,width,bitoffset,value,mode=0):
        logging.debug(str(("GetVarValue",self.RCUi,devreg['addr'],value,mode)))
#                self.GetI2C(RCUi,devreg,width,bitoffset,value)
#        if dev.store>0:
#            value[0]=self.previous[RCUi,dev.store-1]
#        return True
        callback=devreg['drivercls'].i2csetget;
        reg=devreg['register_R']

        if mode==2:
           return callback(devreg['addr'],int2bytes(reg),read=2)
        elif mode==3:
           if devreg.get('wait',0)>0: callback(0,[devreg['wait']],read=3)
           elif reg>255: callback(0,[250],read=3)
           return True;

        l1=int(np.floor((width+bitoffset+7)/8))
#        print(width,bitoffset,l1)
        value2=value
        value2[0]=None; #default
        if mode==1:
           if not(callback(devreg['addr'],value2,read=1)): return False;
        else:
          if reg>255: #This is for the monitor ADC (+ DTH)
            callback(0,[250],read=3)
            if not(callback(devreg['addr'],int2bytes(reg),read=2)): return False;
            callback(0,[250],read=3)
            if not(callback(devreg['addr'],value2,read=1)): return False;
          else:
            rd=(5 if devreg.get('crc') else 1)
            if not(callback(devreg['addr'],value2,reg=reg,read=rd)): return False;
        if value2[0] is None:  return False
        value[:]=value2[:];
        if devreg.get('store')==True:
             storearray=self.getstorearray(devreg,len(value));
             storearray[self.RCUi]=(value[0] if len(value)==1 else value[:])
             logging.debug(str(("Store buffer",self.RCUi,storearray[self.RCUi])))
 #            print("Stored values:",self.getstorearray(devreg))
        if (width!=l1*8) or (bitoffset>0):
            if (width<8):
              for i in range(len(value)):
                value[i]=UnMask(value[i],width,bitoffset)
            else:
                value[0]=UnMask(value[0],width-(l1-1)*8,bitoffset)
        else: value[0]=value2[0]
        return True;


    def GetVarValuePar(self,devreg,width,bitoffset,value,buffer=False):
    #setup self.mask, switch channels and self.RCUi before 
        logging.debug(str(("GetVarValueP",self.RCUi,devreg['addr'],value,buffer)))
#                self.GetI2C(RCUi,devreg,width,bitoffset,value)
#        if dev.store>0:
#            value[0]=self.previous[RCUi,dev.store-1]
#        return True
        callback=devreg['drivercls'].i2csetget;
        reg=devreg['register_R']
        step2=len(value);
        value2=[0]*self.N;
        l1=int(np.floor((width+bitoffset+7)/8))
#        print(width,bitoffset,l1)
        #for x in range(self.N):
        #   value2[x*step2]=None; #default
        if buffer:
           if devreg.get('store'):
             storearray=self.getstorearray(devreg,step2);
             for x in self.mask:
                  value2[x*step2:(x+1)*step2]=storearray[x]
        else:
           if len(self.mask)<1: self.conf['parentcls'].UpdateNewChannel();
           callback(devreg['addr'],int2bytes(reg),read=2)
           for x in self.mask:
              value3=value2[x*step2:(x+1)*step2]
              if not(self.SetSwitch(x)): return value2
              callback(devreg['addr'],value3,read=1)
              if devreg.get('store')==True:
                 storearray=self.getstorearray(devreg,step2);
                 storearray[x]=(value3[0] if step2==1 else value3[:])
              value2[x*step2:(x+1)*step2]=value3;
                 # logging.debug(str(("Store buffer",self.RCUi,storearray[self.RCUi])))
 #            print("Stored values:",self.getstorearray(devreg))
        logging.debug(str(("GetVarValueP",value2)))
        if (width!=l1*8) or (bitoffset>0):
            if (width<8):
              for i in range(len(value2)):
                value2[i]=UnMask(value2[i],width,bitoffset)
            else:
                value2[0]=UnMask(value2[0],width-(l1-1)*8,bitoffset) #not implemented correctly!!
        logging.debug(str(("GetVarValueP2",value2)))
        return value2;


    def SetVarValuePar(self,devreg,width,bitoffset,value,buffer=False):
    #setup self.mask, switch channels and self.RCUi before 
            if devreg['register_W']==-1: return True; #We can not set it, only read it e.g. temperature
            logging.debug(str(("RCU1 Set Par ",self.RCUi,devreg['addr'],value)))
            #self.parentcls.SetChannel(1<<RCU_Switch[RCUi]);
            if devreg['store']:
                storearray=self.getstorearray(devreg);
                previous=storearray[self.RCUi];
#                print(value,previous)
                for x in range(len(value)):
                    value[x]=ApplyMask(value[x],width,bitoffset,(previous[x] if isinstance(previous,list) else previous));
                for x in self.mask:
                    storearray[x]=(value[0] if len(value)==1 else value[:])
                              #print("buffer",devreg['addr'],devreg['register_W'],x,storearray[x])
            #  devreg['drivercls'].i2csetget
            if buffer: return True;
            if len(self.mask)>1: self.conf['parentcls'].UpdateNewChannel();
            return devreg['drivercls'].i2csetget(devreg['addr'],value,reg=devreg['register_W'])
