import numpy as np
from .hwdev import hwdev
import logging
from .i2c_array import ApplyMask
from time import sleep
from pypcc.queuetypes import *
import signal
try:
  import RPi.GPIO as GPIO
except:
  GPIO=False;

class hba1(hwdev):
  def __init__(self,config):
    hwdev.__init__(self,config);
    logging.info("HBA1 driver loaded")
#    self.previousHBA=np.zeros([32,3,32],dtype='int')
    self.pin=config['parameters'][0];
    self.addr=config['devreg'][0]['addr']
    self.reg=config['devreg'][0]['register_R']
    if GPIO:
      GPIO.setmode(GPIO.BCM)
      GPIO.setup(self.pin,GPIO.IN)
      logging.info("HBAT wait for PPS on pin %i, TX=%i:%i",self.pin,self.addr,self.reg)
    else:
      logging.warn("RPi GPIO module not found, PPS input disable!")

  def OPCUASetVariable(self,varid,var1,data,mask):
       logging.info(str(("HBA set Var",var1['name'],len(data),len(mask),data[:32*3],mask[:12])))
       if var1.get('wait')==0: #Do not send, only update stored register
             if len(data)<var1.get("dim"):
                  N=var1.get("dim")//len(data)
                  data=data*N
             self.conf['parentcls'].SetStoredValue(var1,data,mask)
             return self.OPCUAReadVariable(varid,var1,mask)

       self.conf['parentcls'].SetGetVarValueMask(var1,data,mask,getalso=False)
       if len(mask)>0:
         RCUmask=[(mask[i*3] | mask[i*3+1] | mask[i*3+2]) for i in range(32)]
#       print(RCUmask)
       #Wait for PPS if required else wait a bit
         self.conf['parentcls'].SetSwitchMask(RCUmask)
       if var1.get('wait')==-1: #"PPS":
         logging.info("HBA wait PPS")
         channel=GPIO.wait_for_edge(self.pin,GPIO.RISING,timeout=1500)
         self.conf['parentcls'].i2csetget(self.addr,[self.reg])
         if channel is None:
           logging.warning("PPS not received!");
         sleep(2.0)
#         return False;
       elif var1.get('wait'):
         self.conf['parentcls'].i2csetget(self.addr,[self.reg])
         logging.debug("Wait %i ms",var1.get('wait'))
         try:
              sleep(float(var1.get('wait'))/1000.)
         except:
              logging.warning("HBAT1 wait not a number"+str((var1.get('wait'))));
       else:
         self.conf['parentcls'].i2csetget(self.addr,[self.reg])
       data,mask2=self.conf['parentcls'].GetVarValueMask(var1,mask)
       Data=OPCUAset(varid,InstType.varSet,data.copy(),mask2.copy())
       return [Data]

  def OPCUAReadVariable(self,varid,var1,mask):
      #Only read from stored register
      #logging.warn(self.conf['name']+" OPCUAReadVariable Not implemented!")
      data=self.conf['parentcls'].GetStoredValue(var1)
      Data=OPCUAset(varid,InstType.varSet,data,mask)
      return [Data]


  def i2csetget(self,addr,data,reg=None,read=0):
    if read==0: return self.sethba(addr,reg,data)
    elif read==1: return self.gethba(addr,reg,data)
    else: logging.warn("Not implemented!")
    return False;

  def sethba(self,addr,reg,data):
      logging.debug("SetHba addr=0x%x reg=0x%x",addr,reg)
      I2Ccallback=self.conf['parentcls'].i2csetget
      I2Ccallback(0x40,[0],read=1)#wakeup, do nothing
      sleep(0.005) 
      retry=0;
      while not(I2Ccallback(addr,data,reg=reg)) and (retry<2): retry+=1;
      if retry==2: logging.warn("uC BF comm error");
#      if len(data)>16:
#        sleep(0.005) 
#        while not(I2Ccallback(addr,data[16:],reg=reg+16)) and (retry<2): retry+=1;
#        if retry>=2: logging.warn("uC BF comm error2");
      sleep(0.01) 
      return True

  def gethba(self,addr,reg,data):
      I2Ccallback=self.conf['parentcls'].i2csetget
      logging.debug("getHba addr=0x%x reg=0x%x",addr,reg)
      if not(I2Ccallback(addr,data,read=1)): return False;
#      I2Ccallback(addr,data,read=1);
      logging.debug("getHba addr=0x%x reg=0x%x data=%s",addr,reg,str((data)))
      if data is None:  return False
#      data[:]=[255]*len(data)
      return True;
