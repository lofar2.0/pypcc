from test_common import *
import numpy as np

RCU=[0,1,2,3,4,5,6,7,8,9,10];
connect()
setAntmask(RCU)
setRCUmask(RCU)
#call_debug_method("DTH_off")
FRCU=0.05*0;
FCH=0.425*0;
#F0=101.0
F0=97.0
name="RCU_DTH_freq"

if True:
 att=get_value(name+"_R")
 print("freq old:",att[3*RCU[0]:3*RCU[-1]+3])

 for r in RCU:
   Freq=F0+np.array([0,1,2])*FCH+r*FRCU;
   att[3*r:3*r+3]=[int(np.round(x*1e6)) for x in Freq]

 print("freq set:",att[3*RCU[0]:3*RCU[-1]+3])
 set_value(name+"_RW",att)

 time.sleep(5)
 att=get_value(name+"_R")
 print("freq new :",att[3*RCU[0]:3*RCU[-1]+3])

name="RCU_DTH_PWR"
Pwr=-40.0
if True:
 att=get_value(name+"_R")
 print("pwr old:",att[3*RCU[0]:3*RCU[-1]+3])

 for r in RCU:
   att[3*r:3*r+3]=[Pwr,Pwr,Pwr]
 print("pwr set:",att[3*RCU[0]:3*RCU[-1]+3])
 set_value(name+"_RW",att)

 time.sleep(5)
 att=get_value(name+"_R")
 print("pwr new :",att[3*RCU[0]:3*RCU[-1]+3])

callmethod("RCU_DTH_on")
#callmethod("RCU_DTH_stop")

disconnect()