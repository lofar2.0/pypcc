from test_common import *
connect("opc.tcp://localhost:4850/")
import time

var1="RECVTR_PPS_counter_R"
vart="RECVTR_PPS_time"
#  for x in range(20):#
for x in range(4):
  time.sleep(0.5);
  print("PPS=",get_value(var1)," time=",get_value(vart+"_R"));
for y in range(5):
  print("Set time");
  set_value(vart+"_RW",10+y)
  time.sleep(0.1);
  print("PPS=",get_value(var1)," time=",get_value(vart+"_R"));
  for x in range(5):
    time.sleep(0.5);
    print("PPS=",get_value(var1)," time=",get_value(vart+"_R"));

disconnect();
