from enum import Enum
import logging
import numpy as np
from .hwdev import hwdev

class SPIBB_pins(Enum):
    CLK = 0  
    SDI = 1  
    SDO = 2 
    CS = 3  


class spibitbang2(hwdev):
  def __init__(self,config):
    hwdev.__init__(self,config)

  def i2csetget(self,addr,data,reg=None,read=0):
    if read==0: return self.SetSPIbb(reg,data)
    elif read==1: return self.GetSPIbb(reg,data)
    else: logging.warn("Not implemented!")
    return False;

  def SetSPIbb(self,address,value):
         
        CSdev=self.conf['devreg'][SPIBB_pins.CS.value]
        CSpin=self.conf['parameters'][SPIBB_pins.CS.value]
        SDIdev=self.conf['devreg'][SPIBB_pins.SDI.value]
        SDIpin=self.conf['parameters'][SPIBB_pins.SDI.value]
        CLKdev=self.conf['devreg'][SPIBB_pins.CLK.value]
        CLKpin=self.conf['parameters'][SPIBB_pins.CLK.value]

        logging.info(str(("SPIbb2 set",address,value)))

        data2 = (  address << 9 ) + value[0]
        SetI2C=self.conf['parentcls'].SetVarValue

        bit_array = "{0:{fill}16b}".format(data2, fill='0')
      #    print(bit_array)
        SetI2C(CSdev,1,CSpin,[1]) #disable
        SetI2C(CSdev,1,CSpin,[0]) #enable
        for bit in bit_array:
              SetI2C(CLKdev,1,CLKpin,[0]) 
              SetI2C(SDIdev,1,SDIpin,[int(bit)]) 
              SetI2C(CLKdev,1,CLKpin,[1]) 
        SetI2C(CLKdev,1,CLKpin,[0])#Why? 
        SetI2C(CLKdev,1,CLKpin,[1]) 
        SetI2C(CSdev,1,CSpin,[1]) #disable
     #   SetI2C(RCUi,SDIdev,1,SDIpin,[1]) #high when finished
        return True;
  
  def GetSPIbb(self,reg_address,value):
        CSdev=self.conf['devreg'][SPIBB_pins.CS.value]
        CSpin=self.conf['parameters'][SPIBB_pins.CS.value]
        SDOdev=self.conf['devreg'][SPIBB_pins.SDO.value]
        SDOpin=self.conf['parameters'][SPIBB_pins.SDO.value]
        SDIdev=self.conf['devreg'][SPIBB_pins.SDI.value]
        SDIpin=self.conf['parameters'][SPIBB_pins.SDI.value]
        CLKdev=self.conf['devreg'][SPIBB_pins.CLK.value]
        CLKpin=self.conf['parameters'][SPIBB_pins.CLK.value]


        logging.info(str(("SPIbb2 get",reg_address)))
        SetI2C=self.conf['parentcls'].SetVarValue
        GetI2C=self.conf['parentcls'].GetVarValue

        ADC_bytes = 0x00
        #          ADC_rw    = 0x01 # 0 for write, 1 for read
        data =  (reg_address << 1) + 1 #was 7??
          
        SetI2C(CSdev,1,CSpin,[1]) #disable
        SetI2C(CSdev,1,CSpin,[0]) #enable


        bit_array = "{0:{fill}8b}".format(data, fill='0')
        for bit in bit_array:
              SetI2C(SDIdev,1,SDIpin,[int(bit)]) 
              SetI2C(CLKdev,1,CLKpin,[0]) 
              SetI2C(CLKdev,1,CLKpin,[1]) 


          #    print("read byte")
        a=[0]
        N=1 #len(value)
        ret_value=[0]
        for i in range(N): value[i]=0
        for cnt in range(8*(ADC_bytes+1)):
              SetI2C(CLKdev,1,CLKpin,[0]) 
              SetI2C(CLKdev,1,CLKpin,[1])  #read after rising
              GetI2C(SDOdev,1,SDOpin,ret_value) 
#              for i in range(N): value[i]=(value[i]<<1)+ ret_value[i]
              for i in range(N): 
                   if ret_value is None: value[i]=None 
                   elif not value[i] is None: value[i]=(value[i]<<1)+ ret_value[i]
        SetI2C(CLKdev,1,CLKpin,[0]) 
        SetI2C(CSdev,1,CSpin,[1]) #disable
        return True;


