IP_CCD=       '10.99.250.90'
IP_APSCT_TEST='10.99.100.100'
IP_UNKNOWN   ='10.99.250.99'
#IP_APSCT_subrack='10.99.x.100'

import logging
import subprocess

#try:
#from .lmp_id import get_LMP_ID
#except:
#  from lmp_id import get_LMP_ID

import RPi.GPIO as GPIO
def get_LMP_ID():
#    pins=[21,20,16,12,7,8]
    pins=[7,8,16,12,21] #ID5..1, ID0 (GPIO 20)=UNB
    Npins=len(pins);
    GPIO.setmode(GPIO.BCM)
    for i,pin in enumerate(pins):
       GPIO.setup(pin,GPIO.IN)

    value=0;
    for pin in pins:
      value=2*value+1 if GPIO.input(pin) else 2*value
    return value

def get_eth0_ip_ifconfig():
    result=subprocess.run(['ifconfig','eth0'],stdout=subprocess.PIPE).stdout.decode()
    result=result.split('\n')[1].split()
#    print(result)
    if result[0]=='inet': return result[1]
    return None

def get_eth0_ip():
    result=subprocess.run(['grep','static ip_address=10.99','/etc/dhcpcd.conf'],stdout=subprocess.PIPE).stdout.decode()
    for line in result.split('\n'):
      line=line.split(' ')
      if (len(line)==0) or not(line[0]=='static'): continue 
      line=line[1].split('=');
      if len(line)<2: continue
      line=line[1].split('/')[0]
      return line
#    if result[0]=='inet': return result[1]
    return None

def replace_IP(old,new):
  s="sudo sed -i ""s/%s/%s/g"" /etc/dhcpcd.conf" % (old,new)
  logging.info(s)
  subprocess.run(s.split(' '))
  result=subprocess.run(['grep',new,'/etc/dhcpcd.conf'],stdout=subprocess.PIPE).stdout.decode()
  if len(result.split('\n'))!=2:
    logging.error("IP replacement error")
    return
  logging.info("Restart eth0")
  subprocess.run(['sudo','ifconfig','eth0','down'])
#  sleep(1)
  subprocess.run(['sudo','ifconfig','eth0','up'])

  
def check_ip():
 ip_current=get_eth0_ip()
 logging.info("current IP="+str(ip_current))
 ID=get_LMP_ID()
 logging.info("LMP id="+hex(ID))

 ip_new=IP_UNKNOWN #default (CCD)
 if ID==0x3F:  #APSCT in test setup
   ip_new=IP_APSCT_TEST
   logging.info("APSCT test setup, ip=%s"%ip_new)
 if ID in [0,1,2,3]: #APSCT in subrack
   ip_new='10.99.%i.100'%ID
   logging.info("APSCT subrack, ip=%s"%ip_new)
 if ID in [20,21,10]:
   ip_new=IP_CCD
   logging.info("CCD detected, ip=%s"%ip_new)

 if (ip_new!=ip_current) and not(ip_current is None):
   logging.warning("Change IP to %s"%ip_new)
   replace_IP(ip_current,ip_new)
#print(get_value());
if __name__=='__main__':
   logging.getLogger().setLevel('INFO')
   check_ip()
