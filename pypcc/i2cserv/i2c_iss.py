import os
#if os.sys.platform is 'linux':
from usb_iss import UsbIss, defs
import time
import logging
#read=0: write to register
#read=1: read from register
#read=2: write to register (common in group)
#read=3: wait ms second
from .hwdev import hwdev;

class i2c_iss(hwdev):
    def __init__(self,config):
       hwdev.__init__(self,config);
       self.I2Cdev='com'+str(config['parameters'][0]);
       logging.info("iss i2c driver on "+self.I2Cdev)
       try:
           self.iss=UsbIss()
           self.iss.open(self.I2Cdev)
           self.iss.setup_i2c(100,True,None,None)
#           print(self.iss.read_iss_mode())
       except:
           logging.warning("I2C ISS failed!")
           self.iss=None
#       print(self.iss.test(0x20))
       self.I2Ccounter=0

    def i2csetget(self,addr,data,reg=None,read=0):
       logging.debug(str(("I2C",addr,reg,data,read)))
       if self.iss is None: return True #Should be false, only for testing!!!
       try:
#       if True:
              if read==3:
                     time.sleep(data[0]/1000.)
                     return True
              if read==1: #read some data
                     length=len(data)
                     if not(reg is None):
                        #bus.ioctl_write(0,bytes(bytearray([reg])))
 #                       self.iss.i2c.write_ad0(addr,[reg])
                          if reg>255: 
                            reg=[(reg//256)%256,reg%256]
                            time.sleep(0.3)
                            self.iss.i2c.write_ad0(addr,reg)
                            time.sleep(0.3)
                            data[:]=[int(x) for x in self.iss.i2c.read_ad0(addr,length)]
                          else:
                            data[:]=[int(x) for x in self.iss.i2c.read(addr,reg,length)]
#                     time.sleep(0.500)
#                     data[:]=[int(x) for x in bus.ioctl_read(0,length)]
                     else:
                        data[:]=[int(x) for x in self.iss.i2c.read_ad0(addr,length)]
                     logging.debug(str(("I2C get",addr,reg,data,read)))
#         print("I2C read",addr,reg,data,read)
              else:
                    logging.debug(str(("I2C set",addr,reg,data,read)))
                    if reg is None: 
 #                           bus.iaddr_bytes=0
 #                           reg=0;
                         self.iss.i2c.write_ad0(addr,data)
                    else:
                         #TXlen=bus.ioctl_write(reg,bytes(bytearray(data)))
                         self.iss.i2c.write(addr,reg,data)
#                     if TXlen<0:
 #                         bus.close()
#                          return False;
#                     if (TXlen!=len(data)): logging.warn("I2C write error %i/%i"%(TXlen,len(data)))
#              bus.close()
              return True;
       except:
#       else:
#              if bus: bus.close()
              logging.warning("I2C failed!")
              if len(data)>0: data[0]=0
              return False;

