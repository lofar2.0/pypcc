import os
#if os.sys.platform is 'linux':
#import pylibi2c;
import time
import logging
#read=0: write to register
#read=1: read from register
#read=2: write to register (common in group)
#read=3: wait ms second
from .hwdev import hwdev;

from periphery import I2C;

class i2cp(hwdev):
    def __init__(self,config):
       hwdev.__init__(self,config);
       self.I2Cdev='/dev/i2c-'+str(config['parameters'][0]);
       self.i2c=I2C(self.I2Cdev)
       logging.info("i2c driver on port "+str(self.I2Cdev))
       self.I2Ccounter=0

    def i2csetget(self,addr,data,reg=None,read=0):
#      logging.debug(str(("I2C",addr,reg,data,read)))
#       try:
       if True:
         if read==3:
            time.sleep(data[0]/1000.)
            return True
         logging.debug(str(("I2C",addr,reg,data,read)))

         if read==1:
           if not(reg is None):  self.i2c.transfer(addr,[I2C.Message([reg])])
           msgs=[I2C.Message(data,read=True)]
           self.i2c.transfer(addr,msgs)
           data[:]=msgs[0].data
           logging.debug(str(("I2C get",addr,reg,data,read)))
         else:
            if reg is None: 
               msgs=[I2C.Message(data)]
            else:
               msgs=[I2C.Message([reg]),I2C.Message(data)]
            self.i2c.transfer(addr,msgs)
            logging.debug(str(("I2C set",addr,reg,bytes(bytearray(data)),read)))
         return True;
#       except:
       else:
         return False;

