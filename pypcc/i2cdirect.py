#name='RECVTR_LB_TEST' #YAML config file with all register values etc

import logging
#import argparse
from pypcc.opcuaserv import opcuaserv
from pypcc.opcuaserv import i2client
from pypcc.opcuaserv import yamlreader
#from opcuaserv import pypcc2
from pypcc.i2cserv import i2cthread
#import threading
import time
import sys
import signal
from pypcc.yamlconfig import Find;
import pypcc.yamlconfig as yc
from datetime import datetime
from pypcc.opcuaserv.yamlreader import byte2var,var2byte

class i2cdirect():
    def __init__(self,name):
        self.conf=yc.yamlconfig(name)
        self.conf.linkdevices()
        self.conf.loaddrivers()
        self.conf.linkdrivers()
        self.name=self.conf.conf['name']
        self.runmethod(self.name+"_Init")

    def GetVal(self,name):
        varid=self.conf.getvarid(name);
        if varid is None:
            logging.error("Variable "+name+" not found")
            return None,None
        var1=self.conf.getvars()[varid]
        drv=var1.get('drivercls');
        data=drv.OPCUAReadVariable(varid,var1,[])
        data=data[0].data
        return byte2var(var1,data),var1

    def GetRaw(self,name):
        varid=self.conf.getvarid(name);
        if varid is None:
            logging.error("Variable "+name+" not found")
            return None,None
        var1=self.conf.getvars()[varid]
        drv=var1.get('drivercls');
        data=drv.OPCUAReadVariable(varid,var1,[])
        return data,var1


    def SetVal(self,name,data):
        varid=self.conf.getvarid(name);
        if varid is None:
            logging.error("Variable "+name+" not found")
            return None
        var1=self.conf.getvars()[varid]
        drv=var1.get('drivercls');        
        data2,mask=var2byte(var1,data)
        return drv.OPCUASetVariable(varid,var1,data2,[])

    def SetRegister(self,regname,value):
        methodid=self.conf.getmethodid(self.name+"_Init");
        var1=self.conf.getmethod(methodid)
        drv=var1.get('drivercls');
        v1=self.conf.getdevreg(regname)
        drv2=v1.get('drivercls')
        mask=[]
        if drv:  drv.Setdevreg(v1,value,mask)
        elif drv2: drv2.Setdevreg(v1,value,mask)
        else: logging.warn("Driver not specified for instruction"+key)

    def runmethod(self,methodname):
        logging.info("Run method:"+methodname)
        methodid=self.conf.getmethodid(methodname);
        var1=self.conf.getmethod(methodid)
        drv=var1.get('drivercls');
        for inst in var1['instructions']:
            for key,value in inst.items():
                if not(isinstance(value,list)): value=[value]
                logging.info(str(("Run instruction",key,value)));
                if (key=='WAIT'):
                    time.sleep(value[0]/1000.)
                    continue;
                v1=self.conf.getvarid(key)
                if not(v1 is None): 
                    if value[0]=='Update':
                        self.GetVal(key)
                    else:
                        self.SetVal(key,value)
                    continue;
                v1=self.conf.getmethodid(key)
                if v1: 
                    self.runmethod(key)
                    continue;
                v1=self.conf.getdevreg(key)
                if v1:
                    mask=[]
                    drv2=v1.get('drivercls')
                    if value[0]=='Update':
                        if drv:  drv.Getdevreg(v1,mask)
                        elif drv2: drv2.Getdevreg(v1,mask)
                        else: logging.warn("Driver not specified for instruction"+key)
                    else:
                        if drv:  drv.Setdevreg(v1,value,mask)
                        elif drv2: drv2.Setdevreg(v1,value,mask)
                        else: logging.warn("Driver not specified for instruction"+key)

                    continue;
                logging.warn("Unknown instruction "+key)
        return
