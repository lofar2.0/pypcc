import setuptools

import subprocess
def _get_version_hash():
    """Talk to git and find out the tag/hash of our latest commit"""
    try:
#        p = subprocess.Popen(["git", "rev-parse","HEAD"],
#        p = subprocess.Popen(["git", "show","-s","--format=%ci"],
#        p = subprocess.Popen(["git", "log","-1","--format='%H'"],
        p = subprocess.Popen(["git", "log","-1","--format='%ct %h'","--abbrev-commit"],
                             stdout=subprocess.PIPE)
    except EnvironmentError:
        print("Couldn't run git to get a version number for setup.py")
        return

    ver = p.communicate()[0]
    ver = ver.decode('ascii')[1:-2].split()
    from datetime import datetime
    ver="%s_%s"%(datetime.utcfromtimestamp(int(ver[0])).isoformat(),ver[1])
#    ver=ver.strip()
#    ver=str(ver)[2:-1]
    print("Git hash=",ver)
    return ver

#with open('requirements.txt') as f:
#    required = f.read().splitlines()
# Requires: setup.cfg
setuptools.setup(description=_get_version_hash(),
    install_requires=["asyncua",
    "numpy",
    "recordclass",
    "pyyaml",
    "python-logstash-async",
    "importlib-resources",
    ])

