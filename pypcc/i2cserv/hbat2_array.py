import numpy as np
import logging
from pypcc.queuetypes import *
from .hwdev import hwdev
from .i2c_dev import i2c_dev

class hbat2_array(i2c_dev):
    def __init__(self,config):
        i2c_dev.__init__(self,config);
        self.addr=config['parameters'];
        self.N=len(self.addr);
        self.I2Cmask=[0]*self.N
#        self.disableI2ConError=True;
        self.I2Cmaskid=config.get('maskid',None)
        if self.I2Cmaskid is None: logging.warn(config['name']+" mask not found!")

