from test_common import *
import numpy as np
from time import sleep
RCU=[0,1,2,3];
On=[True,True,True]
N=12;

name="RCU_PWR_ANT_on"

#exit()
connect()
setAntmask(RCU,[True,True,True])
#setRCUmask(RCU):

AntPwr=get_value(name+"_R")
print(AntPwr)
try:
  while True:
    Rnd=np.random.rand(N)
    for x in range(N):
      AntPwr[x]=True if Rnd[x]>0.5 else False
    print(AntPwr)
    set_value(name+"_RW",AntPwr)
    sleep(np.random.rand(1)*10)
finally:
 disconnect();
exit()

disconnect()