import os
#if os.sys.platform is 'linux':
import pylibi2c;
import time
import logging
#read=0: write to register
#read=1: read from register
#read=2: write to register (common in group)
#read=3: wait ms second
from .hwdev import hwdev;

class i2c(hwdev):
    def __init__(self,config):
       hwdev.__init__(self,config);
       self.I2Cdev='/dev/i2c-'+str(config['parameters'][0]);
       logging.info("i2c driver on port "+str(self.I2Cdev))
       self.I2Ccounter=0

    def i2csetget(self,addr,data,reg=None,read=0):
#       logging.debug(str(("I2C",addr,reg,data,read)))
       bus=None;
       try:
#       if True:
              if read==3:
                     time.sleep(data[0]/1000.)
                     return True

#       return True;
              bus=pylibi2c.I2CDevice(self.I2Cdev,addr)
              if read==1:
                     length=len(data)
                     bus.iaddr_bytes=0
                     if not(reg is None):
                            bus.ioctl_write(0,bytes(bytearray([reg])))
#                     time.sleep(0.500)
                     data[:]=[int(x) for x in bus.ioctl_read(0,length)]
                     logging.debug(str(("I2C get",addr,reg,data,read)))
#         print("I2C read",addr,reg,data,read)
              else:
                     if reg is None: 
                            bus.iaddr_bytes=0
                            reg=0;
                     logging.debug(str(("I2C set",addr,reg,bytes(bytearray(data)),read)))
                     TXlen=bus.ioctl_write(reg,bytes(bytearray(data)))
                     if TXlen<0:
                          bus.close()
                          return False;
                     if (TXlen!=len(data)): logging.warn("I2C write error %i/%i"%(TXlen,len(data)))
              bus.close()
              return True;
       except:
#       else:
              if bus: bus.close()
              logging.debug("I2C failed!")
              if len(data)>0: data[0]=0
              return False;

