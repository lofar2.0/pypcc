#!/bin/bash
loc_id=`/root/setip/getip.py`
if [ -z  "$loc_id" ]
then
  echo "error"
else
  echo ifconfig eth0 $loc_id netmask 255.255.0.0
  sleep 1
  ifconfig eth0
  ifconfig -v eth0 down
  ifconfig -v eth0 $loc_id netmask 255.255.0.0
  ifconfig -v eth0 up
  ifconfig eth0
fi
