from pypcc.i2cdirect import i2cdirect
from time import sleep

import logging
logging.basicConfig(level="ERROR",format='%(asctime)s [%(levelname)-8s,%(filename)-20s:%(lineno)-3d] %(message)s')
#logging.basicConfig(level="DEBUG",format='%(asctime)s [%(levelname)-8s,%(filename)-20s:%(lineno)-3d] %(message)s')

d1=i2cdirect('RECVTR_LB_TEST')

def RCU_off(d1):
 logging.warning("Switch RCU Power off");
 d1.SetRegister("IO1.GPIO1",[0])
 d1.SetRegister("IO1.GPIO2",[0x0]) #Analog power off
 d1.SetRegister("IO2.GPIO1",[0x0]) #Digital power off
 d1.SetRegister("IO2.GPIO2",[0x0])
 d1.SetRegister("IO3.GPIO1",[0x0]) 
 d1.SetRegister("IO3.GPIO2",[0x0])

 d1.SetRegister("IO1.CONF1",[0])
 d1.SetRegister("IO1.CONF2",[0])
 d1.SetRegister("IO2.CONF1",[0x80]) #Pgood on P07
 d1.SetRegister("IO2.CONF2",[0])

def RCU_on(d1):
 logging.warning("Switch RCU Power on");
 d1.SetRegister("IO2.CONF1",[0x80]) #Pgood on P07
 d1.SetRegister("IO2.GPIO1",[0x4A]) #Digital power on
 d1.SetRegister("IO2.GPIO2",[0x55])
 d1.SetRegister("IO3.GPIO1",[0x15]) #ADC_SDIO=high, clk=low,  DTH_EN=low
 d1.SetRegister("IO3.GPIO2",[0x47]) #ADC SC=high, DTH_SDA=high
 d1.SetRegister("IO1.GPIO1",[0x0A])
 d1.SetRegister("IO1.GPIO2",[0x8A]) #Analog power on
 d1.SetRegister("IO2.CONF2",[0])
 d1.SetRegister("IO3.CONF1",[0]) #All output
 d1.SetRegister("IO3.CONF2",[0])
 d1.SetRegister("IO3.DIR1",[0]) #All output
 d1.SetRegister("IO3.DIR2",[0])
 d1.SetRegister("IO1.CONF1",[0])
 d1.SetRegister("IO1.CONF2",[0])

def ADC_config(d1):
 logging.warning("Configure ADCs");
 d1.SetRegister("ADC1.JESD_control1",[0x14])
 d1.SetRegister("ADC1.SYNC_control",[1])
 d1.SetRegister("ADC1.CML_level",[0x7])
 d1.SetRegister("ADC1.dither",[0x0])
 d1.SetRegister("ADC1.Update",[1])
 d1.SetRegister("ADC2.JESD_control1",[0x14])
 d1.SetRegister("ADC2.SYNC_control",[1])
 d1.SetRegister("ADC2.CML_level",[0x7])
 d1.SetRegister("ADC2.dither",[0x0])
 d1.SetRegister("ADC2.Update",[1])
 d1.SetRegister("ADC3.JESD_control1",[0x14])
 d1.SetRegister("ADC3.SYNC_control",[1])
 d1.SetRegister("ADC3.CML_level",[0x7])
 d1.SetRegister("ADC3.dither",[0x0])
 d1.SetRegister("ADC3.Update",[1])

def enable_ant_pwr(d1):
 logging.warning("Switch antenna output power on");
 d1.SetVal("CH1_PWR_ANT_on",[True])
 d1.SetVal("CH2_PWR_ANT_on",[True])
 d1.SetVal("CH3_PWR_ANT_on",[True])

def dither_config(d1): #Switch dither on
 logging.warning("Configure Dither sources");
 d1.SetVal("CH1_DTH_freq",[101000000])
 d1.SetVal("CH1_DTH_PWR",[-10])
 d1.SetRegister("DTH1.CONF",[0,0,0])
 d1.SetRegister("DTH1.Tune",[0,0])
 d1.SetRegister("DTH1.Start",[0,1,0,0,1])
 d1.SetVal("CH2_DTH_freq",[102000000])
 d1.SetVal("CH2_DTH_PWR",[-10])
 d1.SetRegister("DTH2.CONF",[0,0,0])
 d1.SetRegister("DTH2.Tune",[0,0])
 d1.SetRegister("DTH2.Start",[0,1,0,0,1])
 d1.SetVal("CH3_DTH_freq",[103000000])
 d1.SetVal("CH3_DTH_PWR",[-10])
 d1.SetRegister("DTH3.CONF",[0,0,0])
 d1.SetRegister("DTH3.Tune",[0,0])
 d1.SetRegister("DTH3.Start",[0,1,0,0,1])


#RCU_off(d1)
#sleep(1.0)
#RCU_on(d1)
#sleep(1.0)
#ADC_config(d1)
#if False:
#    d1.runmethod("RCU_off");
#    sleep(1.0)
if True:
    d1.runmethod("RCU_on");
    sleep(1.0)
if True:
    d1.runmethod("ADC_on");
#enable_ant_pwr(d1)
#dither_config(d1)


for varname in ['RCU_PCB_ID','RCU_PCB_version','RCU_PCB_number',
            'RCU_PWR_good','RCU_PWR_DIGITAL_on','RCU_PWR_ANALOG_on','RCU_DTH_shutdown',
            'RCU_IO1_GPIO1','RCU_IO1_GPIO2','RCU_IO2_GPIO1','RCU_IO2_GPIO2',
            "RCU_LED_red_on","RCU_LED_green_on"]:
    data,var1=d1.GetVal(varname);
    print(varname+"=",data[0])

for varname in ['RCU_TEMP','RCU_PWR_3V3','RCU_PWR_2V5','RCU_PWR_1V8']:
    data,var1=d1.GetVal(varname);
    print(varname+"=",data[0])

for ch in ['CH1','CH2','CH3']:
 print("Channel ",ch)
 for varname in ['band_select','attenuator_dB','ADC_shutdown',
     'PWR_ANT_on','PWR_ANT_VOUT','PWR_ANT_VIN','PWR_ANT_IOUT',
     'ADC_locked','ADC_sync','ADC_JESD','ADC_CML_level',
     'DTH_freq','DTH_PWR','DTH_on',
     ]:
    data,var1=d1.GetVal(ch+'_'+varname);
    print("  ",varname+"=",data[0])
