#import serial
import hbat1 as hb
from time import sleep
from datetime import datetime

#Register Definitions
HBA_X=0
HBA_Y=1
HBA_PWR=2
#HBA_PWR=0xB
HBA_ID=0x4 
HBA_VERSION=0x0C
HBA_PCB_VERSION=0x20
HBA_PCB_NUMBER=0x30
HBA_ADDR=0x10
HBA_V=0x50
HBA_VSENSE=0x54
HBA_SAVE_EEPROM=0x40
HBA_LOAD_EEPROM=0x41
Debug=False

def GetPackets(ser,Debug=False):
  D=hb.GetDelay(ser);
  D2=hb.NormDelays(D)
  #print("Received delays:",D2[:10])
  NoData=True;
  S=hb.Decode(D2)
  RXdata=[]
  #print("Received packets:",S)
  while len(S)>0:
    NoData=False;
    L=S[1]
    S1=S[:L+3]
    CRC=hb.CRCcheck(S1);
    if Debug: print("Received packet:",S1,"CRC=",CRC)
    if (CRC==0) and (S1[0]>0x80):
         #print("Reply from Addr=",S1[0]-0x80," data=",S[2:-2])
         RXdata=S[2:-2]   
         if Debug: print("Reply from Addr=",S1[0]-0x80," data=",[hex(a) for a in RXdata])
    S=S[L+3:]
  if NoData: print("Communication error!")
  return RXdata;

def string2hex(D1):    
        s='0x'
        for d in D1[::-1]: s+=hex(d)[2:]
        return s
class dante():
    def __init__ (self,SetReg,GetReg):
        #self.ser = serial.Serial(serial_port,115200,timeout=0.3)  # open serial port
        self.delay={}
        self.LED={}
        self.term_on={}
        self.dab_on={}
        self.SetRegisters=SetReg
        self.RequestRegisters=GetReg
        
        #print("Serial port:",self.ser.name)         # check which port was really used
        
#    def SetRegisters(self,reg,data,addr=None):
#        func=len(data)*2;
#        if addr is None: addr=self.addr
#        TX1=hb.MakeRequest(addr,data,func,reg);
#        if Debug: print("Packet to TX",TX1)
#        TX2=hb.ManchesterEncode(TX1)
#        self.ser.write(bytearray(TX2))
#        GetPackets(self.ser);

#    def RequestRegisters(self,reg,length,addr=None):
#        func=length*2+1;
#        if addr is None: addr=self.addr
#        TX1=hb.MakeRequest(addr,[],func,reg);
#        if Debug: print("Packet to TX",TX1)
#        TX2=hb.ManchesterEncode(TX1)
#        self.ser.write(bytearray(TX2))
#        return GetPackets(self.ser);
    
    def power_on(self,addr=None):
        self.SetRegisters(HBA_PWR,[0x0F],addr=addr);

    def power_off(self,addr=None):
        self.SetRegisters(HBA_PWR,[0x0],addr=addr);
        

    def get_UID(self,addr=None):
        return string2hex(self.RequestRegisters(HBA_ID,8,addr=addr));
    def get_PCB_version(self,addr=None):
        return bytearray(self.RequestRegisters(HBA_PCB_VERSION,16,addr=addr)).decode('utf8');

    def get_PCB_number(self,addr=None):
        return bytearray(self.RequestRegisters(HBA_PCB_NUMBER,16,addr=addr)).decode('utf8');

    def getvoltage(self,x,addr=None):
        D1=self.RequestRegisters(0x30+x,0,addr=addr);
        if len(D1)==0:
            sleep(0.1)
            D1=self.RequestRegisters(0x30+x,0,addr=addr);
        return D1[0]*256+D1[1] if len(D1)>=2 else 0 
    def getCurrent(self,addr=None):
        senseA=self.getvoltage(0,addr=addr)
        senseB=self.getvoltage(1,addr=addr)
        return (senseB-senseA)#*0.016666    
    
    def set_delay(self,val,addr=None):
        if addr is None: addr=self.addr;
        self.delay[addr]=val;
        self.update_shift(addr=addr)

    def set_LED(self,val,addr=None):
        if addr is None: addr=self.addr;
        self.LED[addr]=val;
        self.update_shift(addr=addr)

    def set_term(self,val,addr=None):
        if addr is None: addr=self.addr;
        self.term_on[addr]=val;
        self.update_shift(addr=addr)

    def set_DAB_filter(self,val,addr=None):
        if addr is None: addr=self.addr;
        self.dab_on[addr]=val;
        self.update_shift(addr=addr)

        
    def update_shift(self,addr=None):
        val=self.delay.get(addr,15)*4
        if not(self.term_on.get(addr,False)): val+=0x80
        self.SetRegisters(HBA_X,[val+self.dab_on.get(addr,False)*1,val+self.LED.get(addr,False)*1],addr=addr)
  