# PyPCC
Python OPC-UA server to control the I2C devices in the LTS. 

+ pypcc2.py: OPC-UA server that expose (visible) variables and methods.

# Prerequisites

## Requirements

This Python3 code uses other Python3 modules:

- opcua
- recordclass
- yaml
- numpy

We recommend to install a virtual environment and then install the dependencies there.  For convenience we have added the relevant modules to the file `requirements.txt` that can be used to install them with pip3: `python3 -m pip install -r requirements.txt`

Below are step-by-step instructions that show how to install the dependencies neatly in a Python3 virtual environment.

```bash
# Create a Python3 virtual environment with the name "env"
python3 -m venv env
# Activate the virtual environment
. env/bin/activate
# Update the already installed modules in the virtual environment since they are usually outdated
python3 -m pip install --upgrade pip wheel
# Deactivate the virtual environment and start it again
deactivate
. env/bin/activate
# And finally install the module dependencies
python3 -m pip install -r requirements.txt
```
Notes for using python3.11:
- install crypto 3.3 for asyncua
- add #define PY_SSIZE_T_CLEAN before #include <Python.h> in pyi2c.c in libi2c.

# Install it
The software is installed by executing: `python3 setup.py install`

# Execute it

The software can be executed using commandline parameters, for example specifying the config yaml file (RECVTR.yaml) and OPC-UA port (4840):  `hwtr -c RECVTR -p 4840`

Alternativly the 4 hardware translators can be executed, using it's default parameters as:
- `apscttr`
- `recvtr`
- `unb2tr`
- `apsputr`

# Install it as a service
The translators can also be installed as services:
- `cp bin/*.service /lib/systemd/system/.`
- `systemctl daemon-reload`
- `systemctl enable recvtr.service` so it is startup at boot
- `systemctr start recvtr.service` start the translator

The log can then be monited by `journalctl -f -u recvtr.service`

```bash
hwtr --help
usage: hwtr [-h] [-s] [-t] [-p PORT] [-l {DEBUG,INFO,WARNING,ERROR}] [-c CONFIG]

optional arguments:
  -h, --help            show this help message and exit
  -s, --simulator       Do not connect to I2c, but simulate behaviour.
  -t, --test            Do not start OPC-UA server.
  -p PORT, --port PORT  Port number to listen on [4842].
  -l {DEBUG,INFO,WARNING,ERROR}, --loglevel {DEBUG,INFO,WARNING,ERROR}
                        Log level [INFO].
  --loghost LOGHOST     Logstash host to which to forward logs []                    
  -c CONFIG, --config CONFIG
                        YAML config files, comma seperated [RCU]

```

