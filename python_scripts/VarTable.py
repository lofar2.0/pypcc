import pypcc.yamlconfig as yc
import sys
if len(sys.argv)<2: 
  print("Usage: VarTable yamlfile")
  exit()
RW={'ro':'R','rw':'RW','variable':'RW'}
Masked=True;
Y=yc.yamlconfig(sys.argv[1])
sep=','
#Sort alphabetically
names=[(v['name']).upper() for v in Y.getvars()]
SI=sorted(range(len(names)), key=lambda k: names[k])
if Masked:
 print("Name                        "+sep+"Dim     "+sep+"Type   "+sep+"R/W   "+sep+"Mask            "+sep+"Mon  "+sep+"Description")
else:
 print("Name                   "+sep+"Type   "+sep+"R/W   "+sep+"Mon  "+sep+"Description")
for i0 in SI:
    v=Y.getvar1(i0)
#for v in Y.getvars():
    if v.get("debug",False): continue;
    if v.get("rw",False)=='hidden': continue;

    dim=v.get('dim',1)
    if dim>1:
       dim2=v.get('dim2',None)
       if not(dim2 is None):
         dimS='[%i,%i]' % (dim2[0],dim2[1])
       else: 
         dimS='[%i]' % dim
    else: dimS='[1]'

    S=('%-28s' % v['name'])+sep
    if Masked: S+=('%-8s' % dimS)+sep
    S+=('%-7s' % str(v['dtype']))+sep
    S+=('%-6s' % RW[v['rw']])+sep
    if Masked: S+=('%-16s' % v.get('mask',''))+sep
    S+=('%-5s' % str(v.get('monitor','')))+sep
    S+=str(v.get('description',''))
    print(S)
print()
if Masked:
  print(('%-23s'% "Name")+sep+('%-9s'% "Mask")+sep+"Description")
else:
  print(('%-23s'% "Name")+sep+"Description")
for v in Y.conf['methods']:
    if v.get("debug",False): continue;
    S=('%-23s' % v['name'])+sep
#    S+=str(v['dim'])+sep
#    S+=str(v['dtype'])+sep
#    S+=RW[v['rw']]+sep
    if Masked: S+=('%-9s' % v.get('mask',''))+sep
#    S+=str(v.get('monitor',''))+sep
    S+=str(v.get('description',''))
    print(S)
