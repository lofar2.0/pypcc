FROM ubuntu:20.04

# Just copy everything
COPY . /pypcc

RUN apt-get update && apt-get install -y python3 python3-pip python3-yaml git && \
    pip3 install -r /pypcc/requirements.txt

WORKDIR /pypcc

# install pypcc package
RUN python3 setup.py install
