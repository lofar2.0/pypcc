import logging
import argparse
from opcuaserv import opcuaserv
from opcuaserv import i2client
from opcuaserv import yamlreader
#from opcuaserv import pypcc2
from i2cserv import i2cthread
import threading
import time
import sys
import signal
from yamlconfig import Find;

logging.basicConfig(level="DEBUG",format='%(asctime)s [%(levelname)-8s,%(filename)-20s:%(lineno)-3d] %(message)s')

RunTimer=True;
#def signal_handler(sig, frame):
#    logging.warn('Stop signal received!')
#    global RunTimer; 
#    RunTimer=False
#signal.signal(signal.SIGINT, signal_handler)

logging.info("Start I2C processes")   
threads=[]
I2Cclients=[]
name='UNB2'
RCU_I2C=i2client.i2client(name=name)
thread1=i2cthread.start(*RCU_I2C.GetInfo())
threads.append(thread1)
I2Cclients.append(RCU_I2C)

#Load yaml so that we know the variable names
RCU_conf=yamlreader.yamlreader(RCU_I2C,yamlfile=name)
var1=RCU_conf.getvarid('UNB2_EEPROM_Unique_ID');
N=2;
mask=[i<1 for i in range(N)];
print("mask=",mask);
#RCU_I2C.readvar(var1,mask);

points=["UNB2_POL_SWITCH_1V2_VOUT","UNB2_POL_SWITCH_1V2_TEMP"]
#points=UNB2_POL_SWITCH_1V2_IOUT,UNB2_POL_SWITCH_1V2_TEMP,UNB2_POL_SWITCH_PHY_VOUT,UNB2_POL_SWITCH_PHY_IOUT,UNB2_POL_SWITCH_PHY_TEMP]
N=2;
mask=[i<1 for i in range(N)];
print("mask=",mask);
for  p in points:
  var1=RCU_conf.getvarid(p);
#  RCU_I2C.readvar(var1,mask);

N=8;
mask=[i<4 for i in range(N)];
print("mask=",mask);
var1=RCU_conf.getvarid("UNB2_FPGA_POL_CORE_IOUT")
#RCU_I2C.readvar(var1,mask);


N=48;
mask=[i<15 for i in range(N)];
print("mask=",mask);
var1=RCU_conf.getvarid("UNB2_FPGA_QSFP_CAGE_TEMP")
#RCU_I2C.readvar(var1,mask);

N=16;
mask=[i<2 for i in range(N)];
print("mask=",mask);
var1=RCU_conf.getvarid("UNB2_FPGA_DDR4_SLOT_PART_NUMBER")
RCU_I2C.readvar(var1,mask);

#var1=RCU_conf.getvarid('UNB2_FPGA_QSFP_CAGE_LOS');
#N=48;
#mask=[i<8 for i in range(N)];
#print("mask=",mask);
#RCU_I2C.readvar(var1,mask);

#var1=RCU_conf.getmethodid('RCU_on');
#N=32;
#mask=[i<2 for i in range(N)];
#RCU_I2C.callmethod(var1,mask)

time.sleep(2);

while RCU_I2C.data_waiting():
    varid,data,mask=RCU_I2C.readdata()
    print("Results:",RCU_conf.getvar1(varid)['name'],data)

logging.info("Stop threads")
for i2c in I2Cclients:
    i2c.stop()
for thread1 in threads:
    thread1.join()
