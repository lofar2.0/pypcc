from importlib import metadata
import argparse
import logging
import signal
import sys
import threading
import time
from pypcc.simpleflock import simpleflock

parser = argparse.ArgumentParser()
parser.add_argument("-s", "--simulator", help="Do not connect to I2c, but simulate behaviour.", action="store_true")
parser.add_argument("-t", "--test", help="Do not start OPC-UA server.", action="store_true")
parser.add_argument("-p", "--port", help="Port number to listen on [%(default)s].", type=int, default=4842)
parser.add_argument("-l", "--loglevel", help="Log level [%(default)s].", type=str, choices=["DEBUG","INFO","WARNING","ERROR"], default="WARNING")
parser.add_argument("--loghost", help="Logstash host to which to forward logs [%(default)s]",type=str, default='')
parser.add_argument("--lockfile", help="Lock while using I2C [%(default)s]",type=str, default='')
parser.add_argument("-c", "--config", help="YAML config files, comma seperated [%(default)s]",type=str, default='RCU')
args = parser.parse_args()

#Todo test logstash
#from logconfig import configure_logger
#log_extra = {
#    "simulator": args.simulator, 
#    "test": args.test,
#    "port": args.port,
#    "config": args.config,
#    "lofar_id": f"pypcc - {args.config}",
#}
#configure_logger(logstash_host=args.loghost,level=args.loglevel, log_extra=log_extra)
logging.getLogger().setLevel(args.loglevel)
git_hash=metadata.metadata("pypcc")["Summary"]
logging.info("Git Hash="+git_hash)

from pypcc.opcuaserv import opcuaserv
from pypcc.opcuaserv import i2client
from pypcc.opcuaserv import yamlreader
#from opcuaserv import pypcc2
from pypcc.i2cserv import i2cthread

RunTimer=True;
def signal_handler(sig, frame):
    logging.warn('Stop signal received!')
    global RunTimer; 
    RunTimer=False
signal.signal(signal.SIGINT, signal_handler)

#Start i2c processes as soon as possible to have minimum duplication
logging.info("Start I2C processes")   

if not(args.simulator):
  logging.info("Make I2C lock")   
  import multiprocessing as mp
#  lock=mp.Lock()
#else:
#lock=None;
lock = None if args.lockfile=='' else simpleflock(args.lockfile,10)

#I2Cports=['UNB2','RCU','CLK']
#I2Cports=['RCU']
I2Cports=[x for x in args.config.split(',')]
I2C_process=[]
I2Cclients=[]
for name in I2Cports:
    RCU_I2C=i2client.i2client(name=name) 
    if not(args.simulator):  
        thread1=i2cthread.start(*RCU_I2C.GetInfo(),lock=lock) #Start a new I2C process
        I2C_process.append(thread1)
    I2Cclients.append(RCU_I2C)
#Initialise OPCUA server and load variables
logging.info("Initialised OPC-UA Server")   
configs=[]
I2C_threads=[]
if not(args.test):  
    handler=opcuaserv.InitServer(port=args.port)
    logging.info("Load OPCUA variables & start i2c listing thread")   
    opcuaserv.AddVarR("TR_software_version_R",git_hash+"_"+args.config,[],False)
    for i,name in enumerate(I2Cports):
        RCU_I2C=I2Cclients[i]
        RCU_conf=yamlreader.yamlreader(RCU_I2C,yamlfile=name)
        RCU_conf.AddVars(opcuaserv.AddVarR,opcuaserv.AddVarW)
        RCU_conf.AddMethod(opcuaserv.Addmethod)
        RCU_conf.CallInit();
        configs.append(RCU_conf);

        thread2=threading.Thread(target=RCU_conf.getvar); #Thread on OPC-UA side of pipe
        thread2.start()
        I2C_threads.append(thread2)
    time.sleep(1)
    logging.info("Start OPC-UA server")
    opcuaserv.start()
logging.getLogger().setLevel("WARNING")

if False:
   opcuaserv.server.stop()
   exit()

def stop_process(thread):
  thread.join(10)
  if thread.exitcode is None:
    logging.warning("Kill thread")
    thread.kill()
    thread.join(1)
  if thread.exitcode is None:
    logging.warning("Terminate thread")
    thread.terminate() 
  return thread.exitcode

def restart_I2C(i):
              logging.warning("Restarting I2C process!")
              RCU_I2C=I2Cclients[i];
              RCU_I2C.restart() #make new Queues
              stop_process(I2C_process[i])

              logging.getLogger().setLevel(args.loglevel)
              logging.info("Start new I2C process")
              thread1=i2cthread.start(*RCU_I2C.GetInfo(),lock=lock) #Start a new I2C process
              I2C_process[i]=thread1

              RCU_conf=configs[i]
              logging.info("Wait for I2C thread")
              I2C_threads[i].join()
              logging.info("Start new I2C thread")
              RCU_conf.CallInit()
              thread2=threading.Thread(target=RCU_conf.getvar) #start new I2C thread
              thread2.start()
              I2C_threads[i]=thread2

              RCU_conf.lastRecv=time.time()
              logging.getLogger().setLevel("WARNING")
#cnt=0
try:
 while RunTimer:
    if not(args.test): 
        handler.check_datachange(0.1);
    else:
        time.sleep(0.1);
    for c in configs:
        if c.Monitor(): continue
#    if not(args.simulator) and not(args.test):  
#        for i,c in enumerate(configs):
#           timeout=c.watchdog()
#           if timeout>10:
#                 print(i,timeout)
#           if timeout>60:
#              logging.error("I2C timeout!")
#               restart_I2C(i)
#              RunTimer=False
#    cnt+=1;
#    if cnt==60*10*2: 
#      print("****** restart test *****")
#      restart_I2C(0)
          
finally:
   if not(args.test):
       logging.info("Stop OPC-UA server")
       opcuaserv.server.stop()

logging.info("Stop threads")
for i2c in I2Cclients:
    i2c.stop()
for thread1 in I2C_process:
    stop_process(thread1)
for thread1 in I2C_threads:
    thread1.join()
