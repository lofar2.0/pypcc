from test_common import *
import numpy as np
from time import sleep
RCU=[0,1,2,3];
N=4;

name1="RCU_LED_red_on"
name2="RCU_LED_green_on"

#exit()
connect()
setRCUmask(RCU)

LED1=get_value(name1+"_R")
LED2=get_value(name2+"_R")
print(LED1,LED2)
try:
  while True:
    Rnd=np.random.rand(N)
    for x in range(N):
      LED1[x]=True if Rnd[x]>0.5 else False
    Rnd=np.random.rand(N)
    for x in range(N):
      LED2[x]=True if Rnd[x]>0.5 else False
    print(LED1,LED2)
    set_value(name1+"_RW",LED1)
    set_value(name2+"_RW",LED2)
    sleep(np.random.rand(1)*10)
finally:
 disconnect();
exit()

disconnect()