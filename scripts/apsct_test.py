from test_common import *
from time import sleep
connect("opc.tcp://localhost:4843/")

if True:
  print("Switch off")
  callmethod("APSCT_off")
  sleep(0.5)
  wait_not_busy("APSCTTR_translator_busy_R",timeout_sec=3)

if True:
  print("Setup 160MHz")
  callmethod("APSCT_160MHz_on")
  sleep(0.5)
  wait_not_busy("APSCTTR_translator_busy_R",timeout_sec=3)

if False:
  print("Setup 200MHz")
  callmethod("APSCT_200MHz_on")
  sleep(0.5)
  wait_not_busy("APSCTTR_translator_busy_R",timeout_sec=3)

if True:
  print("Clear lol")
  set_value("APSCT_PLL_clr_lol_RW",True)
  sleep(0.1)
  set_value("APSCT_PLL_clr_lol_RW",False)

if False:
  print("Get monitor values")
  set_value("APSCTTR_monitor_rate_RW",1)
  sleep(3)
  set_value("APSCTTR_monitor_rate_RW",30)


names=get_all_variables()
for name in names:
  att=get_value(name)
  print(name,[att])

disconnect();
