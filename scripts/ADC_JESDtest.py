from test_common import *
connect()

#name="RCU_ADC_JESD3"
name="RCU_ADC_testsignal"
#RCU=[0];
RCU=[x for x in range(32)];
Att=[False]*3 #default
Att=[True]*3
#RCU=[0,1,2,3,16,17,18,19];
#Att=[0x24]*3 #8-bit 0010, 0100 PN9 
#Att=[10,10,10]


setAntmask(RCU)

wait_not_busy("RECVTR_translator_busy_R",10)
att=get_debug_value(name+"_R")
print("JESD old:",att[:15])

#for r in RCU:
#  att[3*r:3*r+3]=Att
#print("JESD set:",att[:15])
#set_value(name+"_RW",att)
if True:
 callmethod("RCU_ADC_testsignal_on")

 time.sleep(0.5)
 wait_not_busy("RECVTR_translator_busy_R",10)
 att=get_debug_value(name+"_R")
 
 print("JESD new:",att[:15])

disconnect()