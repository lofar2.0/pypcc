from test_common import *

name="RCU_PWR_ANT_on"
RCU=[5];
On=[False,False,True]
#On=[False,False,False]
#Att=[10,10,10]
#RCU=[1,2,3];
#Att=[0,0,0]

connect()
setAntmask(RCU)
setRCUmask(RCU)

att=get_value(name+"_R")
print("Att old:",att[:18])
for r in RCU:
  att[3*r:3*r+3]=On
print("Att set:",att[:18])
set_value(name+"_RW",att)

time.sleep(0.5)
att=get_value(name+"_R")
print("Att new:",att[:18])

disconnect()