#!/bin/bash
sudo systemctl stop recvtr.service
sudo systemctl stop apscttr.service
sudo systemctl stop apsputr.service
sudo systemctl stop unb2tr.service
sudo systemctl stop ccdtr.service
sudo systemctl disable recvtr.service
sudo systemctl disable apscttr.service
sudo systemctl disable apsputr.service
sudo systemctl disable unb2tr.service
sudo systemctl disable ccdtr.service
