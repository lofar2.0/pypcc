from .hba1 import hba1;
import logging
import signal
try:
  import RPi.GPIO as GPIO
except:
  GPIO=False;
  
class gpio_hba_trigger(hba1):
  def __init__(self,config):
    hba1.__init__(self,config);
    self.pin=config['parameters'][0];
    self.addr=config['devreg'][0]['addr']
    self.reg=config['devreg'][0]['register_R']
    logging.info("HBAT wait for PPS on pin %i, TX=%i:%i",self.pin,self.addr,self.reg)
    if GPIO:
      GPIO.setmode(GPIO.BCM)
      GPIO.setup(self.pin,GPIO.IN)
    else:
      logging.warn("RPi GPIO module not found, PPS input disable!")

  def OPCUASetVariable(self,varid,var1,data,mask):
       logging.info(str(("HBA set Var",var1['name'],data[:32*3],mask[:12])))
       self.conf['parentcls'].SetGetVarValueMask(var1,data,mask,getalso=False)
       #Wait for PPS if required else wait a bit
       channel=GPIO.wait_for_edge(self.pin,GPIO.RISING,timeout=1500)
       self.conf['parentcls'].i2csetget(self.addr,[],self.reg,0)
       if channel is None:
         logging.info("PPS not received!");
#         return False;
       if var1.get('wait'):
         logging.debug("Wait %i ms",var1.get('wait')) 
         sleep(var1['wait']/100.)
       data,mask2=self.conf['parentcls'].GetVarValueMask(var1,mask)
       Data=OPCUAset(varid,InstType.varSet,data.copy(),mask2.copy())
       return [Data]

#  def i2csetget(self,addr,data,reg=None,read=0):
#    if (read==0) and GPIO:
#    self.conf['parentcls'].i2csetget(addr,data,reg,read)
    
    
