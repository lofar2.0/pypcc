##NB translator should be off or monitor should be 0
name='RECVTR_HB' #YAML config file with all register values etc
varID='RCU_PCB_ID'

logFile='SetVersion'

import logging
import argparse
from pypcc.opcuaserv import opcuaserv
from pypcc.opcuaserv import i2client
from pypcc.opcuaserv import yamlreader
#from opcuaserv import pypcc2
from pypcc.i2cserv import i2cthread
import threading
import time
import sys
import signal
from pypcc.yamlconfig import Find;
import pypcc.yamlconfig as yc
from datetime import datetime
import numpy as np

testtime=datetime.now().strftime("%y-%m-%d %H:%M")
if len(sys.argv)<3:
  print("setVersion RCUnr RCUsi HBAnr");
  print("e.g. SetVersion 8 1 1")
#  exit();

RCUNR=int(sys.argv[1]);
SINR=int(sys.argv[2]);
HBANR=int(sys.argv[3]);
print(RCUNR,SINR,HBANR)
logging.basicConfig(level="WARNING",format='%(asctime)s [%(levelname)-8s,%(filename)-20s:%(lineno)-3d] %(message)s')
#logging.basicConfig(level="DEBUG",format='%(asctime)s [%(levelname)-8s,%(filename)-20s:%(lineno)-3d] %(message)s')

RunTimer=True;
conf=yc.yamlconfig(name)
conf.linkdevices()
conf.loaddrivers()
conf.linkdrivers()

def GetVal(name,N=1):
 varid=conf.getvarid(name);
# print("varid",varid)
 var1=conf.getvars()[varid]
 dim=var1['dim']
 drv=var1.get('drivercls');
 mask=[False]*RCUNR*N+[True]*N+[False]*((dim-RCUNR-1)*N);
 data=drv.OPCUAReadVariable(varid,var1,mask)
 data=data[0].data
 N3=len(data)//dim
 return data[N3*RCUNR:N3*(RCUNR+1)],var1

#reading the ID also set the switch
data,var1=GetVal(varID);
ID=("%.2x%.2x%.2x%.2x" % (data[0],data[1],data[2],data[3]))

rootLogger = logging.getLogger()
rootLogger = logging.getLogger()
fileHandler = logging.FileHandler("{0}.log".format(logFile))
#fileHandler.setFormatter(logFormatter)
rootLogger.addHandler(fileHandler)

logging.warning("ID=%s" % ID)
if int(ID,16)==0: 
    logging.error("Error: No ID");
    exit()

uCaddr=0x40
HBAaddr=0x41+SINR
i2c=conf.conf['drivers'][1]['obj'] #assume I2C is first device
v=[0]


if True:
#    print("Set Vref=0x0c");
    i2c.i2csetget(uCaddr,[0x0c],reg=13,read=0)
#    print("Set mode=0");
    i2c.i2csetget(uCaddr,[0],reg=2,read=0)
#    print("Set Auto readback=0");
    i2c.i2csetget(uCaddr,[0],reg=5,read=0)
#    print("TX start high=0");
    i2c.i2csetget(uCaddr,[4],reg=14,read=0)
if True:
    i2c.i2csetget(uCaddr,v,reg=2,read=1) #get mode
    print("Current mode=",v[0])

    i2c.i2csetget(uCaddr,v,reg=12,read=1) #get mode
    print("Wait PPS=",v[0])

    i2c.i2csetget(uCaddr,v,reg=13,read=1) #get mode
    print("Vref=",v[0])

    i2c.i2csetget(uCaddr,v,reg=5,read=1) #get mode
    print("Auto readback=",v[0])

    i2c.i2csetget(uCaddr,v,reg=14,read=1) #get mode
    print("TX start high=",v[0])

    time.sleep(0.3)

#exit()
CRCtab=np.load("CRC_HBAT1.npy")
CRCtabl=[d%256 for d in CRCtab]
CRCtabh=[d//256 for d in CRCtab]
def CRCcheck(S1):
    crcl=0;crch=0;
    for b in S1:
        i=crcl ^ b
        crcl=crch ^ CRCtabl[i]
        crch=CRCtabh[i]
#    print(i,CRCtabh[i])
#    crch=crcl ^ CRCtabh[i]
#    crcl= CRCtabl[i]
    return crch*256+crcl


def MakeRequest(serv,data=[],func=5,reg=0):
    data2=[func,reg]+data
    l=len(data2)+1
    data2=[serv,l]+data2
#    CRC=CRCcheck(data2)
#    data2=data2+[CRC%256,CRC//256]
#    assert(CRCcheck(data2)==0)
    return data2

def RequestRegisters(HBANR,reg,length):
    func=length*2+1;
    TX1=MakeRequest(HBANR,[],func,reg);
    i2c.i2csetget(HBAaddr,TX1,read=0)
#    print("Packet to TX",[hex(d) for d in TX1])
#    TX2=hb.ManchesterEncode(TX1)\n",
#    ser.write(bytearray(TX2))\n",
#    return GetPackets();"
def setreg(reg,value):
    TX1=MakeRequest(HBANR,value,func=len(value)*2,reg=reg)
    print(TX1)
    i2c.i2csetget(HBAaddr,TX1,read=0)

def calc_delay(val,term=False):
        TTDs=[False]*5;
        for x in range(5):
            TTDs[x]=val//(2**(x))%2==1
        val2=0;
        previous=term;
#        for x in range(5,-1,-1):
        for x in range(5):
           val2=val2*2+1*(not(TTDs[x]==previous))
           previous=TTDs[x]
        val2=val2*2+1*previous
#        print(val,TTDs,val2,bin(val2))
        return val2
REG_PWR=0x02
REG_X=0x00

setreg(REG_PWR,[0xff])
#exit()
time.sleep(0.5)

dab=False
led=False
term=False
val1=calc_delay(15,term=term)
val2=calc_delay(15,term=term)
print("delay reg=",hex(val2))
setreg(REG_X,[val1+dab*0x80,val2+led*0xc0])
time.sleep(0.5)
#exit()
for x in range(100000):
  print(x)
  time.sleep(0.5)
  if True:
    i2c.i2csetget(uCaddr,[0x0c],reg=13,read=0)
    i2c.i2csetget(uCaddr,[0],reg=2,read=0)
    i2c.i2csetget(uCaddr,[0],reg=5,read=0)
    i2c.i2csetget(uCaddr,[4],reg=14,read=0)
  RequestRegisters(HBANR,0,2)
exit()

HBANRs=range(1,17) if HBANR==0 else [HBANR]
for HBANR in HBANRs:
 RequestRegisters(HBANR,0,1)
 time.sleep(0.2)
 D=[0]*8
 i2c.i2csetget(HBAaddr,D,read=1)
 D=D[2:]
 CRC=CRCcheck(D[:-2])
 CRCerror=((CRC & 0xff)!=D[-2]) or (CRC//256!=D[-1])
 print(HBANR,"RX:",[hex(d) for d in D],',CRC error' if CRCerror else '',',wrong addr' if (HBANR+0x80!=D[0]) else '')
# if CRCerror: print('   CRC=',hex(CRC & 0xff),hex(CRC//256))
# if (HBANR+0x80!=D[0]): print('    wrong address',D[0],HBANR+0x80) 

#data,var1=GetVal(varSet);
#version=bytearray(data).decode("utf-8",errors='ignore')
#logging.warning("Old "+varSet+":"+version)
#if Ver is None: exit()
#print(var1)
#ROMaddr=var1['devreg'][0]['addr']
#ROMreg=var1['devreg'][0]['register_W']
#varlen=var1['width']//8
#print(varlen)
#exit()

#i2c=conf.conf['drivers'][0]['obj'] #assume I2C is first device

#exit()

#R1=0
#ROM=0x50

#Upload version
#Ver2=[(c.encode('utf-8')[0]) for c in Ver]
#Ver2=Ver2[:varlen]
#print(len(Ver),Ver,Ver2)
#V2=[0]
#for i,v in enumerate(Ver2):
# time.sleep(0.1)
# i2c.i2csetget(ROMaddr,[v],reg=ROMreg+i,read=0)
# time.sleep(0.1)
# i2c.i2csetget(ROMaddr,V2,reg=ROMreg+i,read=1)
# print(i,v,V2)
#for i in range(len(Ver2),varlen):
# time.sleep(0.1)
# i2c.i2csetget(ROMaddr,[255],reg=ROMreg+i,read=0)
# time.sleep(0.1)
# i2c.i2csetget(ROMaddr,V2,reg=ROMreg+i,read=1)

#time.sleep(0.1)
#data,var1=GetVal(varSet);
#version=bytearray(data).decode("utf-8",errors='ignore')
#print("Version",version)
#logging.warning("New "+varSet+":"+version)
