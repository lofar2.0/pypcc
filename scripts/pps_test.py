import RPi.GPIO as GPIO
import time
PIN=24
  
GPIO.setmode(GPIO.BCM)
GPIO.setup(PIN,GPIO.IN)

for x in range(3):
  channel=GPIO.wait_for_edge(PIN,GPIO.RISING,timeout=1500)
  print("Timeout!" if channel is None else "PPS")
  channel=GPIO.wait_for_edge(PIN,GPIO.FALLING,timeout=1500)
  print("Timeout!" if channel is None else "PPS")
