from pypcc.i2cdirect import i2cdirect
from time import sleep

import logging
logging.basicConfig(level="ERROR",format='%(asctime)s [%(levelname)-8s,%(filename)-20s:%(lineno)-3d] %(message)s')
#logging.basicConfig(level="DEBUG",format='%(asctime)s [%(levelname)-8s,%(filename)-20s:%(lineno)-3d] %(message)s')

d1=i2cdirect('RECVTR_HB_TEST')



def ADC_config(d1):
 logging.warning("Configure ADCs");
 d1.SetRegister("ADC1.JESD_control1",[0x14])
 d1.SetRegister("ADC1.SYNC_control",[1])
 d1.SetRegister("ADC1.CML_level",[0x7])
 d1.SetRegister("ADC1.dither",[0x0])
 d1.SetRegister("ADC1.Update",[1])
 d1.SetRegister("ADC2.JESD_control1",[0x14])
 d1.SetRegister("ADC2.SYNC_control",[1])
 d1.SetRegister("ADC2.CML_level",[0x7])
 d1.SetRegister("ADC2.dither",[0x0])
 d1.SetRegister("ADC2.Update",[1])
 d1.SetRegister("ADC3.JESD_control1",[0x14])
 d1.SetRegister("ADC3.SYNC_control",[1])
 d1.SetRegister("ADC3.CML_level",[0x7])
 d1.SetRegister("ADC3.dither",[0x0])
 d1.SetRegister("ADC3.Update",[1])

def enable_ant_pwr(d1):
 logging.warning("Switch antenna output power on");
 d1.SetVal("CH1_PWR_ANT_on",[True])
 d1.SetVal("CH2_PWR_ANT_on",[True])
 d1.SetVal("CH3_PWR_ANT_on",[True])

def dither_config(d1): #Switch dither on
 logging.warning("Configure Dither sources");
 d1.SetVal("CH1_DTH_freq",[101000000])
 d1.SetVal("CH1_DTH_PWR",[-10])
 d1.SetRegister("DTH1.CONF",[0,0,0])
 d1.SetRegister("DTH1.Tune",[0,0])
 d1.SetRegister("DTH1.Start",[0,1,0,0,1])
 d1.SetVal("CH2_DTH_freq",[102000000])
 d1.SetVal("CH2_DTH_PWR",[-10])
 d1.SetRegister("DTH2.CONF",[0,0,0])
 d1.SetRegister("DTH2.Tune",[0,0])
 d1.SetRegister("DTH2.Start",[0,1,0,0,1])
 d1.SetVal("CH3_DTH_freq",[103000000])
 d1.SetVal("CH3_DTH_PWR",[-10])
 d1.SetRegister("DTH3.CONF",[0,0,0])
 d1.SetRegister("DTH3.Tune",[0,0])
 d1.SetRegister("DTH3.Start",[0,1,0,0,1])


#RCU_off(d1)
if False:
    d1.runmethod("RCU_off");
    sleep(1.0)
if True:
    d1.runmethod("RCU_on");
    sleep(1.0)
if True:
    d1.runmethod("ADC_on");
enable_ant_pwr(d1)
#dither_config(d1)

if True:
 for varname in ['RCU_PCB_ID','RCU_PCB_version','RCU_PCB_number',
                'RCU_firmware_version',
            'RCU_PWR_good','RCU_PWR_DIGITAL_on','RCU_PWR_ANALOG_on',
            'RCU_IO1_GPIO1','RCU_IO1_GPIO2','RCU_IO2_GPIO1','RCU_IO2_GPIO2',
            "RCU_LED_red_on","RCU_LED_green_on","RCU_DTH_shutdown"]:
    data,var1=d1.GetVal(varname);
    print(varname+"=",data[0])
if True:
    LED=0
    data=d1.SetVal("CH1_HBAT_BF2_byte",[0x80+LED]*32)
#    print(data[0].data)
    data=d1.SetVal("CH2_HBAT_BF2_byte",[0x80+LED]*32)
#    print(data[0].data)
    data=d1.SetVal("CH3_HBAT_BF_byte",[0x80+LED]*32)
#    print(data[0].data)
    data,var1=d1.GetVal("CH1_HBAT_BF2_byte")
    print(data)
    data,var1=d1.GetVal("CH2_HBAT_BF2_byte")
    print(data)
    data,var1=d1.GetVal("CH3_HBAT_BF2_byte")
    print(data)
if True:
 for varname in ['RCU_TEMP','RCU_PWR_3V3','RCU_PWR_2V5','RCU_PWR_1V8']:
    data,var1=d1.GetVal(varname);
    print(varname+"=",data[0])

if True:
 for ch in ['CH1','CH2','CH3']:
  print("Channel ",ch)
  for varname in ['band_select','attenuator_dB','ADC_shutdown',
     'PWR_ANT_on','PWR_ANT_VOUT','PWR_ANT_VIN','PWR_ANT_IOUT',
     'ADC_locked','ADC_sync','ADC_JESD','ADC_CML_level',
     'DTH_freq','DTH_PWR','DTH_on',
     ]:
    data,var1=d1.GetVal(ch+'_'+varname);
    print("  ",varname+"=",data[0])
