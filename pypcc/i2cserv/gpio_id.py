#import numpy as np
from .hwdev import hwdev;
import logging
import RPi.GPIO as GPIO
from pypcc.queuetypes import *


class gpio_id(hwdev):
  def __init__(self,config):
    hwdev.__init__(self,config);
    self.pins=config['parameters']
    self.Npins=len(self.pins);
    GPIO.setmode(GPIO.BCM)
    for i,pin in enumerate(self.pins):
        GPIO.setup(pin,GPIO.IN)
    logging.info(str((config['name']," gpio, value=",self.get_value())));

  def get_value(self):
    value=0;
    for pin in self.pins:
      value=2*value+1 if GPIO.input(pin) else 2*value
    return value

  def OPCUASetVariable(self,varid,var1,data,mask):
      return []

  def OPCUAReadVariable(self,varid,var1,mask):
      value=self.get_value()
      logging.info(" gpio, value=%i"%value);
      Data=OPCUAset(varid,InstType.varSet,[value],mask.copy())
      return [Data]
 


