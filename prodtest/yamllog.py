import yaml

class yamllog():
  def __init__(self,yamlfile):
    self.conf=yaml.load(open(yamlfile+'.yaml'))
  def getdata(self,table,field,value):
    for D in self.conf[table]:
       if D[field]==value: return D
    return None;
  def insert(self,table,D):
    self.conf[table].append(D);
  def replace(self,table,field,value,Dnew):
    for i,D in enumerate(self.conf[table]):
       if D[field]==value: 
           self.conf[table][i]=Dnew;
           return;
    self.insert(table,Dnew);
  def save(self,yamlfile):
    with open(yamlfile+".yaml","w") as file:
      yaml.dump(self.conf,file)

if False:
 from datetime import datetime;
 testtime=datetime.now().strftime("%y-%m-%d %H:%M")
 T1=yamllog("RCU2HQM")
 D=T1.getdata("Power_supply","ID","I2C1")
 D['Ioff_6V']=50;
 D['time']=testtime;
 T1.replace("Power_supply","ID","I2C1",D)
 print(D)
 T1.save("RCU2HQM2")
