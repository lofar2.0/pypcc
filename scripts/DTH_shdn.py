from test_common import *
connect()

name="RCU_DTH_shutdown"
RCU=[0,1,2,3];
Att=[False,False,False]
#Att=[True,True,True]
#RCU=[1,2,3];
#Att=[0,0,0]


setAntmask(RCU)

att=get_debug_value(name+"_R")
print("Att old:",att[:12])
for r in RCU:
  att[3*r:3*r+3]=Att
print("Att set:",att[:12])
set_debug_value(name+"_RW",att)

time.sleep(1.0)
att=get_debug_value(name+"_R")
print("Att new:",att[:12])

disconnect()