#3 switches of UNB2
import logging
from .i2c_smbus import i2c_smbus as i2c

class i2c_switch2(i2c):
    def __init__(self,config):
        i2c.__init__(self,config)
        self.SWaddr1=config['devreg'][0]['addr']
        self.SWaddr2=config['devreg'][1]['addr']
        self.SWaddr3=config['devreg'][2]['addr']
        self.channel1=-1
        self.channel2=-1
        self.channel3=-1
        logging.info("i2c switch2 at address %i,%i,%i" % (self.SWaddr1,self.SWaddr2,self.SWaddr3))
#        logging.warn("APSCT switch disabled for testing")
    def setI2Cswitch(self,addr,state):
        res=self.i2csetget(addr,[state]);
        if not(res):
           logging.warning("Error setting switch, switch reset not implemented!");
        return res;

    def SetSW1(self,channelbit):
        channel=1<<(channelbit)
        if (channel)==self.channel1: return True;
        logging.debug("SetChannel1=%i" % channelbit)
        self.channel1=channel
        self.channel2=-1
        self.channel3=-1
#        return True; #testing without APSCT switch
        return self.setI2Cswitch(self.SWaddr1,channel)

    def SetSW2(self,channelbit):
        channel=1<<(channelbit)
        if (channel)==self.channel2: return True;
        logging.debug("SetChannel2=%i" % channelbit)
        self.channel2=channel
        self.channel3=-1
        return self.setI2Cswitch(self.SWaddr2,channel)

    def SetSW3(self,channelbit):
        channel=1<<(channelbit)
        if (channel)==self.channel3: return True;
        logging.debug("SetChannel3=%i" % channelbit)
        self.channel3=channel
        return self.setI2Cswitch(self.SWaddr3,channel)

    def SetSWx(self,sw1,sw2,sw3):
        channel=0;
        for x in sw1: channel+=(1<<x);
        self.channel1=channel;
        self.setI2Cswitch(self.SWaddr1,channel)
        if channel==0: return;
        channel=0;
        for x in sw2: channel+=(1<<x);
        self.channel2=channel;
#        self.i2csetget(self.SWaddr2,[channel])
        self.setI2Cswitch(self.SWaddr2,channel)
        if channel==0: return;
        channel=0;
        for x in sw3: channel+=(1<<x);
        if channel==0: return;
        self.channel3=channel;
#        self.i2csetget(self.SWaddr3,[channel])
        self.setI2Cswitch(self.SWaddr3,channel)
      
    def clearSwitch(self):
        return

