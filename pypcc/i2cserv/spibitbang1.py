from enum import Enum
import logging
import numpy as np
from .hwdev import hwdev

class SPIBB_pins(Enum):
    CLK = 0  
    SDIO = 1  
    SDIOdir = 2 
    CS = 3  

class spibitbang1(hwdev):
  def __init__(self,config):
    hwdev.__init__(self,config)
#    logging.info("spibitbang todo")

  def i2csetget(self,addr,data,reg=None,read=0):
    logging.debug(str(("spibitbang:",addr,data,reg,read)))
    if read==0: return self.SetSPIbb(reg,data)
    elif read==1: return self.GetSPIbb(reg,data)
    elif read==2: return self.GetSPIbb(data[0],None) #Set register in parallel
    else: logging.warn("Not implemented!")
    return False;

  def SetSPIbb(self,ADC_address,value):
         
        CSdev=self.conf['devreg'][SPIBB_pins.CS.value]
        CSpin=self.conf['parameters'][SPIBB_pins.CS.value]
        SDOdev=self.conf['devreg'][SPIBB_pins.SDIO.value]
        SDOpin=self.conf['parameters'][SPIBB_pins.SDIO.value]
        CLKdev=self.conf['devreg'][SPIBB_pins.CLK.value]
        CLKpin=self.conf['parameters'][SPIBB_pins.CLK.value]
#        SDIOdirdev=self.conf['devreg'][SPIBB_pins.SDIOdir.value]
#        SDIOdirpin=self.conf['parameters'][SPIBB_pins.SDIOdir.value]

        logging.debug(str(("SPIbb set",ADC_address,value)))
        SetI2C=self.conf['parentcls'].SetVarValue
        ADC_bytes = 0x00
        ADC_rw    = 0x00 # 0 for write, 1 for read
        data2 = ( ADC_rw << 23 ) + ( ADC_bytes << 21 ) + ( ADC_address << 8 ) + value[0]
          
        bit_array = "{0:{fill}24b}".format(data2, fill='0')
      #    print(bit_array)
        if not(SetI2C(CSdev,1,CSpin,[0])): return False #enable
        for bit in bit_array:
              SetI2C(SDOdev,1,SDOpin,[int(bit)]) 
              SetI2C(CLKdev,1,CLKpin,[1]) 
              SetI2C(CLKdev,1,CLKpin,[0]) 
        SetI2C(CSdev,1,CSpin,[1]) #disable
        SetI2C(SDOdev,1,SDOpin,[1]) #high when finished
        return True;

  def GetSPIbb(self,ADC_reg_address,value):
        CSdev=self.conf['devreg'][SPIBB_pins.CS.value]
        CSpin=self.conf['parameters'][SPIBB_pins.CS.value]
        SDOdev=self.conf['devreg'][SPIBB_pins.SDIO.value]
        SDOpin=self.conf['parameters'][SPIBB_pins.SDIO.value]
        CLKdev=self.conf['devreg'][SPIBB_pins.CLK.value]
        CLKpin=self.conf['parameters'][SPIBB_pins.CLK.value]
        SDIOdirdev=self.conf['devreg'][SPIBB_pins.SDIOdir.value]
        SDIOdirpin=self.conf['parameters'][SPIBB_pins.SDIOdir.value]
#        print("SPI registers")
#        print("CS",CSdev,CSpin)
#        print("SDO",SDOdev,SDOpin)
#        print("CLK",CLKdev,CLKpin)
#        print("DIR",SDIOdirdev,SDIOdirpin)
        logging.debug(str(("SPIbb get",ADC_reg_address,value)))
        SetI2C=self.conf['parentcls'].SetVarValue
        GetI2C=self.conf['parentcls'].GetVarValue
        ADC_bytes = 0x00
        if not(ADC_reg_address is None):  
          ADC_rw    = 0x01 # 0 for write, 1 for read
        
          data = ( ADC_rw << 15) + ( ADC_bytes << 13 ) + ADC_reg_address

          if not(SetI2C(CSdev,1,CSpin,[0])): return False #enable


          bit_array = "{0:{fill}16b}".format(data, fill='0')
        #print("SPI TX bits",bit_array)
          for bit in bit_array:
              SetI2C(SDOdev,1,SDOpin,[int(bit)]) 
              SetI2C(CLKdev,1,CLKpin,[1]) 
              SetI2C(CLKdev,1,CLKpin,[0]) 

          SetI2C(CSdev,1,CSpin,[1]) #disable

          #    print("read byte")
        if not(value is None):  
           SetI2C(SDIOdirdev,1,SDIOdirpin,[1]) #input
           SetI2C(CSdev,1,CSpin,[0]) #enable
           a=[0]
           N=1;#len(value)
           ret_value=[0];
           for i in range(N): value[i]=0
           for cnt in range(8*(ADC_bytes+1)):
              GetI2C(SDOdev,1,SDOpin,ret_value) #enable
#              logging.debug("Got bit"+str((ret_value)))
              for i in range(N): 
                   if ret_value is None: value[i]=None 
                   elif not value[i] is None: value[i]=(value[i]<<1)+ ret_value[i]
              SetI2C(CLKdev,1,CLKpin,[1]) 
              SetI2C(CLKdev,1,CLKpin,[0])  #read after falling edge
           SetI2C(CSdev,1,CSpin,[1]) #disable
           SetI2C(SDIOdirdev,1,SDIOdirpin,[0]) #output
           logging.debug(str(("SPIbb got",value)))
        return True;

def GetSPIbb2(SetI2C,GetI2C,RCUi,SPIdev,I2Cdev,I2Cpins,value):
#Read 3 SPI devices in parallel from same IOexpander
        ADC_reg_address=SPIdev[0].Register_R
        Nv=len(SPIdev)
        def Setbit(pintype,value):
           for i in range(1,Nv):
             SetI2C(RCUi,I2Cdev[i][pintype],1,I2Cpins[i][pintype],[value],buffer=True) 
           SetI2C(RCUi,I2Cdev[0][pintype],1,I2Cpins[0][pintype],[value]) 
        def Getbit(pintype):
           print("N=",Nv,len(value))
           retvalue=np.zeros_like(value)
           retvalue[0::Nv]=GetI2C(RCUi,I2Cdev[0][pintype],1,I2Cpins[0][pintype])
           for i in range(1,Nv):
             retvalue[i::Nv]=GetI2C(RCUi,I2Cdev[i][pintype],1,I2Cpins[i][pintype],buffer=True)
           return retvalue
           
        CLK=0
        SDIO=1
        SDIOdir=2
        CS=3

        logging.debug(str(("SPIbb get",ADC_reg_address)))
          
        ADC_bytes = 0x00
        ADC_rw    = 0x01 # 0 for write, 1 for read
        
        data = ( ADC_rw << 15) + ( ADC_bytes << 13 ) + ADC_reg_address
        Setbit(CLK,0) 
          
        Setbit(CS,0) #enable

        bit_array = "{0:{fill}16b}".format(data, fill='0')
        logging.debug(str(("SPI TX",bit_array)))
        for bit in bit_array:
              Setbit(CLK,0) 
              Setbit(SDIO,int(bit)) 
              Setbit(CLK,1) 

#        Setbit(CS,1) #disable
        Setbit(SDIOdir,1) #input
        Setbit(CLK,0) 

          #    print("read byte")
#        Setbit(CS,0) #enable
        a=[0]
        N=len(value)
        for i in range(N): value[i]=0
        for cnt in range(8*(ADC_bytes+1)):
              ret_value=Getbit(SDIO)
              for i in range(N): value[i]=(value[i]<<1)+ ret_value[i]
              Setbit(CLK,1) 
              Setbit(CLK,0)  #read after falling edge
        Setbit(CS,1) #disable
        Setbit(SDIO,1)#High when finished 
        Setbit(SDIOdir,0) #output
        return True;