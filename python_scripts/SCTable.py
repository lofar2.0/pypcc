import pypcc.yamlconfig as yc
import sys
if len(sys.argv)<2: 
  print("Usage: SCTable yamlfile")
  exit()
RW={'ro':'_R','rw':'_R/_RW','variable':'_RW'}
DT={'uint8':'numpy.int64  ',
   'uint32':'numpy.int64  ',
  'boolean':'numpy.bool_  ',
   'double':'numpy.float64',
   'string':'numpy.str    '}
Y=yc.yamlconfig(sys.argv[1])
sep='\t'
#Sort alphabetically
names=[(v['name']).upper() for v in Y.getvars()]
SI=sorted(range(len(names)), key=lambda k: names[k])
for i0 in SI:
    v=Y.getvar1(i0)
#for v in Y.getvars():
    if v.get("debug",False): continue;
    rw=v.get("rw",'ro')
#    print(rw)
    if rw=='hidden': continue;

    dim=v.get('dim',1)
    if dim>1:
       dim2=v.get('dim2',None)
       if not(dim2 is None):
         dimS=', dims=(%i,%i)' % (dim2[0],dim2[1])
       else: 
         dimS=', dims=(%i,)' % dim
    else: dimS=''

    if rw in ['rw','ro']:
     name=v['name']+'_R'
     S='%-20s' % name
     name='"2:'+v['name']+'_R"'
     S+=' = attribute_wrapper(comms_annotation=[%-25s],datatype=' % name
     dt=str(v['dtype'])
     S+=DT[dt];
     S+=dimS+")"
     print(S)

    if rw in ['rw','variable']:
     name=v['name']+'_RW'
     S='%-20s' % name
     name='"2:'+v['name']+'_RW"'
     S+=' = attribute_wrapper(comms_annotation=[%-25s],datatype=' % name
     dt=str(v['dtype'])
     S+=DT[dt];
     S+=dimS+", access=AttrWriteType.READ_WRITE)"
#     print(dt)
#     S+=str(v['dtype'])+sep
#     S+=RW[v['rw']]+sep
#     S+=v.get('mask','')+sep
#     S+=str(v.get('monitor',''))+sep
#     S+=str(v.get('description',''))+sep
     print(S)
print()
#print("Name"+sep+"Mask"+sep+"Description")
#for v in Y.conf['methods']:#
#    if v.get("debug",False): continue;
#    S=v['name']+sep
#    S+=str(v['dim'])+sep
#    S+=str(v['dtype'])+sep
#    S+=RW[v['rw']]+sep
#    S+=v.get('mask','')+sep
#    S+=str(v.get('monitor',''))+sep
#    S+=str(v.get('description',''))+sep
#    print(S)
