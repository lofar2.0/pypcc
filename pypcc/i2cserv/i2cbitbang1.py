from enum import Enum
import logging
import numpy as np
from .hwdev import hwdev
import time
#This is copy of spibitbang. Need to be updated!!
class SPIBB_pins(Enum):
    CLK = 0  
    SDA = 1  
    SDAdir = 2 

class i2cbitbang1(hwdev):
  def __init__(self,config):
    hwdev.__init__(self,config)

  def i2csetget(self,addr,data,reg=None,read=0):
#    print("I2Cbbset",addr,data,reg,read);
    if read==0: 
       if not(reg is None): 
            if reg>255: data=[reg//256,reg%256]+data;
            else:       data=[reg]+data;
       if not(self.SetI2Cbb(addr,data)): return False;
       return self.GetI2Cbb(addr,[]);
    if read==2: return self.SetI2Cbb(addr,data)
    elif read==1: 
       if not(reg is None): 
            if reg>255: reg=[reg//256,reg%256];
            else:       reg=[reg];
            self.SetI2Cbb(addr,reg)
       return self.GetI2Cbb(addr,data)
    elif read==3: 
              time.sleep(data[0]/1000);
              return True
    else: logging.warn("Not implemented!")
    return False;


  def SetI2Cbb(self,address,value):
        SDAdev=self.conf['devreg'][SPIBB_pins.SDA.value]
        SDApin=self.conf['parameters'][SPIBB_pins.SDA.value]
        CLKdev=self.conf['devreg'][SPIBB_pins.CLK.value]
        CLKpin=self.conf['parameters'][SPIBB_pins.CLK.value]
        DIRdev=self.conf['devreg'][SPIBB_pins.SDAdir.value]
        DIRpin=self.conf['parameters'][SPIBB_pins.SDAdir.value]
        SetI2C=self.conf['parentcls'].SetVarValue
        GetI2C=self.conf['parentcls'].GetVarValue


#        ADC_address=dev.Register_W<<1; #Write
        ADC_address=address<<1;#dev.Register_W<<1; #Write
        logging.debug(str(("I2Cbb set",hex(ADC_address),value)))
        
        SetI2C(DIRdev,1,DIRpin,[1]) #Input = high
        SetI2C(CLKdev,1,CLKpin,[1]) #Should be high for start
	#start
        SetI2C(SDAdev,1,SDApin,[0]) #Output = low
        SetI2C(DIRdev,1,DIRpin,[0]) #Output = low
        SetI2C(CLKdev,1,CLKpin,[0]) 
        ack=[0];
        def TXbyte(b):
          for bit in "{0:{fill}8b}".format(b, fill='0'):
              SetI2C(DIRdev,1,DIRpin,[int(bit)]) 
              SetI2C(CLKdev,1,CLKpin,[1]) 
              SetI2C(CLKdev,1,CLKpin,[0]) 
          SetI2C(DIRdev,1,DIRpin,[1]) #input 
#          GetI2C(SDAdev,1,SDApin,ack)
#          print("Ack=",ack[0]);
          SetI2C(CLKdev,1,CLKpin,[1]) 
          GetI2C(SDAdev,1,SDApin,ack)
#          print("Ack=",ack[0]);
          SetI2C(CLKdev,1,CLKpin,[0]) 
          return ack[0];
 
        TXbyte(ADC_address);
        for v in value:
           TXbyte(v);
        #stop
        SetI2C(DIRdev,1,DIRpin,[0]) #low
        SetI2C(CLKdev,1,CLKpin,[1]) #Should be high
        SetI2C(DIRdev,1,DIRpin,[1]) #Input = high
        SetI2C(CLKdev,1,CLKpin,[0]) #keep low between transmission to not generate stop/start when other i2c actice
        
        return True;

  def GetI2Cbb(self,reg_address,value):
        SDAdev=self.conf['devreg'][SPIBB_pins.SDA.value]
        SDApin=self.conf['parameters'][SPIBB_pins.SDA.value]
        CLKdev=self.conf['devreg'][SPIBB_pins.CLK.value]
        CLKpin=self.conf['parameters'][SPIBB_pins.CLK.value]
        DIRdev=self.conf['devreg'][SPIBB_pins.SDAdir.value]
        DIRpin=self.conf['parameters'][SPIBB_pins.SDAdir.value]
        SetI2C=self.conf['parentcls'].SetVarValue
        GetI2C=self.conf['parentcls'].GetVarValue


        ADC_address=1+(reg_address<<1); #Read
        logging.debug(str(("I2Cbb get",hex(ADC_address),value)))

        SetI2C(DIRdev,1,DIRpin,[1]) #Input = high
        SetI2C(CLKdev,1,CLKpin,[1]) #Should be high for start bit
	#start
        SetI2C(SDAdev,1,SDApin,[0]) #Output = low
        SetI2C(DIRdev,1,DIRpin,[0]) #Output = low
        SetI2C(CLKdev,1,CLKpin,[0]) 
        ack=[0];
        def TXbyte(b):
          for bit in "{0:{fill}8b}".format(b, fill='0'):
              SetI2C(DIRdev,1,DIRpin,[int(bit)]) 
              SetI2C(CLKdev,1,CLKpin,[1]) 
              SetI2C(CLKdev,1,CLKpin,[0]) 
          SetI2C(DIRdev,1,DIRpin,[1]) #input 
#          GetI2C(SDAdev,1,SDApin,ack)
#          print("Ack=",ack[0]);
          SetI2C(CLKdev,1,CLKpin,[1]) 
          GetI2C(SDAdev,1,SDApin,ack)
#          print("Ack=",ack[0]);
          SetI2C(SDAdev,1,SDApin,[0]) 
          SetI2C(CLKdev,1,CLKpin,[0]) 
          return ack[0];

        def RXbyte(last=False):
          b=0;
          for i in range(8):
              SetI2C(CLKdev,1,CLKpin,[1]) 
              GetI2C(SDAdev,1,SDApin,ack)
#              print(ack[0])
              if ack[0] is None: b=None
              elif not b is None: b=(b<<1)+ack[0];
              SetI2C(CLKdev,1,CLKpin,[0]) 
#          print("RXbyte",hex(b));
          SetI2C(SDAdev,1,SDApin,[0]) 
          SetI2C(DIRdev,1,DIRpin,[1 if last else 0]) #outout = low = ack 
          SetI2C(CLKdev,1,CLKpin,[1]) 
          SetI2C(CLKdev,1,CLKpin,[0]) 
          SetI2C(DIRdev,1,DIRpin,[1])
          return b;
 
        TXbyte(ADC_address);
        status=RXbyte(last=(len(value)==0))
#        if not(RXbyte()==0x80): return False;
        for i in range(len(value)):
          value[i]=RXbyte(last=(i==len(value)-1))
        logging.debug(str(("si status:",hex(status),[hex(v) for v in value])))#should be 0x80 - will fail for None!!
        #stop
        #SetI2C(DIRdev,1,DIRpin,[0]) #low
        SetI2C(CLKdev,1,CLKpin,[1]) 
        SetI2C(DIRdev,1,DIRpin,[1]) #Input = high
        SetI2C(CLKdev,1,CLKpin,[0]) #keep low between transmission to not generate stop/start when other i2c actice

        return True;