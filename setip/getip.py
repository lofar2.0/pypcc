#!/usr/local/bin/python3.11
import RPi.GPIO as gpio

ID_PINS = [7, 8, 16, 12, 21] # Reverse ID bits

gpio.setmode(gpio.BCM)  # Set IO pins used for the APSCT-ID
for pin in ID_PINS:
    gpio.setup(pin, gpio.IN)

bits=[gpio.input(pin) for pin in ID_PINS]
#print(bits)

loc_id=0
for bit in bits:
    loc_id = 2*loc_id + bit
ip='10.99.0.%i'%(100+loc_id)
#ip='10.87.6.213'
print(ip)
