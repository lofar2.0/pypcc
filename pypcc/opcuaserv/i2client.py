#This is the OPC-UA side of the pipes to I2Cserver

from multiprocessing import Queue
from pypcc.queuetypes import *


class i2client():
    def __init__(self,name='RCU'):
        self.Qin=Queue() 
        self.Qout=Queue()
        self.name=name;

    def GetInfo(self):
        return self.Qin,self.Qout,self.name

    def stop(self):
        self.Qout.put(None);
        self.Qin.put(None);
#        self.thread1.join();
#        os.close(self.Output)
#        self.Input.close()
    def restart(self):
        self.stop()
        self.Qin=Queue()
        self.Qout=Queue()

    def readvar(self,id1,mask=[]):
        Data=OPCUAset(id1,InstType.varRead,[],mask.copy())
        self.Qout.put(Data);

    def setvar(self,id1,data=[],mask=[]):
        Data=OPCUAset(id1,InstType.varSet,data.copy(),mask.copy())
        self.Qout.put(Data);

    def QoutLength(self):
        return self.Qout.qsize()

    def callmethod(self,id1,mask=[]):
        Data=OPCUAset(id1,InstType.method,[],mask.copy())
        self.Qout.put(Data);

    def data_waiting(self):
        return (self.Qin.qsize()>0);

    def readdata(self):
        message=self.Qin.get()
        if message is None: return None
        return message.id,message.data,message.mask


