from .hwdev import hwdev;
import logging
import threading
from pypcc.queuetypes import *
import time;
import struct

#import signal
try:
  import RPi.GPIO as GPIO
except:
  GPIO=False;
  
class gpio_pps(hwdev):
  def __init__(self,config):
    hwdev.__init__(self,config);
    self.pin=config['parameters'][0];
    self.cnt=0;
    self.timesec=None;
    self.timeid=None;
    logging.info("PPS on pin %i",self.pin)
    if GPIO:
      GPIO.setmode(GPIO.BCM)
      GPIO.setup(self.pin,GPIO.IN)
    else:
      logging.warn("RPi GPIO module not found, PPS input disable!")
      return
    self.PPSid=config.get('maskid',None)
    if self.PPSid is None:
      logging.warn("PPS cnt variable not linked to driver!")
      return

    self.thread=threading.Thread(target=self.PPS_wait_loop); 
    self.thread.start();

  def ppsdata(self):
    data=struct.pack('>Q',self.cnt)
    return [d for d in data];

  def incPPS(self):
      self.cnt+=1;
      if not(self.timesec is None):
          self.Qout.put(OPCUAset(self.timeid,InstType.varSet,self.timesec,[]))
          self.cnt=0;
          self.timesec=None;
      logging.debug("PPS cnt=%i" % self.cnt) 
      self.Qout.put(OPCUAset(self.PPSid,InstType.varSet,self.ppsdata(),[]))

  def PPS_wait_loop(self):
     time.sleep(3);
     while True:
         self.Qout=self.conf.get('Qout',None)
         if self.Qout is None:
            logging.warn("PPS thread end (No Qout)")
            return
         channel=GPIO.wait_for_edge(self.pin,GPIO.RISING,timeout=2500)
         if not(channel is None): 
          self.incPPS();
         else:
            logging.debug("PPS not received!") 
         channel=GPIO.wait_for_edge(self.pin,GPIO.FALLING,timeout=1500)
         if not(channel is None): 
          self.incPPS();
         else:
            logging.debug("PPS not received!") 

  def OPCUAReadVariable(self,varid,var1,mask):
      if varid==self.PPSid:
        return [OPCUAset(self.PPSid,InstType.varSet,self.ppsdata(),[])]
      else:
         self.timeid=varid;
         print("timeid=",varid);
         return [OPCUAset(self.PPSid,InstType.varSet,[0,0,0,0,0,0,0,0],[])]

  def OPCUASetVariable(self,varid,var1,data,mask):
      if varid==self.PPSid:
          logging.warn("Can not set PPS count");
          return []
      if self.timeid is None: self.timeid=varid;
      if not(varid==self.timeid): 
          logging.warn("Unknown variable");
          return []
      logging.info("Set PPS time");
      self.timesec=data.copy();
      return []
