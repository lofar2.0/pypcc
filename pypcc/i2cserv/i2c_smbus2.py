import os
from smbus2 import SMBus,i2c_msg
import time
import logging
#read=0: write to register
#read=1: read from register
#read=2: write to register (common in group)
#read=3: wait ms second
from .hwdev import hwdev;

class i2c_smbus2(hwdev):
    def __init__(self,config):
       hwdev.__init__(self,config);
       self.bus_nr=config['parameters'][0]
       logging.info("smbus2 driver on bus "+str(self.bus_nr))
       self.bus = SMBus(self.bus_nr)
       self.I2Ccounter=0


    def i2csetget(self,addr,data,reg=None,read=0):
#       logging.debug(str(("I2C",addr,reg,data,read)))
       try:
#       if True:
              if read==3:
                     time.sleep(data[0]/1000.)
                     return True

              if read==1:
                     if not(reg is None):
                          data[:]=self.bus.read_i2c_block_data(addr, reg, len(data))
                     else:
                          msg=i2c_msg.read(addr,len(data))
                          self.bus.i2c_rdwr(msg)
                          if len(msg)!=len(data):
                              logging.debug(str(("I2C get failed",addr,reg,data,read)))
                              data[0]=0;
                              return False

                          data[:]=list(msg)
                     logging.debug(str(("I2C get",addr,reg,data,read)))
              else:
                     if not(reg is None): 
                            self.bus.write_i2c_block_data(addr, reg, data)
                     else:
                          msg=i2c_msg.write(addr,data)
                          self.bus.i2c_rdwr(msg)
              return True;
       except:
#       else:
              logging.debug("I2C failed!")
              if len(data)>0: data[0]=0
              return False;

