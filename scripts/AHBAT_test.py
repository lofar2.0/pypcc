from test_common import *
busy="RECVTR_translator_busy_R"
RCU=0
AntMask=[False,True,False]

connect()
set_value("RECVTR_monitor_rate_RW",0)

if False:
  setRCUmask([RCU])
  callmethod("RCU_on")
  wait_not_busy(busy,timeout_sec=4)


setAntmask([RCU],AntMask)
wait_not_busy(busy,timeout_sec=4)

#val,tp=get_value_type(name+"_R")
if False:
    print("LEDs & PWR on")
    set_value("HBAT_PWR_on_RW",[True]*(32*96))
    set_value("HBAT_PWR_LNA_on_RW",[True]*(32*96))
    set_value("HBAT_LED_on_RW",[True]*(32*96))
    val,tp=get_value_type("HBAT_BF_delay_steps_R")
    set_value("HBAT_BF_delay_steps_RW",val,tp)
    wait_not_busy(busy,timeout_sec=4)

setAntmask([RCU],AntMask,name="AHBAT_mask_RW")
wait_not_busy(busy,timeout_sec=4)

sinr=1

if False:
    set_value("AHBAT_Vref_RW",[0x04]*96)
    wait_not_busy(busy,timeout_sec=4)


if False:
  print("Configuration:")
  callmethod("AHBAT_update")
  wait_not_busy(busy,timeout_sec=8)

#print("UID",get_value("AHBAT_UID_R")[16*sinr:16*(sinr+1)])
  uid=get_value("AHBAT_UID_R")[16*sinr:16*(sinr+1)]
#print("UID",[hex(x) for x in uid])

  swver=get_value("AHBAT_software_version_R")[16*sinr:16*(sinr+1)]
#print("SW ver",[('%08x'%x)[::-1] for x in swver])

  PCBver=get_value("AHBAT_PCB_version_R")[16*sinr:16*(sinr+1)]
  PCBnr=get_value("AHBAT_PCB_number_R")[16*sinr:16*(sinr+1)]
#print("PCB ver",PCBver)
  config=get_value("AHBAT_config1_R")[16*sinr:16*(sinr+1)]
  config2=get_value("AHBAT_config2_R")[16*sinr:16*(sinr+1)]

  for cnt,u1,s1,p1,c1,c2,n1 in zip(range(16),uid,swver,PCBver,config,config2,PCBnr):
    print('%2i %016x %08x %016x%016x %s %s'%(cnt+1,u1,s1,c1,c2,p1,n1))

if True:
  print("ADC values:")
  callmethod("AHBAT_read_ADC")
  wait_not_busy(busy,timeout_sec=8)

  uid=get_value("AHBAT_ADC_R")[16*sinr:16*(sinr+1)]
  for cnt,val in enumerate(uid):
    print('%2i %6i %6i %6i %6i %6i'%(cnt+1,val&0xffff,(val>>16)&0xffff,(val>>32)&0xffff,(val>>48)&0xffff,((val>>16)&0xffff)-((val)&0xffff)))
#  print("ADC",['%016x'%x for x in uid])

disconnect()
exit()



name="HBAT_BF_delay_steps"
NewVal1=range(32)
NewVal2=[1]*32
#NewValues=[NewVal1,NewVal2,NewVal1,NewVal2,NewVal1,NewVal2,NewVal1,NewVal2,NewVal1,NewVal2]
NewValues=[NewVal1,NewVal2]
DEBUG=True

from test_common import *
import numpy as np
connect()

i=(RCU*3)*32

val,tp=get_value_type(name+"_R")
if DEBUG:
  print("Current values:");
  for x in range(3): print(val[i+x*32:i+(x+1)*32])

for cnt,NewVal in enumerate(NewValues):

 for x in range(3): val[i+x*32:i+(x+1)*32]=NewVal

 set_value(name+"_RW",val,tp)
 if DEBUG:
   print("set:")
   for x in range(3): print(val[i+x*32:i+(x+1)*32])
 time.sleep(1)
 wait_not_busy(busy,timeout_sec=4)
# time.sleep(0.1)
 val=get_value(name+"_R")
 if DEBUG:
   print("readback:")
   for x in range(3): print(val[i+x*32:i+(x+1)*32])

 same=True;
 for x in range(3): 
   for y in range(32): 
     if not(val[i+x*32+y]==NewVal[y]): same=False;
 print(cnt+1,"Success" if same else "FAILED!")
#time.sleep(5)
#val[i:i+64]=[0]*64

#set_value(name+"_RW",val)
#print("set:",val[i:i+64])
#time.sleep(1)
#val=get_value(name+"_R")
#print("new:",val[i:i+64])

disconnect()
