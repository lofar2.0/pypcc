#import numpy as np
from .hwdev import hwdev;
import logging
import RPi.GPIO as GPIO
from pypcc.queuetypes import *


class gpio(hwdev):
  def __init__(self,config):
    hwdev.__init__(self,config);
    self.pins=config['parameters']
    self.Npins=len(self.pins);
    GPIO.setmode(GPIO.BCM)
#    self.state=[0 for pin in self.pins]
#    logging.info(str(("gpio, Pins=",self.state)));
    for i,pin in enumerate(self.pins):
        GPIO.setup(pin,GPIO.OUT)
#    for i,pin in enumerate(self.pins):
#        GPIO.setup(pin,GPIO.IN)
    self.state=[GPIO.input(pin) for pin in self.pins]
    logging.info(str((config['name']," gpio, Pins=",self.state)));

  def OPCUASetVariable(self,varid,var1,data,mask):
      logging.info("Set gpio");
      if len(mask)!=self.Npins: 
          if len(mask)>0: logging.warning("GPIO Wrong mask length %i,%i"%(len(mask),self.Npins));
          mask=[True]*self.Npins;
      if len(data)!=self.Npins: 
          if len(data)==1: data=[data[0]]*self.Npins;
          else:
            logging.warning("GPIO Wrong data length");
            return []
      for i,pin in enumerate(self.pins):
        if mask[i]:
          GPIO.output(pin,data[i]%2);
          self.state[i]=data[i]%2
      Data=OPCUAset(varid,InstType.varSet,self.state.copy(),mask.copy())
      return [Data]


  def OPCUAReadVariable(self,varid,var1,mask):
      logging.info("Read gpio");
      Data=OPCUAset(varid,InstType.varSet,self.state.copy(),mask.copy())
      return [Data]
 


