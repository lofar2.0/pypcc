from test_common import *

#name="RCU_band_select"
name="RCU_hband_select"
#RCU=[0,1,2,3,4,5,8,12];
#RCU=[0,1,2,3,4];
RCU=[0,1,2,3];
Att=[2,2,2] #30MHz
#Att=[1,1,1] #10
#Att=[4,4,4]
#Att=[1+4,2,1]
#Att=[2,1+4,1]
#HB 2=110-180
#   1=170-230
#   4=210-270
connect()
setAntmask(RCU)

att=get_value(name+"_R")
print("Att old:",att[3*RCU[0]:3*RCU[-1]+3])
for r in RCU:
  att[3*r:3*r+3]=Att
set_value(name+"_RW",att)

time.sleep(0.5)
att=get_value(name+"_R")
print("Att new:",att[3*RCU[0]:3*RCU[-1]+3])

disconnect()