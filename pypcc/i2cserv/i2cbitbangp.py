from enum import Enum
import logging
import numpy as np
from .hwdev import hwdev
import time
#This is copy of spibitbang. Need to be updated!!
class SPIBB_pins(Enum):
    CLK = 0  
    SDA = 1  
    SDAdir = 2 

class i2cbitbangp(hwdev):
  def __init__(self,config):
    hwdev.__init__(self,config)
    self.N=len(self.conf['parameters'])//3;

  def i2csetget(self,addr,data,reg=None,read=0):
    logging.debug(str(("I2Cbbpset",addr,data,reg,read)));
    if read==0: 
       if not(reg is None): 
            if reg>255: data=[reg//256,reg%256]+data;
            else:       data=[reg]+data;
       if not(self.SetI2Cbb(addr,data)): return False;
       return self.GetI2Cbb(addr,[]);
    if read==2: return self.SetI2Cbb(addr,data)
    elif read==1: 
       if not(reg is None): 
            if reg>255: reg=[reg//256,reg%256];
            else:       reg=[reg];
            self.SetI2Cbb(addr,reg)
       return self.GetI2Cbb(addr,data)
    elif read==3: 
              time.sleep(data[0]/1000);
              return True
    else: logging.warn("Not implemented!")
    return False;


  def SetI2Cbb(self,address,value):
        SDAdev=self.conf['devreg'][SPIBB_pins.SDA.value]
        SDApin=[self.conf['parameters'][SPIBB_pins.SDA.value*self.N+x] for x in range(self.N)]
        CLKdev=self.conf['devreg'][SPIBB_pins.CLK.value]
        CLKpin=[self.conf['parameters'][SPIBB_pins.CLK.value*self.N+x] for x in range(self.N)]
        DIRdev=self.conf['devreg'][SPIBB_pins.SDAdir.value]
        DIRpin=[self.conf['parameters'][SPIBB_pins.SDAdir.value*self.N+x] for x in range(self.N)]
        SetI2C=self.conf['parentcls'].SetVarValuePar
        GetI2C=self.conf['parentcls'].GetVarValuePar

        def SetReg(dev,pin,value):
#           print("SetReg",pin,value)
           for x in range(1,self.N):
              SetI2C(dev,1,pin[x],value,buffer=True)
           SetI2C(dev,1,pin[0],value,buffer=False)

        def GetReg(dev,pin):
#           print("SetReg",pin,value)
           value=GetI2C(dev,1,pin[0],[0],buffer=False);
           for x in range(1,self.N):
              value+=GetI2C(dev,1,pin[x],[0],buffer=True)
#           value=np.array(value).T
#           print(value.shape)
           return value

#        ADC_address=dev.Register_W<<1; #Write
        ADC_address=address<<1;#dev.Register_W<<1; #Write
        logging.debug(str(("I2Cbb set",hex(ADC_address),value)))
        
        SetReg(DIRdev,DIRpin,[1]) #Input = high
        SetReg(CLKdev,CLKpin,[1]) #Should be high for start
	#start
        SetReg(SDAdev,SDApin,[0]) #Output = low
        SetReg(DIRdev,DIRpin,[0]) #Output = low
        SetReg(CLKdev,CLKpin,[0]) 
#        ack=[0];
        def TXbyte(b):
          for bit in "{0:{fill}8b}".format(b, fill='0'):
              SetReg(DIRdev,DIRpin,[int(bit)]) 
              SetReg(CLKdev,CLKpin,[1]) 
              SetReg(CLKdev,CLKpin,[0]) 
          SetReg(DIRdev,DIRpin,[1]) #input 
#          GetI2C(SDAdev,1,SDApin,ack)
#          print("Ack=",ack[0]);
          SetReg(CLKdev,CLKpin,[1]) 
          ack=GetReg(SDAdev,SDApin)
#          print("Ack=",ack[0]);
          SetReg(CLKdev,CLKpin,[0]) 
          return ack;
 
        ack=TXbyte(ADC_address);
        #print(len(ack),ack)
        for v in value:
           TXbyte(v);
        #stop
        SetReg(DIRdev,DIRpin,[0]) #low
        SetReg(CLKdev,CLKpin,[1]) #Should be high
        SetReg(DIRdev,DIRpin,[1]) #Input = high
        SetReg(CLKdev,CLKpin,[0]) #keep low between transmission to not generate stop/start when other i2c actice
        
        return True;

  def GetI2Cbb(self,reg_address,value):
        SDAdev=self.conf['devreg'][SPIBB_pins.SDA.value]
        SDApin=[self.conf['parameters'][SPIBB_pins.SDA.value*self.N+x] for x in range(self.N)]
        CLKdev=self.conf['devreg'][SPIBB_pins.CLK.value]
        CLKpin=[self.conf['parameters'][SPIBB_pins.CLK.value*self.N+x] for x in range(self.N)]
        DIRdev=self.conf['devreg'][SPIBB_pins.SDAdir.value]
        DIRpin=[self.conf['parameters'][SPIBB_pins.SDAdir.value*self.N+x] for x in range(self.N)]
        SetI2C=self.conf['parentcls'].SetVarValuePar
        GetI2C=self.conf['parentcls'].GetVarValuePar
        def SetReg(dev,pin,value):
#           print("SetReg",pin,value)
           for x in range(1,self.N):
              SetI2C(dev,1,pin[x],value,buffer=True)
           SetI2C(dev,1,pin[0],value,buffer=False)

        def GetReg(dev,pin):
#           print("SetReg",pin,value)
           value=[GetI2C(dev,1,pin[0],[0],buffer=False)];
           for x in range(1,self.N):
              value.append(GetI2C(dev,1,pin[x],[0],buffer=True))
           value=np.array(value).T
           #print(value.shape)
           return value.flatten();

        ADC_address=1+(reg_address<<1); #Read
        logging.debug(str(("I2Cbb get",hex(ADC_address),len(value),value)))

        SetReg(DIRdev,DIRpin,[1]) #Input = high
        SetReg(CLKdev,CLKpin,[1]) #Should be high for start bit
	#start
        SetReg(SDAdev,SDApin,[0]) #Output = low
        SetReg(DIRdev,DIRpin,[0]) #Output = low
        SetReg(CLKdev,CLKpin,[0]) 
        ack=[0];
        def TXbyte(b):
          for bit in "{0:{fill}8b}".format(b, fill='0'):
              SetReg(DIRdev,DIRpin,[int(bit)]) 
              SetReg(CLKdev,CLKpin,[1]) 
              SetReg(CLKdev,CLKpin,[0]) 
          SetReg(DIRdev,DIRpin,[1]) #input 
#          GetI2C(SDAdev,1,SDApin,ack)
#          print("Ack=",ack[0]);
          SetReg(CLKdev,CLKpin,[1]) 
          ack=GetReg(SDAdev,SDApin)
#          print("Ack=",ack[0]);
          SetReg(SDAdev,SDApin,[0]) 
          SetReg(CLKdev,CLKpin,[0]) 
          return ack;

        def RXbyte(last=False):
          b=None;
          for i in range(8):
              SetReg(CLKdev,CLKpin,[1]) 
              ack=GetReg(SDAdev,SDApin)
#              print(ack[0])
#              if ack[0] is None: b=None
#              elif not b is None: b=(b<<1)+ack[0];
              if b is None:
                 b=ack
              else:
                 b=[(b[i]<<1)+ack[i] for i in range(len(ack))]
#              print("RX step",b,ack)
              SetReg(CLKdev,CLKpin,[0]) 
#          print("RXbyte",hex(b));
          SetReg(SDAdev,SDApin,[0]) 
          SetReg(DIRdev,DIRpin,[1 if last else 0]) #outout = low = ack 
          SetReg(CLKdev,CLKpin,[1]) 
          SetReg(CLKdev,CLKpin,[0]) 
          SetReg(DIRdev,DIRpin,[1])
          return b;
 
        TXbyte(ADC_address);
        status=RXbyte(last=(len(value)==0))
        step=len(status);
        step2=len(value)//step
#        print("step=",step);
#        if not(RXbyte()==0x80): return False;
        for i in range(step2):
          value2=RXbyte(last=(i==step2-1))
          for j in range(step):
            value[i+j*step2]=value2[j];
        logging.debug(str(("si status:",[hex(s) for s in status],[hex(v) for v in value])))#should be 0x80 - will fail for None!!
        #stop
        #SetI2C(DIRdev,1,DIRpin,[0]) #low
        SetReg(CLKdev,CLKpin,[1]) 
        SetReg(DIRdev,DIRpin,[1]) #Input = high
        SetReg(CLKdev,CLKpin,[0]) #keep low between transmission to not generate stop/start when other i2c actice

        return True;