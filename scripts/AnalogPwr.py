from test_common import *

name="RCU_PWR_ANALOG_on"
#name="RCU_PWR_DIGITAL_on"
#RCU=[0,1,2,3,4,5,8,12];
#RCU=[12];
RCU=[19];
RCU=[0,1,2,3];

#RCU=[2]
LEDvalue=False;
LEDvalue=True;

connect()
setRCUmask(RCU)

led=get_value(name+"_R")
print("old:",led)
for r in RCU:
  led[r]=LEDvalue

set_value(name+"_RW",led)
time.sleep(0.1)

print("new:",get_value(name+"_R"))

disconnect()
