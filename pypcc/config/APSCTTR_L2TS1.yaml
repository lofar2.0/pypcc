version: "1.0"
description: "1234"
name: "APSCTTR"

drivers:
 - name: I2C
   type: i2c
   parameters: [5] #I2C port number
 - name: I2C_CLK
   type: i2c_dev #I2C devices
   parent: I2C
   status: APSCTTR_I2C_error
 - name: SPIbb1 
   type: spibitbang2 #SPI bitbang via GPIO expander: CLK, SDI,SDO,CS
   parent: I2C_CLK
   devreg: [IO1.GPIO1,IO1.GPIO1,IO1.GPIO1,IO1.GPIO1]
   parameters: [4,7,5,6]
 - name: SPIbb2 
   type: spibitbang2 #SPI bitbang via GPIO expander: CLK, SDI,SDO,CS
   parent: I2C_CLK
   devreg: [IO2.GPIO1,IO2.GPIO1,IO2.GPIO1,IO2.GPIO1]
   parameters: [4,7,5,6]

#This is the I2C devices in the RCU
device_registers:
 - name: IO
   dim: 2
   description: IO-Expander (TCA6416APWR)
   address: [0x20,0x21]
   driver: I2C_CLK
   registers:
   - name: CONF1
     description: Direction of port1
     address: 6
     store: True
   - name: CONF2
     description: Direction of port2
     address: 7
     store: True
   - name: GPIO1
     description: Input/Ouput port 1
     address: [0,2]  #Read / Write address different
     store: True
   - name: GPIO2
     description: Input/Ouput port 2
     address: [1,3]  #Read / Write address different
     store: True

 - name: PLL2
   driver: SPIbb1
   registers:
    - name: PLL_stat
      description: PLL locked status
      address: 0x0
    - {name: r3, address: 0x03}
    - {name: r5, address: 0x05}
    - {name: r6, address: 0x06}

 - name: PLL1
   driver: SPIbb2
   registers:
    - name: PLL_stat
      description: PLL locked status
      address: 0x0
    - {name: r3, address: 0x03}
    - {name: r5, address: 0x05}
    - {name: r6, address: 0x06}

 - name: ROM
   description: 24AA02UIDT
   address: 0x50
   driver: I2C_CLK
   registers:
   - name: ID
     description: Random
     address: 0xfc
   - name: Version
     description: Set in production
     address: 0
   - name: Serial
     address: 0x20

 - name: VSENSE
   description: Monitor ADC
   address: 0x74
   device: LTC2495
   driver: I2C_CLK
   registers:
   - name: V_0
     address: 0xB080
     wait: 250
   - name: V_1
     address: 0xB880
     wait: 250
   - name: V_2
     address: 0xB180
     wait: 250
   - name: V_3
     address: 0xB980
     wait: 250
   - name: V_4
     address: 0xB280
     wait: 250
   - name: V_5
     address: 0xBA80
     wait: 250
   - name: V_6
     address: 0xB380
     wait: 250
   - name: Temp
     address: 0xA0C0
     wait: 250


variables:
  - name: APSCTTR_I2C_error
    description: 0=good, >0 indicates an I2C communication error
    driver: I2C_CLK
    rw:  ro #server RW variable, not linked to IO
    dtype: uint8

  - name: APSCTTR_monitor_rate
    description: Monitor rate in seconds
    rw:  variable 
    dtype: uint8

  - name: APSCTTR_translator_busy
    description: True when I2C line busy
    rw:  ro #server variable, not linked to IO
    dtype: boolean
    dim: 1

  - name: APSCT_PCB_ID
    description: Unique PCB ID
    driver: I2C_CLK
    devreg:  ROM.ID
    width: 32
    rw:  ro
    dtype: uint32

  - name: APSCT_PCB_version
    description: Version number
    driver: I2C_CLK
    devreg:  ROM.Version
    width: 0x80  #16 characters
    rw:  ro
    dtype: string

  - name: APSCT_PCB_number
    description: PCB number (astron.nl/webforms/IenS-Boarden/view.php?id=xxx)
    driver: I2C_CLK
    devreg:  ROM.Serial
    width: 0x80  #16 characters
    rw:  ro
    dtype: string


  - name: [APSCT_PWR_PLL_200MHz_on,APSCT_PWR_PLL_160MHz_on]
    description: CLK power status. Controlled by APSCT_xxxMHz_ON and APSCT_OFF
    rw:  ro
    dtype: boolean
    driver: I2C_CLK
    devreg:  [IO1.GPIO1,IO2.GPIO1]
    bitoffset: 1
    width: 1

  - name: APSCT_PWR_on
    description: CLK power status. Controlled by APSCT_xxxMHz_ON and APSCT_OFF
    rw:  ro
    dtype: boolean
    driver: I2C_CLK
    devreg:  IO1.GPIO2
    bitoffset: 3
    width: 1

  - name: APSCT_PPS_ignore
    rw:  rw
    dtype: boolean
    driver: I2C_CLK
    devreg:  IO1.GPIO2
    bitoffset: 2
    width: 1

  - name: [APSCT_INPUT_10MHz_good,APSCT_INPUT_PPS_good]
    rw:  ro
    dtype: boolean
    driver: I2C_CLK
    devreg:  IO2.GPIO2
    bitoffset: [0,1]
    width: 1
    monitor: true


  - name: [APSCT_PLL_200MHz_locked,APSCT_PLL_160MHz_locked]
#    description: First status pin give lock status
    rw:  ro
    dtype: boolean
    monitor: true
    driver: I2C_CLK
    devreg:  [IO1.GPIO1,IO2.GPIO1]
    bitoffset: 2
    width: 1

  - name: [APSCT_PLL_200MHz_error,APSCT_PLL_160MHz_error]
#    description: Second status pin give error
    rw:  ro
    dtype: boolean
    monitor: true
    driver: I2C_CLK
    devreg:  [IO1.GPIO1,IO2.GPIO1]
    bitoffset: 3
    width: 1

  - name: APSCT_PLL_200MHz_locked_SPI
    description: 0x81=locked
    driver: I2C_CLK
    devreg:  PLL2.PLL_stat
    width: 8
    rw:  ro
    dtype: uint8
    debug: True

  - name: APSCT_PLL_160MHz_locked_SPI
    description: 0x81=locked
    driver: I2C_CLK
    devreg:  PLL1.PLL_stat
    width: 8
    rw:  ro
    dtype: uint8
    debug: True

#  - name: [APSCT_PLL_r3,APSCT_PLL_r5,APSCT_PLL_r6]
#    driver: I2C_CLK
#    devreg:  [PLL.r3,PLL.r5,PLL.r6]
#    width: 8
#    rw:  ro
#    dtype: uint8
#    debug: True

  - name: [APSCT_IO1_GPIO1,APSCT_IO1_GPIO2,APSCT_IO2_GPIO1,APSCT_IO2_GPIO2]
    driver: I2C_CLK
    devreg:  [IO1.GPIO1,IO1.GPIO2,IO2.GPIO1,IO2.GPIO2]
    width: 8
    rw:  ro
    dtype: uint8
    debug: True

  - name: APSCT_IO2_GPIO1
    driver: I2C_CLK
    devreg:  IO2.GPIO1
    width: 8
    rw:  ro
    dtype: uint8
    debug: True

  - name: APSCT_TEMP
    description: Temperature sensor on PCB
    driver: I2C_CLK
    devreg:  VSENSE.Temp
    width: 23
    scale: 3.8265e-3
    convert_unit: Kelvin2Celsius
    rw:  ro
    dtype: double
    monitor: true

  - name: [APSCT_PWR_INPUT_3V3,APSCT_PWR_PLL_160MHz_3V3,APSCT_PWR_PLL_200MHz_3V3,APSCT_PWR_CLKDIST1_3V3,APSCT_PWR_CLKDIST2_3V3,APSCT_PWR_PPSDIST_3V3,APSCT_PWR_CTRL_3V3]
    driver: I2C_CLK
    devreg:  [VSENSE.V_0,VSENSE.V_1,VSENSE.V_2,VSENSE.V_3,VSENSE.V_4,VSENSE.V_5,VSENSE.V_6]
    width: 23
    scale: 1.12165e-6
    rw:  ro
    dtype: double
    monitor: true


methods:
  - name: APSCTTR_Init #Called after startup to load. Should have all stored registers  
    driver: I2C_CLK
    debug: True
    instructions:   
      - APSCTTR_I2C_error : 0
      - APSCT_IO1_GPIO1 : Update
      - APSCT_IO1_GPIO2 : Update
      - APSCT_IO2_GPIO1 : Update
      - APSCT_IO2_GPIO2 : Update
      - IO1.CONF1: Update
      - IO1.CONF2: Update
      - IO2.CONF1: Update
      - IO2.CONF2: Update
      - APSCTTR_Update: 0

  - name: APSCTTR_Update
    driver: I2C_CLK
    debug: True
    instructions:   
      - APSCT_PCB_ID : Update
      - APSCT_PCB_version : Update
      - APSCT_PCB_number : Update
      - APSCT_PWR_on: Update
      - APSCT_PWR_PLL_200MHz_on: Update
      - APSCT_PLL_200MHz_locked: Update
      - APSCT_PLL_200MHz_error: Update
      - APSCT_PWR_PLL_160MHz_on: Update
      - APSCT_PLL_160MHz_locked: Update
      - APSCT_PLL_160MHz_error: Update
      - APSCT_PPS_ignore : Update

  - name: APSCT_200MHz_on  
    driver: I2C_CLK
    description: Configure clock. Monitored using APSCT_PWR_on, APSCT_PLL_error and APSCT_PLL_locked
    instructions:   
     - APSCTTR_I2C_error : 0
     - IO1.CONF1: 0x2C #0010 1100 PPS/PWR output, SCLK,CS,SDI
     - IO1.CONF2: 0x00 
     - IO2.CONF1: 0x2C #0010 1100 PPS/PWR output, SCLK,CS,SDI
     - IO2.CONF2: 0x03 #
     - IO1.GPIO1: 0x42 #0100 0010 high:200MHz PLL enable, CS high
     - IO1.GPIO2: 0xF8 #PWR enable  ##Check if not 4??
     - IO2.GPIO1: 0x00 #All low
     - IO2.GPIO2: 0x00 #All low (just inputs)

     - WAIT: 200        
     - APSCT_PLL200_setup: 0
     - WAIT: 200         #ms to wait before checking lock
     - APSCTTR_Update: 0   #refresh all settings

  - name: APSCT_160MHz_on  
    driver: I2C_CLK
    description: Configure clock. Monitored using APSCT_PWR_on, APSCT_PLL_error and APSCT_PLL_locked
    instructions:   
     - APSCTTR_I2C_error : 0
     - IO1.CONF1: 0x2C #0010 1100 PPS/PWR output, SCLK,CS,SDI
     - IO1.CONF2: 0x00 
     - IO2.CONF1: 0x2C #0010 1100 PPS/PWR output, SCLK,CS,SDI
     - IO2.CONF2: 0x03 #
     - IO1.GPIO1: 0x00      
     - IO1.GPIO2: 0x08 #PWR enable  ##Check if not 4??
     - IO2.GPIO1: 0x42 #0100 0010 high:160MHz PLL enable, CS high
     - IO2.GPIO2: 0x00 #All low (just inputs)

     - WAIT: 200        
     - APSCT_PLL160_setup: 0
     - WAIT: 200         #ms to wait before checking lock
     - APSCTTR_Update: 0   #refresh all settings

  - name: APSCT_off  
    driver: I2C_CLK
    description: Switch clock off. Monitored using APSCT_PWR_on
    instructions:   
     - APSCTTR_I2C_error : 0
     - IO1.GPIO1: 0x00
     - IO1.GPIO2: 0x00 
     - IO2.GPIO1: 0x00 
     - IO2.GPIO2: 0x00 
     - APSCTTR_Update: 0   #refresh all settings

  - name: APSCT_PLL200_setup  
    driver: I2C_CLK
    debug: true
    instructions:   
#   - PLL2.0x03: 0x08 #Set power, this is default
    - PLL2.0x04: 0xCF #
    - PLL2.0x05: 0x97 #was 97, set lock time = =x17?
    - PLL2.0x06: 0x10

    - PLL2.0x07: 0x04 #Stop R divider
    - PLL2.0x08: 0x01 #Set R divider
    - PLL2.0x07: 0x00 #Start R divider

    - PLL2.0x09: 0x10 #Stop N divider
    - PLL2.0x0A: 0x14 #Set N divider=20, 200MHz/20=10MHz = input clock
    - PLL2.0x09: 0x00 #Start N divider

    - PLL2.0x0D: 0x01 #Divider output 1=1 
    - PLL2.0x0F: 0x01 #Divider output 2=1
    - PLL2.0x11: 0x01 #Divider output 3=1
    - PLL2.0x13: 0x01 #Divider output 4=1

  - name: APSCT_PLL160_setup  
    driver: I2C_CLK
    debug: true
    instructions:   
#   - PLL1.0x03: 0x08 #Set power, this is default
    - PLL1.0x04: 0xCF #
    - PLL1.0x05: 0x97 #was 97, set lock time = =x17?
    - PLL1.0x06: 0x10

    - PLL1.0x07: 0x04 #Stop R divider
    - PLL1.0x08: 0x01 #Set R divider
    - PLL1.0x07: 0x00 #Start R divider

    - PLL1.0x09: 0x10 #Stop N divider
    - PLL1.0x0A: 0x10 #Set N divider=16, 160MHz/16=10MHz = input clock
    - PLL1.0x09: 0x00 #Start N divider

    - PLL1.0x0D: 0x01 #Divider output 1=1 
    - PLL1.0x0F: 0x01 #Divider output 2=1
    - PLL1.0x11: 0x01 #Divider output 3=1
    - PLL1.0x13: 0x01 #Divider output 4=1



