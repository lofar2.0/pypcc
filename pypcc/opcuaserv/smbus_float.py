def smbus_2bytes_to_float(data):
    #PMBus Specification, part II, section 7.1
    expo = ((data & 0xf8)>>3)  #5 bit 2's complement
    if expo >= 2**4: expo-=2**5
    if expo == 1: #PK: Reference to this special case?
        expo = -7
    mantisse = ((data & 0x7)<<8) + ((data & 0xff00) >> 8) #11 bits 2's complement
    if mantisse >= 2**10: mantisse-=2**11;
    output = mantisse * 2**expo
    return output

def smbus_2bytes_to_float_exp12(data,expo=-12):
    mantisse = ((data & 0xff)<<8) + ((data & 0xff00) >> 8)
#    print(data,mantisse,expo)
    output = mantisse * 2**expo
    return output

def smbus_2bytes_to_float_exp13(data):
    return smbus_2bytes_to_float_exp12(data,expo=-13)


def si4012_6bytes_to_pwr(data):
    M=20/79.3
    Reg0=-25.5
    Step=-28.9
    pwr=data>>32
    if pwr>=256: pwr-=128;
    pwr=pwr*M+Reg0+Step*(pwr>126)
    return pwr;

def si4012_6bytes_to_pwr_inv(data):
    pwr=data;
    Reg0=-25.5
    if pwr<Reg0: pwr=Reg0
    if pwr>Reg0+22: pwr=Reg0+22
    M=20/79.3
    Step=-28.9
    pwr=(pwr-Reg0)/M
    pwr-=Step/M*(pwr>75)
    pwr=int(pwr)
#    print(pwr)
    if pwr>=128: pwr+=128;
    pwr<<=32
#    pwr+=0<<16 #Cap
    pwr+=125<<8 #fAlphaSteps #default
    pwr+=127    #fBetaSteps #default
    return pwr

def bool_to_int12(data):
    return 1 if data else 2

def bool_to_int12_inv(data):
    return data==1