name='RECVTR_HB' #YAML config file with all register values etc
logPath='log'

import logging
import argparse
from opcuaserv import opcuaserv
from opcuaserv import i2client
from opcuaserv import yamlreader
#from opcuaserv import pypcc2
from i2cserv import i2cthread
import threading
import time
import sys
import signal
from yamlconfig import Find;
import yamlconfig as yc
from datetime import datetime

testtime=datetime.now().strftime("%y-%m-%d %H:%M")

RCUNR=(0 if len(sys.argv)<2 else int(sys.argv[1]));
print("RCU NR=",RCUNR," tested at ",testtime);
logging.basicConfig(level="WARNING",format='%(asctime)s [%(levelname)-8s,%(filename)-20s:%(lineno)-3d] %(message)s')
#logging.basicConfig(level="DEBUG",format='%(asctime)s [%(levelname)-8s,%(filename)-20s:%(lineno)-3d] %(message)s')

RunTimer=True;
conf=yc.yamlconfig(name)
conf.linkdevices()
conf.loaddrivers()
conf.linkdrivers()

Bands=[2,1,4]
Band=Bands[2]; #0=110, 1=170, 2=210
DAB=0; #1=DAB filter enables
Att=10;

def GetVal(name,N=1):
 varid=conf.getvarid(name);
 var1=conf.getvars()[varid]
 drv=var1.get('drivercls');
 mask=[False]*RCUNR*N+[True]*N+[False]*((32-RCUNR-1)*N);
 data=drv.OPCUAReadVariable(varid,var1,mask)
 data=data[0].data
 N3=len(data)//32;
 return data[N3*RCUNR:N3*(RCUNR+1)],var1


data,var1=GetVal('RCU_PCB_ID');
ID=("%.2x%.2x%.2x%.2x" % (data[0],data[1],data[2],data[3]))

rootLogger = logging.getLogger()
#fileHandler = logging.FileHandler("{0}/{1}.log".format(logPath, "RCU2L"+ID))
#fileHandler.setFormatter(logFormatter)
#rootLogger.addHandler(fileHandler)

logging.warning("ID=%s" % ID)
now=datetime.now();
logging.warning("Time="+now.strftime("%Y-%m-%d %H:%M"));
#exit();
#print("Check 
def Check(D,Dmin,Dmax):
  for d in D:
    if (d<Dmin): 
        logging.error("Value to small");
#        return False;
    if (d>Dmax): 
        logging.error("Value to large");
#        return False;
  return True

if True:
 data,var1=GetVal('RCU_TEMP');
 D=((data[0]*256+data[1])*256+data[2])*var1.get('scale',1.)
 logging.warning("RCU Temperature=%.3f K" % D)
 if not Check([D],290,350): exit();

 data,var1=GetVal('RCU_PWR_3V3');
 D=((data[0]*256+data[1])*256+data[2])*var1.get('scale',1.)
 logging.warning("3V3 =%.3f V" % D)
# print("3V3=",D,"V")
 if not Check([D],3.2,3.4): exit();
# if (D<3.2) or (D>3.4): exit()
#print("data=",[hex(d) for d in data[:3]]);
#exit()

def SetRegister(regname,value):
  methodid=conf.getmethodid("RCU_on");
  var1=conf.getmethod(methodid)
  drv=var1.get('drivercls');
  v1=conf.getdevreg(regname)
  drv2=v1.get('drivercls')
  mask=[False]*(RCUNR)+[True]+[False]*(31-RCUNR);
  if drv:  drv.Setdevreg(v1,value,mask)
  elif drv2: drv2.Setdevreg(v1,value,mask)
  else: logging.warn("Driver not specified for instruction"+key)



if True:
# logging.warning("Switch RCU Power off");
# SetRegister("IO1.GPIO1",[0])
# SetRegister("IO1.GPIO2",[0x0]) #Analog power off
# SetRegister("IO2.GPIO1",[0x0]) #Digital power off
# SetRegister("IO2.GPIO2",[0x80])
 SetRegister("IO1.CONF1",[0])
 SetRegister("IO1.CONF2",[0])
 SetRegister("IO2.CONF1",[0x80]) #Pgood on 0x80
 SetRegister("IO2.CONF2",[0])
 SetRegister("IO4.CONF1",[0xC0])#pin 0x40, 0x80 not used
 SetRegister("IO4.CONF2",[0xF8])
 time.sleep(1.0)
 logging.warning("Switch RCU Power on");
 SetRegister("IO1.GPIO1",[Att])#0x0a = 10dB att
 SetRegister("IO1.GPIO2",[0x80+Att])#0x80 Analog on, 0x0a=10dB att
 SetRegister("IO2.GPIO1",[0x40+Att])#0x40 Dig on, 0x0a =10dB att
 SetRegister("IO2.GPIO2",[0x40+Band+(Band<<3)])#0x09 #Band0 (or 0x12 or 0x24)  #LED green=on=low 0x40
 SetRegister("IO4.GPIO1",[0x51 if DAB else 0x2A])#DAB switch states: 0x2A or 0x51
 SetRegister("IO4.GPIO2",[Band])#Band select
 time.sleep(0.5)

# logging.warning("  Check IO expander 1&2");
# print("IO expander status:");
# data,var=GetVal('RCU_IO1_GPIO1');
# if not Check(data,0,0): 
#   logging.warning("  IO1_GPIO1 =%x" % data[0])
#   exit();
# data,var=GetVal('RCU_IO1_GPIO2');
# if not Check(data,0x80,0x80): 
#   logging.warning("  IO1_GPIO2 =%x" % data[0])
#   exit();
# data,var=GetVal('RCU_IO2_GPIO1');
# if not Check(data,0xC0,0xC0): 
#   logging.warning("  IO2_GPIO1 =%x" % data[0])
#   exit();
# data,var=GetVal('RCU_IO2_GPIO2');
# if not Check(data,0x40,0x40): 
#   logging.warning("  IO2_GPIO2 =%x" % data[0])
#   exit();
# time.sleep(0.5)
#exit()
if True:
 logging.warning("Check voltages")
 data,var1=GetVal('RCU_PWR_1V8');
 D=((data[0]*256+data[1])*256+data[2])*var1.get('scale',1.)
 logging.warning("  1V8 =%.3f V" % D)
 if not Check([D],1.7,1.9): exit();

 data,var1=GetVal('RCU_PWR_2V5');
 D=((data[0]*256+data[1])*256+data[2])*var1.get('scale',1.)
 logging.warning("  2V5 =%.3f V" % D)
 if not Check([D],2.4,2.6): exit();
if False:
 data,var1=GetVal('RCU_PWR_ANT_VIN');
 D0=((data[0]*256+data[1])*256+data[2])*var1.get('scale',1.)
 D1=((data[3]*256+data[4])*256+data[5])*var1.get('scale',1.)
 D2=((data[6]*256+data[7])*256+data[8])*var1.get('scale',1.)
 logging.warning("  Vant_in=%.2f %.2f %.2f" % (D0,D1,D2))
 if not Check([D0,D1,D2],5,10): exit();

 logging.warning("Switch Antenna power on")
 SetRegister("IO1.GPIO1",[0xC0]) #Antenna power on
 SetRegister("IO1.GPIO2",[0xC0]) #Analog power on

 data,var1=GetVal('RCU_PWR_ANT_VOUT');
 D0=((data[0]*256+data[1])*256+data[2])*var1.get('scale',1.)
 D1=((data[3]*256+data[4])*256+data[5])*var1.get('scale',1.)
 D2=((data[6]*256+data[7])*256+data[8])*var1.get('scale',1.)
 logging.warning("  Vant_out=%.2f %.2f %.2f" % (D0,D1,D2))
 if not Check([D0,D1,D2],5,10): exit();

 data,var1=GetVal('RCU_PWR_ANT_IOUT');
 D0=((data[0]*256+data[1])*256+data[2])*var1.get('scale',1.)
 D1=((data[3]*256+data[4])*256+data[5])*var1.get('scale',1.)
 D2=((data[6]*256+data[7])*256+data[8])*var1.get('scale',1.)
 logging.warning("  Iant_out=%.2f %.2f %.2f" % (D0,D1,D2))
# print("Iant=",D0,D1,D2)
#exit()

if True:
 logging.warning("Test ADC read")
 logging.warning("  Check IO expander 3");
 SetRegister("IO3.GPIO1",[0x15]) #ADC_SDIO=high, clk=low,  DTH_EN=low
 SetRegister("IO3.GPIO2",[0x47]) #ADC SC=high, DTH_SDA=high
 SetRegister("IO3.CONF1",[0]) #All output
 SetRegister("IO3.CONF2",[0])
 data,var=GetVal('RCU_IO3_GPIO1');
 if not Check(data,0x15,0x15): exit();
 data,var=GetVal('RCU_IO3_GPIO2');
 if not Check(data,0x47,0x47): exit();

#if True:
# SetRegister("IO3.GPIO1",[0xD5]) #ADC_SDIO=high, clk=low,  DTH_EN=low
# SetRegister("IO3.GPIO2",[0xC7]) #ADC SC=high, DTH_SDA=high

# SetRegister("RCU_DTH_shutdown

if False:
 #Test reading register from ADCs
 data,var=GetVal('RCU_ADC_JESD');
 logging.warning("  ADC JESD (0x14)=%x %x %x" % (data[0],data[1],data[2]))
 if not Check(data,0x14,0x14): exit();

 logging.warning("Test ADC write & ADC lock")
 #Test writing ADC register
 SetRegister("ADC1.SYNC_control",[1])
 SetRegister("ADC1.CML_level",[7]);
 SetRegister("ADC1.Update",[1])
 SetRegister("ADC2.SYNC_control",[1])
 SetRegister("ADC2.CML_level",[7]);
 SetRegister("ADC2.Update",[1])
 SetRegister("ADC3.SYNC_control",[1])
 SetRegister("ADC3.CML_level",[7]);
 SetRegister("ADC3.Update",[1])
 data,var=GetVal('RCU_ADC_sync');
 logging.warning("  ADC sync (0x1)=%x %x %x" % (data[0],data[1],data[2]))
 if not Check(data,0x1,0x1): exit();

 data,var=GetVal('RCU_ADC_locked');
 logging.warning("  ADC locked (0x1)=%x %x %x" % (data[0],data[1],data[2]))
 if not Check(data,0x1,0x1): logging.error("*** FAILED ADC lock!")
#      - RCU_ADC_locked: Update

#exit()
#Need to update I2c bitbang....
if False:
 logging.warning("Test DITHER communication")
 SetRegister("IO3.GPIO1",[0x15]) #ADC_SDIO=high, clk=low,  DTH_SDN=low
 SetRegister("IO3.GPIO2",[0x47]) #ADC SC=high, DTH_SDA=high
 SetRegister("IO3.CONF1",[0]) #All output
 SetRegister("IO3.CONF2",[0x0]) #All output (DTH_SDA=input)


# data,var=GetVal('RCU_IO3_GPIO1');print("IO3_1",hex(data[0]))
# data,var=GetVal('RCU_IO3_GPIO2');print("IO3_2",hex(data[0]))
 f0=102.2e6;
 f=int(f0)
 logging.warning("  Set frequency %f"%f0);
# print("Frequency set=",f)
 d=[0]*4;
 for i in range(4):
   d[3-i]=f%256;f//=256
# print([hex(h) for h in d])
 SetRegister("DTH1.Freq",d) #DTH_SDA=input
 SetRegister("DTH2.Freq",d) #DTH_SDA=input
 SetRegister("DTH3.Freq",d) #DTH_SDA=input

 f=int(f0)
 data,var1=GetVal("RCU_DTH_freq")
# data,var1=GetVal("RCU_DTH_Rev")
 for j in range(3):
   f2=0;
   for i in range(4):
     f2=f2*256+data[j*4+i];
   #print("Frequency read back=",f)
   logging.warning("  Readback frequency %f" % f2);
   if not Check([f2],f-1,f+1): exit();
# print([hex(h) for h in data[:30]])

 logging.warning("Test DITHER on")
 SetRegister("DTH1.CONF",[0])
 SetRegister("DTH1.Tune",[0,0])
 SetRegister("DTH1.Start",[0,1,0,0,1])
 SetRegister("DTH2.CONF",[0])
 SetRegister("DTH2.Tune",[0,0])
 SetRegister("DTH2.Start",[0,1,0,0,1])
 SetRegister("DTH3.CONF",[0])
 SetRegister("DTH3.Tune",[0,0])
 SetRegister("DTH3.Start",[0,1,0,0,1])
 time.sleep(0.1)
 data,var=GetVal('RCU_DTH_on');
 logging.warning("  DTH on (0x1)=%x %x %x" % (data[0],data[1],data[2]))
 if not Check(data,0x1,0x1): logging.error("*** FAILED: Dither on! ***")


logging.warning("** PASSED Power and Control test **");


#print(data)
#scale=float(scale)
#data2=[(d*scale) for d in data2]

#print("ID=",[hex(d) for d in data[:4]]);



