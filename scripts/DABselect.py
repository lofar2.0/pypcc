from test_common import *

name="RCU_DAB_filter_on"
#RCU=[0,1,2,3,4,5,8,12];
RCU=[8,9,10,11];
Att=[True]*3
#Att=[False]*3

connect()
setAntmask(RCU)

att=get_value(name+"_R")
print("Att old:",att[3*RCU[0]:3*RCU[-1]+3])
for r in RCU:
  att[3*r:3*r+3]=Att
set_value(name+"_RW",att)

time.sleep(0.5)
att=get_value(name+"_R")
print("Att new:",att[3*RCU[0]:3*RCU[-1]+3])

disconnect()