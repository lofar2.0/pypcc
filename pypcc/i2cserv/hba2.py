import numpy as np
from .hwdev import hwdev
import logging
from .i2c_array import ApplyMask
from time import sleep
from pypcc.queuetypes import *
import signal
try:
  import RPi.GPIO as GPIO
except:
  GPIO=False;

class hba2(hwdev):
  def __init__(self,config):
    hwdev.__init__(self,config);
    logging.info("HBA2 driver loaded")
#    self.previousHBA=np.zeros([32,3,32],dtype='int')
    self.sleep=1 #sec

  def OPCUASetVariable(self,varid,var1,data,mask):
       logging.info(str(("AHBA set Var",var1['name'],len(data),len(mask),data[:32*3],mask[:12])))
       self.conf['parentcls'].SetGetVarValueMask(var1,data,mask,getalso=False)
       if var1.get('wait'):
            sleep(float(var1.get('wait'))/1000.)
       return [];

  def OPCUAReadVariable(self,varid,var1,mask):
      logging.warn(self.conf['name']+" HBAT2 ReadVariable %i %s",varid,var1["name"])
      if var1.get('wait'):
            self.sleep=float(var1.get('wait'))/1000.
      data,mask2=self.conf['parentcls'].GetVarValueMask(var1,mask)
      Data=OPCUAset(varid,InstType.varSet,data.copy(),mask2.copy())
      return [Data]


  def i2csetget(self,addr,data,reg=None,read=0):
    if read==0: return self.sethba(addr,reg,data)
    elif read==1: return self.gethba(addr,reg,data)
    else: logging.warn("Not implemented!")
    return False;

  def sethba(self,addr,reg,data):
#      logging.warning("SetHba2 not implemented")
      logging.info("setHba2 addr=0x%x reg=0x%x len=%i",addr,reg,len(data))
      I2Ccallback=self.conf['parentcls'].i2csetget
      if not(I2Ccallback(addr,[reg,2,data[0]],reg=0x0e)): return False; #only len=2 (BF) implemented
      return True

  def gethba(self,addr,reg,data):
      I2Ccallback=self.conf['parentcls'].i2csetget
      logging.info("getHba2 addr=0x%x reg=0x%x len=%i",addr,reg,len(data)//16)
      #request data
      if not(I2Ccallback(addr,[reg,len(data)//16],reg=0x0C)): return False;
      sleep(self.sleep)
      I2Ccallback(addr,data,read=1);
      logging.info("getHba addr=0x%x reg=0x%x data=%s",addr,reg,str((data)))
##      if data is None:  return False
#      data[:]=[255]*len(data)
      return True;
