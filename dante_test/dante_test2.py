from pypcc.i2cdirect import i2cdirect
from time import sleep

import logging
logging.basicConfig(level="ERROR",format='%(asctime)s [%(levelname)-8s,%(filename)-20s:%(lineno)-3d] %(message)s')
#logging.basicConfig(level="DEBUG",format='%(asctime)s [%(levelname)-8s,%(filename)-20s:%(lineno)-3d] %(message)s')

d1=i2cdirect('RECVTR_HB_DANTE.yaml')


if True:
 for varname in ['RCU_PCB_ID','RCU_PCB_version','RCU_PCB_number',
                'RCU_firmware_version',]:
    data,var1=d1.GetVal(varname);
    print(varname+"=",data[0])

uCaddr=0x40
HBAi2c=0x41

i2c=d1.conf.conf['drivers'][0]['obj'] #assume I2C is first device
v=[0]
i2c.i2csetget(uCaddr,v,reg=2 ,read=1);print("RCU uC Mode",v[0]);
i2c.i2csetget(uCaddr,v,reg=13,read=1);print("RCU uC Vref",v[0]);
i2c.i2csetget(uCaddr,v,reg=5 ,read=1);print("RCU uC Readback",v[0]);
i2c.i2csetget(uCaddr,v,reg=14,read=1);print("RCU uC start high",v[0]);
#exit()

if True:
#    print("Set Vref=0x0c");
#    i2c.i2csetget(uCaddr,[0x0c],reg=13,read=0)
    print("Set mode=1");
    i2c.i2csetget(uCaddr,[1],reg=2,read=0)
    print("Set Auto readback=0");
    i2c.i2csetget(uCaddr,[0],reg=5,read=0)
#    print("TX start high=0");
#    i2c.i2csetget(uCaddr,[4],reg=14,read=0)
v=[0]*8
i2c.i2csetget(HBAi2c,v,reg=0,read=1);print("HBA readback",v);

exit()

#RCU_off(d1)
if False:
    d1.runmethod("RCU_off");
    sleep(1.0)
if True:
    d1.runmethod("RCU_on");
    sleep(1.0)
if True:
    d1.runmethod("ADC_on");
enable_ant_pwr(d1)
#dither_config(d1)

if True:
 for varname in ['RCU_PCB_ID','RCU_PCB_version','RCU_PCB_number',
                'RCU_firmware_version',
            'RCU_PWR_good','RCU_PWR_DIGITAL_on','RCU_PWR_ANALOG_on',
            'RCU_IO1_GPIO1','RCU_IO1_GPIO2','RCU_IO2_GPIO1','RCU_IO2_GPIO2',
            "RCU_LED_red_on","RCU_LED_green_on","RCU_DTH_shutdown"]:
    data,var1=d1.GetVal(varname);
    print(varname+"=",data[0])
if True:
    LED=0
    data=d1.SetVal("CH1_HBAT_BF2_byte",[0x80+LED]*32)
#    print(data[0].data)
    data=d1.SetVal("CH2_HBAT_BF2_byte",[0x80+LED]*32)
#    print(data[0].data)
    data=d1.SetVal("CH3_HBAT_BF_byte",[0x80+LED]*32)
#    print(data[0].data)
    data,var1=d1.GetVal("CH1_HBAT_BF2_byte")
    print(data)
    data,var1=d1.GetVal("CH2_HBAT_BF2_byte")
    print(data)
    data,var1=d1.GetVal("CH3_HBAT_BF2_byte")
    print(data)
if True:
 for varname in ['RCU_TEMP','RCU_PWR_3V3','RCU_PWR_2V5','RCU_PWR_1V8']:
    data,var1=d1.GetVal(varname);
    print(varname+"=",data[0])

if True:
 for ch in ['CH1','CH2','CH3']:
  print("Channel ",ch)
  for varname in ['band_select','attenuator_dB','ADC_shutdown',
     'PWR_ANT_on','PWR_ANT_VOUT','PWR_ANT_VIN','PWR_ANT_IOUT',
     'ADC_locked','ADC_sync','ADC_JESD','ADC_CML_level',
     'DTH_freq','DTH_PWR','DTH_on',
     ]:
    data,var1=d1.GetVal(ch+'_'+varname);
    print("  ",varname+"=",data[0])
