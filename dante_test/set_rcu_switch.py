#!/usr/local/bin/python3.11
from pypcc.i2cdirect import i2cdirect
from time import sleep
import hbat1 as hb
d1=i2cdirect('RECVTR_HB')
RCUNR=14

def GetVal(d1,name,N=1):
 varid=d1.conf.getvarid(name);
# print("varid",varid)
 var1=d1.conf.getvars()[varid]
 dim=var1['dim']
 drv=var1.get('drivercls');
 mask=[False]*RCUNR*N+[True]*N+[False]*((dim-RCUNR-1)*N);
 data=drv.OPCUAReadVariable(varid,var1,mask)
 data=data[0].data
 N3=len(data)//dim
 return data[N3*RCUNR:N3*(RCUNR+1)],var1

#reading the ID also set the switch
varID='RCU_PCB_ID'

data,var1=GetVal(d1,varID);
ID=("%.2x%.2x%.2x%.2x" % (data[0],data[1],data[2],data[3]))
print('rcunr=',RCUNR,'ID=',ID)
