import yaml
import struct
import time
from pypcc.yamlconfig import *
import logging
import numpy as np
from .smbus_float import *
from pypcc.opcuaserv import convert_unit
def bytes2int(bts):
   x=0;
   for b in bts:
     x=x*256+b;
   return x;

def int2bytes(i):
   b=[];
   while i>255: 
        b=[i%256]+b;
        i>>=8;
   return [i]+b;

def byte2var(v,data):
#        v=self.conf['variables'][id1];
        dtype=v.get('dtype','integer');
        width=(v.get('width',8)-1)//8+1
        endian=v.get('endian','>');
        logging.debug(str(("OPCset",width,data)))
        notvalid=[(d is None) for d in data[::width]]
        cnt=int(len(data)//width)
        assert(cnt==len(notvalid))
#        print(notvalid)
        for x in range(len(data)):
               if data[x] is None: data[x]=0; 
#        print(data)
        if dtype=="boolean": 
                data2=[d==1 for d in data]; 
                convert=v.get("convert_unit")
                if convert: 
                    data2=[eval("convert_unit."+convert)(d) for d in data2]
                    for x in range(cnt): 
                        if notvalid[x]: data2[x]=False;
        elif (dtype in ['uint8','uint16','uint32','uint64','double']): 
            data=bytearray(data)
            if width<=1: 
                data2=[d for d in data]
            elif width==2:
                data2 = struct.unpack(endian+'%sH' % (len(data)//2), data)
            elif width==3:
                data2 = [struct.unpack(endian+'L' ,bytearray([0])+data[x*3:x*3+3])[0] for x in range(len(data)//3)]
            elif width==4:
                data2 = struct.unpack(endian+'%sL' % (len(data)//4), data)
            elif width==6:
                data2 = [struct.unpack(endian+'Q' ,bytearray([0,0])+data[x*6:x*6+6])[0] for x in range(len(data)//6)]
            elif width==8:
                data2 = struct.unpack(endian+'%sQ' % (len(data)//8), data)
            else:
                logging.warn("OPCset"+v['name']+" unsupported width!"+str(width))
                return;
        elif dtype=="string": 
#                data2=[(bytearray(data[i*width:(i+1)*width]).decode("utf-8")) for i in range(cnt)]
                data2=[bytearray(data)[i*width:(i+1)*width].decode("utf-8",errors='ignore') for i in range(cnt)]
                for x in range(cnt): 
                       if notvalid[x]: data2[x]="";
        else:
                logging.warn("OPCset unsupported type");
                return;
        if dtype=="double": 
                scale=v.get('scale',1.)
                if scale in ["smbus_2bytes_to_float","smbus_2bytes_to_float_exp12","smbus_2bytes_to_float_exp13","si4012_6bytes_to_pwr"]: 
                     data2=[float(eval(scale)(d)) for d in data2]
                else:
                    scale=float(scale)
                    data2=[(d*scale) for d in data2]
                convert=v.get("convert_unit")
                if convert: data2=[eval("convert_unit."+convert)(d) for d in data2]
                for x in range(cnt): 
                    if notvalid[x]: data2[x]=float('nan')
        return data2

def var2byte(v,data):
        endian=v.get('endian','>');
        mask=v.get('maskOPC',None);
        mask=mask.get_value() if (mask!=None) else [];
#        print("M2:",mask)
        dtype=v.get('dtype','integer');
        width=(v.get('width',8)-1)//8+1
        if not(isinstance(data,list)): data=[data];
        if (dtype=="double"): 
                scale=v.get('scale',1.)
                if scale in ["si4012_6bytes_to_pwr"]: 
                     data=[int(eval(scale+"_inv")(d)) for d in data]
                else:
                    scale=float(scale)
                    data=[int(d/scale) for d in data]
        if (dtype=="boolean"): 
                convert=v.get("convert_unit")
                if convert: data=[eval("convert_unit."+convert+"_inv")(d) for d in data]
                data2=bytearray(data*1);
        elif (dtype in ['uint8','uint16','uint32','uint64','double']): 
            if width<=1: 
                data2=bytearray(data)
            elif width==2:
                data2 = struct.pack(endian+'%sH' % len(data), *data)
            elif width==3:
                data2=bytearray()
                for a in data: data2.extend(struct.pack('>L',a)[1:])
            elif width==4:
                data2 = struct.pack(endian+'%sL' % len(data), *data)
            elif width==6:
                data2=bytearray()
                for a in data: data2.extend(struct.pack('>Q',a)[2:])
            elif width==8:
                data2 = struct.pack(endian+'%sQ' % len(data), *data)
            else:
                logging.warn("setvar"+v['name']+" unsupported width!"+str(width))
                return None;
        elif (dtype=="string"):
                data2=bytearray() 
                for s in data:
                    data2.extend('{s:>{width}'.format(s=s[:width],width=width).encode('ascii'));
        else:
                logging.warn("setvar unsupported type");
                return None;
        data2=[d for d in data2]
        return data2,mask

class yamlreader(yamlconfig):
    def __init__(self,i2cserver,yamlfile='RCU'):
        self.yamlfile=yamlfile;
        yamlconfig.__init__(self,yamlfile)
        self.basename=self.conf.get("name",yamlfile);
        self.server=i2cserver;
        self.timecount=1000; #0 start a 0, 1000 first point immediate
        self.monitorvarcnt=0;
        self.statusid=self.getvarid(self.basename+"_translator_busy");
        self.monitorvarid=self.getvarid(self.basename+"_monitor_rate");
        self.statusOPC=None
        self.lastSend=time.time()
        self.lastRecv=time.time()

    def SetBusy(self):
        if self.statusid is None: return
        self.OPCset(self.statusid,[1],[])

    def GetBusy(self):
        if self.statusid is None: return False
        v=self.conf['variables'][self.statusid];
#        self.OPCset(self.statusid,[1],[])
        var1=v.get('OPCR',None)
        if var1 is None: var1=v.get('OPCW',None)
        if var1 is None:
           logging.warn("OPC variable not found!!");
           return False;
        data3=var1.get_value();
        return data3

    def AddVars(self,AddVarR,AddVarW):
     for v in self.conf['variables']:
#        print(v)
        dim1=v.get('dim',1);
        name=v.get('name');
        datatype=v.get('dtype','integer')
#        dim1=Vars.RCU_MPaddr.nI2C*Vars.RCU_MPaddr.nSwitch*v.nVars
#        dim2=Vars.RCU_MPaddr.nI2C*Vars.RCU_MPaddr.nSwitch*v.size
#        dim3=int(v.size/v.nVars)
        #print(v.name,dim)
        varvalue2=0
        if   datatype in ['uint8','uint16','uint32','uint64']:  varvalue2=dim1*[0]
        elif datatype=='double':    varvalue2=dim1*[np.nan] 
        elif datatype=='boolean':  varvalue2=dim1*[False] 
        elif datatype=='string':   varvalue2=dim1*[""]
        if len(varvalue2)==1: varvalue2=varvalue2[0];
#        print(len(varvalue2),varvalue2)
        if v.get('rw') in ['ro','rw','variable']:
            var1=AddVarR(name+"_R",varvalue2,v['id'],v.get('debug'))
            if not(var1 is None):
              v['OPCR']=var1
              logging.debug("Var added:"+name+"_R")
#            self.server.readvar(v['id'])
#            time.sleep(0.1);
#            Inst=Vars.Instr(Vars.DevType.VarUpdate,v,dim2,varvalue2)
#            Q1.put(Inst)

        if v.get('rw') in ['wo','rw','variable']:
            var1=AddVarW(name+"_RW",varvalue2,v['id'],self,v.get('debug'))
            if not(var1 is None):
              v['OPCW']=var1
              logging.debug("Var added:"+name+"_RW")
     for v in self.conf['variables']:
        mask=v.get('mask');
        if not(mask): continue;
        mask=Find(self.conf['variables'],'name',mask)
        if not(mask): continue;
        mask=mask.get('OPCR',None)
        if (mask is None):
           mask=mask.get('OPCW',None)
        if (mask==None): continue;
        v['maskOPC']=mask
     if not self.monitorvarid is None:
        self.monitorvar=self.conf['variables'][self.monitorvarid].get('OPCW')
        self.monitorvar.set_value(30);
        self.setvar(self.monitorvarid,30);
     else: 
       logging.warning("No monitor_rate variable found")
       self.monitorvar=None;
     if not self.statusid is None:
        self.statusOPC=self.conf['variables'][self.statusid].get('OPCR')
     else:
       logging.warning("No translator_busy variable found");
       self.statusOPC=None;

    def AddMethod(self,Addmethod):
      for v in self.conf['methods']:
          if v.get('hidden'): continue;
#          print(v)
#        Inst1=Vars.Instr(Vars.DevType.Instr,v,0,[])
          Addmethod(v['name'],v['id'],self,v.get('debug'))
          mask=v.get('mask');
          if not(mask): continue;
          mask=Find(self.conf['variables'],'name',mask)
          if not(mask): continue;
          mask=mask.get('OPCW',None)
          if (mask==None): continue;
          v['maskOPC']=mask

    def callMethod(self,id1):
        v=self.conf['methods'][id1];
        logging.warning("Method called:"+v['name'])
        mask=v.get('maskOPC',None);
        mask=mask.get_value() if (mask!=None) else [];
        if not(self.GetBusy()): self.lastRecv=time.time()
        self.SetBusy()
        self.lastSend=time.time()
        self.server.callmethod(id1,mask) 

    def CallInit(self):
        v=Find(self.conf['methods'],'name',self.basename+'_Init');
        if not(v):
            logging.warn(self.basename+"_Init method not found for initialisation!") 
            return;
        self.callMethod(v['id'])

    def setvar(self,id1,data=[]):
        v=self.conf['variables'][id1];
        if v['rw']=='variable': 
          var1=v.get('OPCR',None)
#          if not(var1): var1=v.get('OPCW')
          if var1 is None:
             logging.warn("OPCR variable not found!!");
             return;
          logging.info("Update variable")
          var1.set_value(data);
          return;
        data2,mask=var2byte(v,data)
        logging.info(str(("setvar ",v['name'],data2,mask)));
        if data2 is None: return 
        if not(self.GetBusy()): self.lastRecv=time.time()
        self.SetBusy()
        self.lastSend=time.time()
        self.server.setvar(id1,data2,mask) 

    def getvar(self):
#        if not(self.server.data_waiting()): return;
#        print("getvar ...")
        while True:
#         while (self.server.data_waiting()):
#           try:
            item=self.server.readdata()
            if item is None: break;
            self.lastRecv=time.time()
            id1,data,mask=item; 
            logging.debug(str(("**getvar",id1,data,mask)));
            if len(data)==0: continue;
#           except:
#              print('finished')
#              return;
            self.OPCset(id1,data,mask);

    def OPCset(self,id1,data,mask):
        v=self.conf['variables'][id1];
        data2=byte2var(v,data)
        var1=v.get('OPCR',None)
        if var1 is None: var1=v.get('OPCW',None)
        if var1 is None:
           logging.warn("OPC variable not found!!");
           return;
        data3=var1.get_value();
        datatype=var1.get_data_value().Value.VariantType
        if not(isinstance(data3,list)): data3=[data3];
#        print("OPCset",v['name'],data2[:64],mask)
        if mask: #Only update masked values
            step=len(data2)//len(mask)
#            print("mask step=",step,len(mask),len(data2),len(data3),data2)
            for i in range(len(data2)):
                if mask[i//step]: data3[i]=data2[i]
        else:
            data3=data2;
#        print("OPCset",v['name'],data3[:64],mask)
        logging.info(str(("OPCset",v['name'],data3[:64],mask)))
#        if v['name']=='UNB2_FPGA_POL_ERAM_IOUT': logging.warn(str((data3,data2,mask,var1.get_value(),[hex(d) for d in data])));
        if len(data3)==1: data3=data3[0];
        var1.set_value(data3,datatype);
    def watchdog(self):
#        print(self.lastRecv,self.lastSend,self.lastSend-self.lastRecv)
        return  self.lastSend-self.lastRecv; 
    def Monitor(self):
        if self.monitorvar is None: return
        T1=self.monitorvar.get_value()*10;
        if T1<=0: return
        self.timecount+=1;
#        if self.statusOPC is None: self.statusOPC=self.conf['variables'][self.statusid].get('OPCR')
        while self.timecount>=T1:
          if self.GetBusy():
              logging.info("Busy, try monitor again in 1s")
              self.lastSend=time.time()
              self.timecount=T1-10; #busy, so try again in 1s
              return
          if not(self.statusOPC is None) and self.statusOPC.get_value(): return;
          if self.server.QoutLength()>0: 
             self.lastSend=time.time()
             return;
          v=self.conf['variables'][self.monitorvarcnt];
          if v.get('monitor'):
               mask=(v['maskOPC'].get_value() if v.get('maskOPC') else [])
               if isinstance(mask,list) and (len(mask)>0) and isinstance(mask[0],int):
                   mask=[m==0 for m in mask]
#               print("monitor",v['name'],mask)
#               self.SetBusy()
               self.lastRecv=time.time()
               self.server.readvar(self.monitorvarcnt,mask=mask)
          self.monitorvarcnt+=1;
          if self.monitorvarcnt>=len(self.conf['variables']): 
                    self.monitorvarcnt=0;
                    self.timecount=0;              
#        self.OPCset(self.statusid,[1],[])
