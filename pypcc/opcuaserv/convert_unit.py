def Kelvin2Celsius(T):
  return T-273.15;

def period2RPM(T):
  if T==0: return 0.
  return 60./T;

def bool_invert(T):
  return not(T);

def bool_invert_inv(T):
  return not(T);

def temp_check(T):
  if (T<-100) or (T>200): return float('nan')
  return T;

def int12_to_bool_inv(data):
    return 1 if data else 2

def int12_to_bool(data):
    return data==1
