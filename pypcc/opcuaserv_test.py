#from opcua import ua, Server
from asyncua.sync import ua, Server
from time import sleep
class SubHandler(object):
    """
    Subscription Handler. To receive events from server for a subscription
    """
    def datachange_notification(self, node, val, data):
        print("Datachange callback",node,val,data)
#    def datachange_notification(self, node, val, data):
#        logging.warning(str(("Python: New data change event", node, val, data.monitored_item.Value.StatusCode)))
#        logging.warning(str(("Python: Data came from client: ", (data.monitored_item.Value.StatusCode == ua.StatusCode(ua.StatusCodes.Good)))))

        # if data came from client, mark the status to get a new notification if the client rewrites the same value
        if data.monitored_item.Value.StatusCode == ua.StatusCode(ua.StatusCodes.Good):
            print(str(("Python: New client data change event", node, val, data.monitored_item.Value.StatusCode)))
            #node.set_value(ua.DataValue(val, ua.StatusCode(ua.StatusCodes.GoodCompletesAsynchronously)))
            #data.monitored_item.Value.StatusCode=ua.StatusCodes.GoodCompletesAsynchronously


if True:
    server = Server()
    server.set_endpoint("opc.tcp://0.0.0.0:1234/")
    idx = server.register_namespace("http://lofar.eu")
    print("idx=",idx)
    PCCobj = server.nodes.objects #server.get_objects_node()
    handler = SubHandler()
    sub = server.create_subscription(50, handler)
    myvar2 = PCCobj.add_variable(idx, "test1", 1.5)
    myvar2.set_writable()
    handle = sub.subscribe_data_change(myvar2)
    server.start()
    sleep(0.3)

from asyncua.sync import Client
#from opcua import Client

#with Client(url='opc.tcp://localhost:1234/') as client:
#    while True:
        # Do something with client
#        node = client.get_node('i=1')
#        value = node.read_value()
#        print(value)

client = Client("opc.tcp://localhost:1234/")
try:
        client.connect()
        #client.load_type_definitions()  # load definition of server specific structures/extension objects

        # Client has a few methods to get proxy to UA nodes that should always be in address space such as Root or Objects
        #root = client.get_root_node()
        #print("Root node is: ", root)
        #objects = client.get_objects_node()
        #print("Objects node is: ", objects)

        # Node objects have methods to read and write node attributes as well as browse or populate address space
        #print("Children of root are: ", root.get_children())

        # get a specific node knowing its node id
        #var = client.get_node(ua.NodeId(1002, 2))
        var = client.get_node("ns=2;i=1")
#        handler = SubHandler()
#        sub = client.create_subscription(500, handler)
#        handle = sub.subscribe_data_change(var)
        #var = client.get_node("ns=2;g=1be5ba38-d004-46bd-aa3a-b5b87940c698")
        #print(var)
        #var.get_data_value() # get value of node as a DataValue object
        #print(var.get_value()) # get value of node as a python builtin
        #var.set_value(ua.Variant([23], ua.VariantType.Int64)) #set node value using explicit data type
        var.set_value(2.5) # set node value using implicit data type
        sleep(0.1)
        #print(var.get_value()) # get value of node as a python builtin
        var.set_value(2.5) # set node value using implicit data type
        sleep(0.1)
        #print(var.get_value()) # get value of node as a python builtin
        sleep(0.3)
finally:
        client.disconnect()

server.stop()
