name='RECVTR_HB' #YAML config file with all register values etc
logPath='log'
yamllogfile="prodtest/RCU2HQM"
savedb=False;
savedb=True;


from test_common import *
from datetime import datetime
from prodtest.yamllog import yamllog

loadconf(name);

Bands=[2,1,4]
Band=Bands[0]; #0=110, 1=170, 2=210
DAB=1; #1=DAB filter enables
Att=10;
skip_know_user_input=True;
#skip_know_user_input=False;
db=yamllog(yamllogfile)


data,var1=GetVal('RCU_PCB_ID');
ID=("%.2x%.2x%.2x%.2x" % (data[0],data[1],data[2],data[3]))

logging.warning("ID=%s" % ID)
testtime=datetime.now().strftime("%Y-%m-%d %H:%M");
logging.warning("Time="+testtime);

#If we so not know number, ask it
PCBnum=db.getdata("PCB_number","ID",ID)
if (PCBnum is None) or not(skip_know_user_input):
 print("Enter PCB number:");
 PCBnum=input();
 PCBnum={"ID":ID,"number":PCBnum}
 db.replace("PCB_number","ID",ID,PCBnum)
 if savedb: db.save(yamllogfile);
else:
  print("PCB nunber=",PCBnum['number']);

if True:
 offpwr=db.getdata("off_power","ID",ID)
 if (offpwr is None) or not(skip_know_user_input):
    print("Enter 4V current (A):");
    Ioff3=float(input());
    print("Enter 6V current (A):");
    Ioff6=float(input());
    offpwr={"ID":ID,"I_4V":Ioff3,"I_6V":Ioff6}
 offpwr['time']=testtime;
 data,var1=GetVal('RCU_TEMP');
 D=((data[0]*256+data[1])*256+data[2])*var1.get('scale',1.)
 offpwr['temp']=D;
 logging.warning("RCU Temperature=%.3f K" % D)
 if not Check([D],290,350): exit();

 data,var1=GetVal('RCU_PWR_3V3');
 D=((data[0]*256+data[1])*256+data[2])*var1.get('scale',1.)
 offpwr['V_3v3']=D;
 logging.warning("3V3 =%.3f V" % D)
# print("3V3=",D,"V")
 if not Check([D],3.2,3.4): exit();
 db.replace("off_power","ID",ID,offpwr)
 if savedb: db.save(yamllogfile);
# if (D<3.2) or (D>3.4): exit()
#print("data=",[hex(d) for d in data[:3]]);
#exit()


if True:
# logging.warning("Switch RCU Power off");
# SetRegister("IO1.GPIO1",[0])
# SetRegister("IO1.GPIO2",[0x0]) #Analog power off
# SetRegister("IO2.GPIO1",[0x0]) #Digital power off
# SetRegister("IO2.GPIO2",[0x80])
 SetRegister("IO1.CONF1",[0])
 SetRegister("IO1.CONF2",[0])
 SetRegister("IO2.CONF1",[0x80]) #Pgood on 0x80
 SetRegister("IO2.CONF2",[0])
 SetRegister("IO4.CONF1",[0xC0])#pin 0x40, 0x80 not used
 SetRegister("IO4.CONF2",[0xF8])
 time.sleep(1.0)
 logging.warning("Switch RCU Power on");
 SetRegister("IO1.GPIO1",[Att])#0x0a = 10dB att
 SetRegister("IO1.GPIO2",[0x80+Att])#0x80 Analog on, 0x0a=10dB att
 SetRegister("IO2.GPIO1",[0x40+Att])#0x40 Dig on, 0x0a =10dB att
 SetRegister("IO2.GPIO2",[0x40+Band+(Band<<3)])#0x09 #Band0 (or 0x12 or 0x24)  #LED green=on=low 0x40
 SetRegister("IO4.GPIO1",[0x51 if DAB else 0x2A])#DAB switch states: 0x2A or 0x51
 SetRegister("IO4.GPIO2",[Band])#Band select
 time.sleep(0.5)

# logging.warning("  Check IO expander 1&2");
# print("IO expander status:");
# data,var=GetVal('RCU_IO1_GPIO1');
# if not Check(data,0,0): 
#   logging.warning("  IO1_GPIO1 =%x" % data[0])
#   exit();
# data,var=GetVal('RCU_IO1_GPIO2');
# if not Check(data,0x80,0x80): 
#   logging.warning("  IO1_GPIO2 =%x" % data[0])
#   exit();
# data,var=GetVal('RCU_IO2_GPIO1');
# if not Check(data,0xC0,0xC0): 
#   logging.warning("  IO2_GPIO1 =%x" % data[0])
#   exit();
# data,var=GetVal('RCU_IO2_GPIO2');
# if not Check(data,0x40,0x40): 
#   logging.warning("  IO2_GPIO2 =%x" % data[0])
#   exit();
# time.sleep(0.5)

if True:
 logging.warning("Check voltages")
 onpwr=db.getdata("on_power","ID",ID)
 if (onpwr is None) or not(skip_know_user_input):
    print("Enter 4V current (A):");
    Ioff3=float(input());
    print("Enter 6V current (A):");
    Ioff6=float(input());
    onpwr={"ID":ID,"I_4V":Ioff3,"I_6V":Ioff6}
 onpwr['time']=testtime;
 data,var1=GetVal('RCU_PWR_1V8');
 D=((data[0]*256+data[1])*256+data[2])*var1.get('scale',1.)
 onpwr['V_1v8']=D;
 logging.warning("  1V8 =%.3f V" % D)
 if not Check([D],1.7,1.9): exit();

 data,var1=GetVal('RCU_PWR_2V5');
 D=((data[0]*256+data[1])*256+data[2])*var1.get('scale',1.)
 onpwr['V_2v5']=D;
 logging.warning("  2V5 =%.3f V" % D)
 if not Check([D],2.4,2.6): exit();
 db.replace("on_power","ID",ID,onpwr)
 if savedb: db.save(yamllogfile);

if True:
 antpwr=db.getdata("ant_power","ID",ID)
 if (antpwr is None) or not(skip_know_user_input):
    print("Enter ant current (A):");
    Ioff=float(input());  
    antpwr={"ID":ID,"Ioff":Ioff}
 antpwr['time']=testtime;
 data,var1=GetVal('RCU_PWR_ANT_VIN');
 D0=((data[0]*256+data[1])*256+data[2])*var1.get('scale',1.)
 D1=((data[3]*256+data[4])*256+data[5])*var1.get('scale',1.)
 D2=((data[6]*256+data[7])*256+data[8])*var1.get('scale',1.)
 logging.warning("  Vant_in=%.2f %.2f %.2f" % (D0,D1,D2))
 antpwr['Vin']=[D0,D1,D2]

 if not Check([D0,D1,D2],5,10): exit();

 logging.warning("Switch Antenna power on")
 SetRegister("IO1.GPIO1",[0xC0]) #Antenna power on
 SetRegister("IO1.GPIO2",[0xC0]) #Analog power on

 data,var1=GetVal('RCU_PWR_ANT_VOUT');
 D0=((data[0]*256+data[1])*256+data[2])*var1.get('scale',1.)
 D1=((data[3]*256+data[4])*256+data[5])*var1.get('scale',1.)
 D2=((data[6]*256+data[7])*256+data[8])*var1.get('scale',1.)
 logging.warning("  Vant_out=%.2f %.2f %.2f" % (D0,D1,D2))
 antpwr['Vout']=[D0,D1,D2]
 if not Check([D0,D1,D2],5,10): exit();

 data,var1=GetVal('RCU_PWR_ANT_IOUT');
 D0=((data[0]*256+data[1])*256+data[2])*var1.get('scale',1.)
 D1=((data[3]*256+data[4])*256+data[5])*var1.get('scale',1.)
 D2=((data[6]*256+data[7])*256+data[8])*var1.get('scale',1.)
 antpwr['Iout']=[D0,D1,D2]
 logging.warning("  Iant_out=%.2f %.2f %.2f" % (D0,D1,D2))

 if (antpwr.get("Ion") is None) or not(skip_know_user_input):
    print("Enter ant current (A):");
    Ion=float(input());  
    antpwr["Ion"]=Ion

 db.replace("ant_power","ID",ID,antpwr)
 if savedb: db.save(yamllogfile);
# print("Iant=",D0,D1,D2)
#exit()

if True:
 logging.warning("Test ADC read")
 logging.warning("  Check IO expander 3");
 SetRegister("IO3.GPIO1",[0x15]) #ADC_SDIO=high, clk=low,  DTH_EN=low
 SetRegister("IO3.GPIO2",[0x47]) #ADC SC=high, DTH_SDA=high
 SetRegister("IO3.CONF1",[0]) #All output
 SetRegister("IO3.CONF2",[0])
 data,var=GetVal('RCU_IO3_GPIO1');
 if not Check(data,0x15,0x15): exit();
 data,var=GetVal('RCU_IO3_GPIO2');
 if not Check(data,0x47,0x47): exit();

if True:
 #Test reading register from ADCs
 data,var=GetVal('RCU_ADC_JESD');
 logging.warning("  ADC JESD (0x14)=%x %x %x" % (data[0],data[1],data[2]))
 if not Check(data,0x14,0x14): exit();

 logging.warning("Test ADC write & ADC lock")
 #Test writing ADC register
 SetRegister("ADC1.SYNC_control",[1])
 SetRegister("ADC1.CML_level",[7]);
 SetRegister("ADC1.Update",[1])
 SetRegister("ADC2.SYNC_control",[1])
 SetRegister("ADC2.CML_level",[7]);
 SetRegister("ADC2.Update",[1])
 SetRegister("ADC3.SYNC_control",[1])
 SetRegister("ADC3.CML_level",[7]);
 SetRegister("ADC3.Update",[1])
 data,var=GetVal('RCU_ADC_sync');
 logging.warning("  ADC sync (0x1)=%x %x %x" % (data[0],data[1],data[2]))
 if not Check(data,0x1,0x1): exit();

 data,var=GetVal('RCU_ADC_locked');
 logging.warning("  ADC locked (0x1)=%x %x %x" % (data[0],data[1],data[2]))
 if not Check(data,0x1,0x1): logging.error("*** FAILED ADC lock!")
#      - RCU_ADC_locked: Update

#exit()
#Need to update I2c bitbang....
if False:
 logging.warning("Test DITHER communication")
 SetRegister("IO3.GPIO1",[0x15]) #ADC_SDIO=high, clk=low,  DTH_SDN=low
 SetRegister("IO3.GPIO2",[0x47]) #ADC SC=high, DTH_SDA=high
 SetRegister("IO3.CONF1",[0]) #All output
 SetRegister("IO3.CONF2",[0x0]) #All output (DTH_SDA=input)


# data,var=GetVal('RCU_IO3_GPIO1');print("IO3_1",hex(data[0]))
# data,var=GetVal('RCU_IO3_GPIO2');print("IO3_2",hex(data[0]))
 f0=102.2e6;
 f=int(f0)
 logging.warning("  Set frequency %f"%f0);
# print("Frequency set=",f)
 d=[0]*4;
 for i in range(4):
   d[3-i]=f%256;f//=256
# print([hex(h) for h in d])
 SetRegister("DTH1.Freq",d) #DTH_SDA=input
 SetRegister("DTH2.Freq",d) #DTH_SDA=input
 SetRegister("DTH3.Freq",d) #DTH_SDA=input

 f=int(f0)
 data,var1=GetVal("RCU_DTH_freq")
# data,var1=GetVal("RCU_DTH_Rev")
 for j in range(3):
   f2=0;
   for i in range(4):
     f2=f2*256+data[j*4+i];
   #print("Frequency read back=",f)
   logging.warning("  Readback frequency %f" % f2);
   if not Check([f2],f-1,f+1): exit();
# print([hex(h) for h in data[:30]])

 logging.warning("Test DITHER on")
 SetRegister("DTH1.CONF",[0])
 SetRegister("DTH1.Tune",[0,0])
 SetRegister("DTH1.Start",[0,1,0,0,1])
 SetRegister("DTH2.CONF",[0])
 SetRegister("DTH2.Tune",[0,0])
 SetRegister("DTH2.Start",[0,1,0,0,1])
 SetRegister("DTH3.CONF",[0])
 SetRegister("DTH3.Tune",[0,0])
 SetRegister("DTH3.Start",[0,1,0,0,1])
 time.sleep(0.1)
 data,var=GetVal('RCU_DTH_on');
 logging.warning("  DTH on (0x1)=%x %x %x" % (data[0],data[1],data[2]))
 if not Check(data,0x1,0x1): logging.error("*** FAILED: Dither on! ***")


logging.warning("** PASSED Power and Control test **");


#print(data)
#scale=float(scale)
#data2=[(d*scale) for d in data2]

#print("ID=",[hex(d) for d in data[:4]]);



