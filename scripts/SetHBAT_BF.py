RCU=1
HBAT=0 #HBAT on RCU 0..2
#HBA=5; #HBA Element in HBAT
#BFX=11 #delay in 0.5ns
#BFY=BFX+1
name="HBAT_BF_delay_steps"
from test_common import *
import numpy as np

connect()
AntMask=[(x==HBAT) for x in range(3)]
#AntMask=[True,True,True]
setAntmask([RCU],AntMask)

i=(RCU*3+HBAT)*32

val,tp=get_value_type(name+"_R")
print("old:",val[i:i+32])

val[i:i+32]=np.array(range(32))[::]*0
print("set:",val[i:i+32])

set_value(name+"_RW",val,tp)
time.sleep(1)
val=get_value(name+"_R")
print("new:",val[i:i+32])

disconnect()
