
import logging

#import multiprocessing as mp
#hwdevlock=mp.Lock()

class hwdev():
  def __init__(self,config):
        #print("Init",config['name'])  
        self.conf=config
#        print("Make lock",hwdevlock)

  def OPCUASetVariable(self,varid,var1,data,mask):
      logging.warn(self.conf['name']+" OPCUASetVariable Not implemented!")

  def OPCUAReadVariable(self,varid,var1,mask):
      logging.warn(self.conf['name']+" OPCUAReadVariable Not implemented!")
      return []

  def OPCUAcallMethod(self,var1,data,mask):
      logging.warn(self.conf['name']+" OPCUAcallVariable Not implemented!")
      return []
