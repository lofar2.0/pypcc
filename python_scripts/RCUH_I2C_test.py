import RPi.GPIO as GPIO
from time import sleep
I2C_reset_pin=15

##NB translator should be off or monitor should be 0
name='RECVTR_HB' #YAML config file with all register values etc
varID='RCU_PCB_ID'

import logging
import argparse
from pypcc.opcuaserv import opcuaserv
from pypcc.opcuaserv import i2client
from pypcc.opcuaserv import yamlreader
#from opcuaserv import pypcc2
from pypcc.i2cserv import i2cthread
import threading
import time
import sys
import signal
from pypcc.yamlconfig import Find;
import pypcc.yamlconfig as yc
from datetime import datetime

testtime=datetime.now().strftime("%y-%m-%d %H:%M")

logging.basicConfig(level="WARNING",format='%(asctime)s [%(levelname)-8s,%(filename)-20s:%(lineno)-3d] %(message)s')
#logging.basicConfig(level="DEBUG",format='%(asctime)s [%(levelname)-8s,%(filename)-20s:%(lineno)-3d] %(message)s')


GPIO.setmode(GPIO.BCM)




RunTimer=True;
conf=yc.yamlconfig(name)
conf.linkdevices()
conf.loaddrivers()
conf.linkdrivers()


GPIO.setup(I2C_reset_pin,GPIO.OUT)
#GPIO.output(I2C_reset_pin,0)
#sleep(0.5)
#GPIO.output(I2C_reset_pin,1)

print("State=",GPIO.input(I2C_reset_pin))

def GetVal(name,RCUNR,N=1):
 varid=conf.getvarid(name);
# print("varid",varid)
 var1=conf.getvars()[varid]
 dim=var1['dim']
 drv=var1.get('drivercls');
 mask=[False]*RCUNR*N+[True]*N+[False]*((dim-RCUNR-1)*N);
 data=drv.OPCUAReadVariable(varid,var1,mask)
 data=data[0].data
 N3=len(data)//dim
 return data[N3*RCUNR:N3*(RCUNR+1)],var1

for x in range(32):
   GPIO.output(I2C_reset_pin,0)
   sleep(0.01)
   GPIO.output(I2C_reset_pin,1)
   sleep(0.01)
   data,var1=GetVal(varID,x);
   if data[0] is None:
     print(x,data[0])
   else:
     ID=("%.2x%.2x%.2x%.2x" % (data[0],data[1],data[2],data[3]))
     print(x,ID)
#   print("I2C lines=",GPIO.input(2),GPIO.input(3))

if False:
  GPIO.setup(2,GPIO.IN)
  GPIO.setup(3,GPIO.IN)
  print("I2C lines=",GPIO.input(2),GPIO.input(3))
  GPIO.setup(2,GPIO.OUT)
  GPIO.setup(3,GPIO.OUT)
  GPIO.output(2,0)
  GPIO.output(3,0)
  GPIO.output(2,1)
  GPIO.output(3,1)
  GPIO.setup(2,GPIO.IN)
  GPIO.setup(3,GPIO.IN)
  print("I2C lines=",GPIO.input(2),GPIO.input(3))
GPIO.cleanup()