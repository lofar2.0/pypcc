from test_common import *
connect()

name="RCU_attenuator_dB"
#RCU=[0];
RCU=[0,1,2,3];
#Att=[15,15,15]
#RCU=[0,1,2,3,16,17,18,19];
#Att=[4,4,4]
Att=[4,4,4]


setAntmask(RCU)

att=get_value(name+"_R")
print("Att old:",att[:15])
for r in RCU:
  att[3*r:3*r+3]=Att
print("Att set:",att[:15])
set_value(name+"_RW",att)

time.sleep(0.5)
att=get_value(name+"_R")
print("Att new:",att[:15])

disconnect()