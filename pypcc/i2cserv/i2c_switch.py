import logging
#from .i2c import i2c
from .i2c_smbus2 import i2c_smbus2 as i2c

#import HWconf
#SWaddr=0x70
class i2c_switch(i2c):
    def __init__(self,config):
        i2c.__init__(self,config)
#        self.SWaddr=config['devreg'][0]['addr']
        self.SWcnt=len(config['devreg']);
        self.SWaddrs=[config['devreg'][x]['addr'] for x in range(self.SWcnt)]; 
        self.CurrentChannel=[-1 for x in range(self.SWcnt)];
        self.NoSwitch=self.SWcnt==0;
        S=''; 
        for x in range(self.SWcnt): S+=str(self.SWaddrs[x])+",";
        logging.info("i2c switch at address "+S)
        if self.NoSwitch:
           logging.warn("i2c switch disabled!")

    def clearSwitch(self):
        for x in range(self.SWcnt): self.CurrentChannel[x]=-1;

    def SetSW1(self,channelbit):
#        channel=(0 if (channelbit>5) else 1<<(channelbit)) #LTS
        if self.NoSwitch: return True;
        SWn=channelbit>>3;
        channel=1<<(channelbit & 0x07)
        result=True
        for x in range(self.SWcnt):
           if x==SWn:
              if (channel)!=self.CurrentChannel[x]:
                 self.CurrentChannel[x]=channel
                 logging.debug("SetChannel addr %i = val %i" % (SWn,channel));
                 if not(self.i2csetget(self.SWaddrs[x],[channel])): result=False
           else:
              if self.CurrentChannel[x]!=0:
                 logging.debug("SetChannel addr %i = val %i" % (x,0));
                 self.CurrentChannel[x]=0
                 if not(self.i2csetget(self.SWaddrs[x],[0])): result=False
        if not(result): logging.warning("Switch set error!")
        return result

    def ClearNewChannel(self):
        self.newChannel=[0 for x in self.SWaddrs]+[0];#last one is a dummy

    def AddNewChannel(self,channelbit):
        if self.NoSwitch: return True;
        SWn=(channelbit>>3);
        channel=1<<(channelbit & 0x07)
        self.newChannel[SWn]|=channel;

    def UpdateNewChannel(self):
#        channel&=0x3F;#LTS
        if self.NoSwitch: return True;
        for x in range(self.SWcnt):
              if self.newChannel[x]!=self.CurrentChannel[x]:
                self.CurrentChannel[x]=self.newChannel[x];
                logging.debug("SetChannel2 addr %i = val %i" % (x,self.newChannel[x]));
                if not(self.i2csetget(self.SWaddrs[x],[self.newChannel[x]])): return False;
#        logging.debug("SetChannel=%i" % channel)
#        self.CurrentChannel=channel
        return True

#    def I2Ccallback(self,RCU,addr,data,reg=None,read=0):
#        self.callback1(addr,data,reg,read)    
