import RPi.GPIO as GPIO
#LMP LMP APSCT  CM  rpi
#DIP CON BCK_ID PIN GPIO
#UNB D8   0     27  20
# 1  E8   1     25  21
# 2  E7   2     31  12
# 3  F7   3     29  16
# 4  D10  4     39   8
# 5  E10  5     37   7

def get_LMP_ID():
#    pins=[21,20,16,12,7,8]
    pins=[7,8,16,12,21] #ID5..1, ID0 (GPIO 20)=UNB
    Npins=len(pins);
    GPIO.setmode(GPIO.BCM)
    for i,pin in enumerate(pins):
       GPIO.setup(pin,GPIO.IN)

    value=0;
    for pin in pins:
      value=2*value+1 if GPIO.input(pin) else 2*value
    return value

if __name__ in ['__main__','pypcc.testing.lmp_id']:
    lmpid=get_LMP_ID()
    print(lmpid)
    exit(lmpid)
