logPath='log'

import logging
import argparse
from opcuaserv import opcuaserv
from opcuaserv import i2client
from opcuaserv import yamlreader
#from opcuaserv import pypcc2
#from i2cserv import i2cthread
#import threading
import time
import sys
import signal
from yamlconfig import Find;
import yamlconfig as yc
#from datetime import datetime
#from prodtest.yamllog import yamllog

RCUNR=(0 if len(sys.argv)<2 else int(sys.argv[1]));
print("RCU NR=",RCUNR);
logging.basicConfig(level="WARNING",format='%(asctime)s [%(levelname)-8s,%(filename)-20s:%(lineno)-3d] %(message)s')
#logging.basicConfig(level="DEBUG",format='%(asctime)s [%(levelname)-8s,%(filename)-20s:%(lineno)-3d] %(message)s')

global conf
conf=None

RunTimer=True;
def loadconf(name):
  global conf
  conf=yc.yamlconfig(name)
  conf.linkdevices()
  conf.loaddrivers()
  conf.linkdrivers()
  

#db=yamllog(yamllogfile)
rootLogger = logging.getLogger()

def GetVal(name,N=1):
 varid=conf.getvarid(name);
 var1=conf.getvars()[varid]
 drv=var1.get('drivercls');
 mask=[False]*RCUNR*N+[True]*N+[False]*((32-RCUNR-1)*N);
 data=drv.OPCUAReadVariable(varid,var1,mask)
 data=data[0].data
 N3=len(data)//32;
 return data[N3*RCUNR:N3*(RCUNR+1)],var1

def Check(D,Dmin,Dmax):
  for d in D:
    if (d<Dmin): 
        logging.error("Value to small");
#        return False;
    if (d>Dmax): 
        logging.error("Value to large");
#        return False;
  return True

def SetRegister(regname,value):
  methodid=conf.getmethodid("RCU_on");
  var1=conf.getmethod(methodid)
  drv=var1.get('drivercls');
  v1=conf.getdevreg(regname)
  drv2=v1.get('drivercls')
  mask=[False]*(RCUNR)+[True]+[False]*(31-RCUNR);
  if drv:  drv.Setdevreg(v1,value,mask)
  elif drv2: drv2.Setdevreg(v1,value,mask)
  else: logging.warn("Driver not specified for instruction"+key)


