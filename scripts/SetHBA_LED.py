RCU=1
HBAT=1 #HBAT on RCU 0..2
HBA=14; #HBA Element in HBAT
LED=0 #on
name="HBA_element_led"

from test_common import *
import numpy as np

AntMask=[(x==HBAT) for x in range(3)]
setAntmask([RCU],AntMask)

i=(RCU*3+HBAT)*32+HBA*2

val=get_value(name+"_R")
print("old:",val[i:i+2])

val[i]=LED #Not needed for LED
val[i+1]=LED

set_value(name+"_RW",val)
time.sleep(1)
val=get_value(name+"_R")
print("new:",val[i:i+2])

disconnect()
