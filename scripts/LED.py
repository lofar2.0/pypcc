from test_common import *

name="RCU_LED_green_on"
#name="RCU_LED_red_on"
RCU=2;
LEDvalue=True;

connect()
setRCUmask([RCU])
led=get_value(name+"_R")
print("LED old:",led)
led[RCU]=LEDvalue

set_value(name+"_RW",led)
time.sleep(0.1)

print("LED new:",get_value(name+"_R"))

disconnect()
